package main

import (
	"net/http"
	"os"
	"strings"
	"time"

	// "github.com/mrwnmonm/spacecafe/api/g"
	"github.com/mrwnmonm/spacecafe/api"
)

var mux = http.NewServeMux()
var apiV01Path = "/api/v01"

type _handler func(http.ResponseWriter, *http.Request) *api.Response

type handler struct {
	Handler _handler
	NeedDb  bool
}

func handle(fn _handler, needDb ...bool) *handler {
	if needDb != nil && !needDb[0] {
		return &handler{fn, false}
	}
	return &handler{fn, true}
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if h.NeedDb {
		tx, err := api.JetDb.Begin()
		if err != nil {
			http.Error(w, "Server Error", 500)
			api.ERROR.Println(err)
		}

		defer tx.Commit()

		api.SetTx(r, tx)
	} else {
		api.SetTx(r, nil)
	}

	response := h.Handler(w, r)

	if response == nil {
		return
	}

	switch response.Type {
	case "success":
		api.Respond(w, 200)
	case "error":
		if response.Error != nil {
			api.ERROR.Println(response.Error)
		}
		// http.Error(w, response.Message, response.Code)
		// g.Respond(w, response.Code, response.Message)
		api.RespondWithJson(response.Code, w, &response)
	case "json":
		api.RespondWithJson(response.Code, w, &response.Data)
	}
}

func main() {
	// ironman.InitLoggers()
	// conf.InitConf()
	// conf.LogConf(g.TRACE)
	// ironman.InitDb()
	// ironman.InitRedis()
	// ironman.InitOAuth()
	// g.Init()
	api.Init()

	// ROUTES
	//
	// Serve static files
	mux.HandleFunc("/public/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, r.URL.Path[1:])
	})

	// API Routes
	// TODO: specify request methods
	// TODO: see which requests need authentication and remove it if it don't need it

	// Users
	// mux.HandleFunc(apiV01Path+"/signup", users.Create)
	// mux.HandleFunc(apiV01Path+"/login", users.Login)
	// mux.HandleFunc(apiV01Path+"/verify_email", users.Confirm)
	// mux.HandleFunc(apiV01Path+"/reconfirm_email", users.Reconfirm)
	// mux.HandleFunc(apiV01Path+"/request_pass", users.ResetPass)
	// mux.HandleFunc(apiV01Path+"/renew_pass", users.RenewPass)

	mux.Handle(apiV01Path+"/login", handle(api.Login))
	mux.Handle(apiV01Path+"/connect", handle(api.Connect, false))

	mux.Handle(apiV01Path+"/notificationscount", handle(api.GetUnreadNotificationCount, false))
	mux.Handle(apiV01Path+"/notifications", handle(api.GetNotifications))
	mux.Handle(apiV01Path+"/notifications/read", handle(api.MarkNotificationAsRead, false))

	// // Tables
	mux.Handle(apiV01Path+"/tables", handle(api.NewTable))
	mux.Handle(apiV01Path+"/tables/show", handle(api.GetTable))
	// mux.HandleFunc(apiV01Path+"/tables/join", tables.Join)
	// mux.HandleFunc(apiV01Path+"/tables/leave", tables.Leave)
	mux.Handle(apiV01Path+"/tables/mytables", handle(api.GetUserTables))
	// mux.HandleFunc(apiV01Path+"/tables/search", tables.Search)
	// mux.HandleFunc(apiV01Path + "/tables/popular", tables.Popular)
	// mux.HandleFunc(apiV01Path + "/tables/most_active", tables.MostActive)

	// Votes
	mux.Handle(apiV01Path+"/vote", handle(api.Vote))

	// // Posts
	mux.Handle(apiV01Path+"/posts/index", handle(api.GetPosts))
	mux.Handle(apiV01Path+"/posts/new", handle(api.NewPost))
	mux.Handle(apiV01Path+"/posts/show", handle(api.GetPost))
	// mux.HandleFunc(apiV01Path + "/posts/delete", posts.Delete)
	mux.Handle(apiV01Path+"/posts/scrape", handle(api.ScrapeURL, false))

	// // Comments
	mux.Handle(apiV01Path+"/comments", handle(api.NewComment))
	mux.Handle(apiV01Path+"/comments/index", handle(api.GetComments))
	mux.Handle(apiV01Path+"/comments/indexflat", handle(api.GetCommentsFlat))
	// mux.HandleFunc(apiV01Path+"/comments/delete", api.DeleteC)

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		upath := r.URL.Path
		// ensure that the url prefixed with "/" and if not add it
		if !strings.HasPrefix(upath, "/") {
			upath = "/" + upath
			r.URL.Path = upath
		}
		// ironman.LOG.Println("*******************" + upath)
		// if upath == "/" {
		// 	http.ServeFile(w, r, "index.html")
		// } else {

		// 	if strings.HasPrefix(upath, "/api") {
		// 		// handle unexisted routes
		// 		// http.Redirect(w, r, "/#/404", 404)
		// 	} else {
		// 		http.ServeFile(w, r, "index.html")
		// 		// pass the request to front-end router
		// 		// http.Redirect(w, r, "/#"+upath, 307)
		// 	}
		// }
		http.ServeFile(w, r, "index.html")
	})

	// restrict url protocols to http and https

	port := os.Getenv("DATABASE_URL")

	if port == "" {
		port = api.Port
	}

	// TODO: set timeout
	// Listen and serve, and assign a top handler which begin/commit/rollback the database and log the requests and responses
	err := http.ListenAndServe(":"+port, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		api.LOG.Printf("%s  %s", r.Method, r.RequestURI)
		requestStartedAt := time.Now()

		mux.ServeHTTP(w, r)

		// log request and response status
		// the response status here is came from the ironman.RespondWithJson function
		// where a header key is set called Status with the response status code
		status := w.Header().Get("Status")
		// just align those request with ___ for the logs looks pretty
		if status == "200" {
			api.LOG.Printf("%s  %dms", status, time.Since(requestStartedAt)/time.Millisecond)
		}
		if strings.HasPrefix(r.RequestURI, "/public") || strings.HasPrefix(r.RequestURI, "/favicon") {
			status = "___"
		}
		if status != "200" || status != "___" {
			api.LOG.Printf("%s  %dms", status, time.Since(requestStartedAt)/time.Millisecond)
		}
	}))
	if err != nil {
		panic(err)
	}
}
