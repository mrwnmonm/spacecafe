var Dispatcher = require('../dispatcher/Dispatcher');
var Constants = require('../constants/Constants');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var CHANGE_EVENT = 'change';

var _component = "";
var _params = {};

var _router;

var RouteStore = assign({}, EventEmitter.prototype, {
  getComponent: function(){
    return _component;
  },

  getParams: function(){
    return _params;
  },

  setRouter: function(router){
    _router = router;
  },
 
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

// Register to handle all updates
Dispatcher.register(function(payload) {
  var action = payload.action;
  switch(action.type) {
    case Constants.ROUTE_RENDER:
      _component = action.component;
      _params = (typeof action.params == "undefined") ? {} : action.params;
      RouteStore.emitChange();
      break;
    case Constants.ROUTE_REDIRECT:
      _router.setRoute(action.route);
      break;
    default:
      return true;
  }
  
  return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = RouteStore;