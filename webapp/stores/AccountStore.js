// TODO 'use strict';
var Dispatcher = require('../dispatcher/Dispatcher');
var Constants = require('../constants/Constants');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var cookies = require('../lib/cookies');
var CHANGE_EVENT = 'change';

var _defaultTable = "All";
var _tables = [_defaultTable];
var _id;
var _xtoken;
var _name;
var _notificationsCount = 0;
var _isLoggedIn;
var _notifications = {};

function _init() {
    _id = cookies.get("id");
    _name = cookies.get("name");
    _xtoken = cookies.get("xtoken");
    _isLoggedIn = (cookies.get("xtoken") !== null);
}

function _logout() {
    _id = "";
    _name = "";
    _xtoken = "";
    _isLoggedIn = false;
    _notificationsCount = 0;
    cookies.del("id");
    cookies.del("name");
    cookies.del("xtoken");
}

// function _joinTable(id) {

// }

// function _leaveTable(id) {

// }

var AccountStore = assign({}, EventEmitter.prototype, {
    getTables: function() {
        return _tables;
    },
    init: function() {
        _init();
    },
    logout: function() {
        _logout();
    },
    getId: function() {
        return _id;
    },
    getName: function() {
        return _name;
    },
    getXtoken: function() {
        return _xtoken;
    },
    getNotificationsCount: function() {
        return _notificationsCount;
    },
    isLoggedIn: function() {
        return _isLoggedIn;
    },
    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

// Register to handle all updates
Dispatcher.register(function(payload) {
    var action = payload.action;
    switch (action.type) {
    case Constants.LOGIN:
        _id = action.id;
        _name = action.name;
        _xtoken = action.xtoken;
        _isLoggedIn = true;
        // checkNotifications();
        // connect(Date.now());
        AccountStore.emitChange();
        break;
    case Constants.SET_TABLES:
        var t = action.tables;
        t.unshift(_defaultTable);
        _tables = t;
        AccountStore.emitChange();
        break;
    case Constants.INCREASE_NOTIFICATIONS:
        _notificationsCount = _notificationsCount + action.count;
        AccountStore.emitChange();
        break;
    case Constants.SET_NOTIFICATIONS:
        _notificationsCount = action.count;
        AccountStore.emitChange();
        break;
    default:
        return true;
    }

    return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = AccountStore;