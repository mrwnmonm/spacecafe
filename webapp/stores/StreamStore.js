var Dispatcher = require('../dispatcher/Dispatcher');
var Constants = require('../constants/Constants');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var CHANGE_EVENT = 'change';
var conf = require('../lib/conf');

var _posts = [];

var _expanded = [];

// one level comments
// a way to collapse comments // very important
// Nesting of comments is tricky. 
// Experienced discussion group users love the control; 
// lighter users may find the 'tree' format offputting. 
// One level is probably the right compromise. 


var _getNicePreviewTitle = function(title, haveimg){
  var maxt = conf.getMaxPreviewTitleLength(haveimg);
  if(title.length > maxt){
    return (title.slice(0,maxt - 3) + "...");
  }else{
    return title
  }
}

var _getNicePreviewDesc = function(desc, haveimg){
  var maxd = conf.getMaxPreviewDescLength(haveimg);
  if(desc.length > maxd){
    return (desc.slice(0,maxd - 3) + "...");
  }else{
    return desc
  }
}

var _push = function(posts){
  posts.map(function(post){
    // post.comments = [];
    post.expandclass = [];
    post.expanded = false;
    _posts.push.apply(_posts, [post]);
  });
}

var _assignExpandClasses = function (index) {
  _posts[index].expanded = !_posts[index].expanded;
  if (_posts.length < 2) {return};
  if (!_posts[index].expanded) {
    var x = _posts[index].expandclass.indexOf("pexpanded")
    _posts[index].expandclass.splice(x, 1);
    _expanded.splice(_expanded.indexOf(index), 1)
    if (index == 0) {
      var i = _posts[1].expandclass.indexOf("afterexpanded")
      _posts[1].expandclass.splice(i, 1) 
      return
    }
    if (index == _posts.length - 1) {
      var i = _posts[_posts.length - 2].expandclass.indexOf("beforeexpanded")
      _posts[_posts.length - 2].expandclass.splice(i, 1)
      return
    }
    var i = _posts[index - 1].expandclass.indexOf("beforeexpanded")
    _posts[index - 1].expandclass.splice(i, 1)
    var ii = _posts[index + 1].expandclass.indexOf("afterexpanded")
    _posts[index + 1].expandclass.splice(ii, 1)
  }else{
    _posts[index].expandclass.push("pexpanded");
    _expanded.push(index);
    if (index == 0) {
      _posts[1].expandclass.push("afterexpanded")
      return
    }
    if (index == _posts.length - 1) {
      _posts[_posts.length - 2].expandclass.push("beforeexpanded")
      return
    }
    _posts[index - 1].expandclass.push("beforeexpanded")
    _posts[index + 1].expandclass.push("afterexpanded")
  }
}

var _collapseAll = function () {
  if (_posts.length < 2) {return};
    for (var i = 0; i < _expanded.length; i++) {
      var index = _expanded[i];
      _posts[index].expandclass = [];
      _posts[index].expanded = false;
      if (index == 0) {
        _posts[1].expandclass = [];
        continue;
      }
      if (index == _posts.length - 1) {
        _posts[_posts.length - 2].expandclass = [];
        continue;
      }
      _posts[index - 1].expandclass = [];
      _posts[index + 1].expandclass = [];
    };
  _expanded = [];
}

var StreamStore = assign({}, EventEmitter.prototype, {
  getPosts: function(){
    return _posts;
  },
  getPreview: function(){
    return _preview;
  },
  getNicePreviewTitle: function(title, haveimg) {
    return _getNicePreviewTitle(title, haveimg);
  },
  getNicePreviewDesc: function(desc, haveimg) {
    return _getNicePreviewDesc(desc, haveimg);
  },
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

// Register to handle all updates
Dispatcher.register(function(payload) {
  var action = payload.action;

  switch(action.type) {
    case Constants.STREAM_EXPAND:
      _assignExpandClasses(action.index)
      StreamStore.emitChange();
      break;
    case Constants.STREAM_COLLAPSE_ALL:
      _collapseAll()
      StreamStore.emitChange();
      break;
    case Constants.STREAM_INCREASE_COMMENTS:
      _posts[action.index].comments_count = _posts[action.index].comments_count + 1;
      StreamStore.emitChange();
      break;
    case Constants.STREAM_CLEAR:
      _posts = [];
      _expanded = [];
      StreamStore.emitChange();
      break;
    case Constants.STREAM_PUSH:
      _push(action.posts);
      StreamStore.emitChange();
      break;
    case Constants.STREAM_UPDATE:
      _posts[action.index] = assign(_posts[action.index], action.update);
      StreamStore.emitChange();
      break;
    case Constants.STREAM_UNSHIFT:
      // action.post.comments = [];
      action.post.expandclass = [];
      action.post.expanded = false;
      _posts.unshift(action.post);
      StreamStore.emitChange();
      break;
    // case Constants.STREAM_UPDATE_PREVIEW:
    //   _preview = assign(_preview, action.preview);
    //   StreamStore.emitChange();
    //   break;
    // case Constants.STREAM_RESET_PREVIEW:
    //   _resetPreview();
    //   StreamStore.emitChange();
    default:
      return true;
  }

  return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = StreamStore;