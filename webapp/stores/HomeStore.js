var Dispatcher = require('../dispatcher/Dispatcher');
var Constants = require('../constants/Constants');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var conf = require('../lib/conf');
var CHANGE_EVENT = 'change';
var _currentTable = conf.DefaultTable;

function _changeTable(table) {
  _currentTable = table;
}

var HomeStore = assign({}, EventEmitter.prototype, {
  getCurrentTable: function() {
    return _currentTable;
  },
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

// Register to handle all updates
Dispatcher.register(function(payload) {
  var action = payload.action;

  switch(action.type) {
    case Constants.HOME_CHANGE_TABLE:
      _changeTable(action.text);
      HomeStore.emitChange();
      break;
    default:
      return true;
  }

  return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = HomeStore;