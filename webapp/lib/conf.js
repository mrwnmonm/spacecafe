var conf = {
	APIPath: "/api/v01",
	DefaultTable: "All",
	getMaxPreviewTitleLength: function(haveimg) {
		if (haveimg) {return 140}else{return 140};
	},
	getMaxPreviewDescLength: function(haveimg) {
		if(haveimg) {return 72}else{return 72};
	}
}

module.exports = conf;