var Dispatcher = require('../dispatcher/Dispatcher');
var Constants = require('../constants/Constants')

var StreamActions = {
	clear: function(posts) {
	    Dispatcher.handle({
	        type: Constants.STREAM_CLEAR,
	        posts: posts
	    });
	},
    push: function(posts) {
        Dispatcher.handle({
            type: Constants.STREAM_PUSH,
            posts: posts
        });
    },
    expand: function(index, expanded) {
        Dispatcher.handle({
            type: Constants.STREAM_EXPAND,
            index: index
        });
    },
    increaseComments: function(index) {
        Dispatcher.handle({
            type: Constants.STREAM_INCREASE_COMMENTS,
            index: index
        });
    },
    collapseAll: function() {
        Dispatcher.handle({
            type: Constants.STREAM_COLLAPSE_ALL
        });
    },
    update: function(index, update) {
        Dispatcher.handle({
            type: Constants.STREAM_UPDATE,
            index: index,
            update: update
        });
    },
    unshift: function(post) {
        Dispatcher.handle({
            type: Constants.STREAM_UNSHIFT,
            post: post
        });
    }
    // updatePreview: function(preview){
    // 	Dispatcher.handle({
    // 		type: Constants.STREAM_UPDATE_PREVIEW,
    // 		preview: preview
    // 	});
    // },
    // resetPreview: function(){
    // 	Dispatcher.handle({
    // 		type: Constants.STREAM_RESET_PREVIEW
    // 	});
    // }
};

module.exports = StreamActions;