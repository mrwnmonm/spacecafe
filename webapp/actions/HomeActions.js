var Dispatcher = require('../dispatcher/Dispatcher');
var Constants = require('../constants/Constants')

var HomeActions = {
  changeTable: function(text) {
    Dispatcher.handle({
      type: Constants.HOME_CHANGE_TABLE,
      text: text
    });
  }
};

module.exports = HomeActions;