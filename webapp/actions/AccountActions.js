var Dispatcher = require('../dispatcher/Dispatcher');
var Constants = require('../constants/Constants')

var AccountActions = {
  login: function(id, name, xtoken){
     Dispatcher.handle({
      type: Constants.LOGIN,
      id: id,
      name: name,
      xtoken: xtoken
    });
  },
  setTables: function(tables) {
    Dispatcher.handle({
      type: Constants.SET_TABLES,
      tables: tables
    });
  },
  increaseNotifications: function(count) {
    Dispatcher.handle({
      type: Constants.INCREASE_NOTIFICATIONS,
      count: count
    });
  },
  setNotifications: function(count) {
    Dispatcher.handle({
      type: Constants.SET_NOTIFICATIONS,
      count: count
    });
  }
};

module.exports = AccountActions;