var Dispatcher = require('../dispatcher/Dispatcher');
var Constants = require('../constants/Constants')

var RouteActions = {
  render: function(component, params) {
    Dispatcher.handle({
      	type: Constants.ROUTE_RENDER,
      	component: component,
        params: params
    });
  },
  redirect: function(route){
  	Dispatcher.handle({
      	type: Constants.ROUTE_REDIRECT,
      	route: route
    });
  }
};

module.exports = RouteActions;