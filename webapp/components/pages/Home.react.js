var React = require('react');
var AccountActions = require('../../actions/AccountActions');
var AccountStore = require('../../stores/AccountStore');
var StreamActions = require('../../actions/StreamActions');
var HomeActions = require('../../actions/HomeActions');
var HomeStore = require('../../stores/HomeStore');
var http = require('../../lib/http');
var PostForm = require('../PostForm.react');
var LinkForm = require('../LinkForm.react');
var Stream = require('../Stream.react');


function getStateFromStores() {
  return {
  	tables: AccountStore.getTables(),
    currentTable: HomeStore.getCurrentTable()
  };
}

var Home = React.createClass({
    getInitialState: function() {
        var state = getStateFromStores();
        this.changeTable(state.currentTable, true);
        return state;
    },

    componentWillMount: function(){
        if(!AccountStore.isLoggedIn()){this.props.history.pushState(null, '/welcome'); return;}
        http.post('/tables/mytables', {}, 
        function(data, status){ AccountActions.setTables(data.tables); }, function(data, status){});
    },

    componentDidMount: function() {
        HomeStore.addChangeListener(this._onChange);
        AccountStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function() {
        HomeStore.removeChangeListener(this._onChange);
        AccountStore.removeChangeListener(this._onChange);
    },

    changeTable: function(table, first){
      if (!first) {
        if (this.state.currentTable == table) {return;};
      };
      HomeActions.changeTable(table);
      // StreamActions.clear();
      // this.loadPosts(0, table, "t");
      // if (first == true){}else{this.refs.postbody.value = "";}
      // StreamActions.resetPreview();
    },

    render: function() {
        var currentTable  = this.state.currentTable;
        var changeTable = this.changeTable;
        return ( 
          <div className="col2">
            <div className="sidebar hsb">
              <div className="sc">
                <div className="title"><span>Tables</span></div>
                <ul className="list cleanlist">
                 {this.state.tables.map(function(table) {
                  return ( 
                     <li className="table" key={table}>
                      <div className={table == currentTable ? "active" : ""}>
                          <a className="name" onClick={function(){changeTable(table)}}>{table}</a>
                      </div>
                    </li>
                  );
                })}
                </ul>
              </div>
            </div> 

            <div className="content">
              <div className="strmhead">
                <span className="t">{this.state.currentTable}</span>
                <span className={this.state.currentTable == "All" ? "hide" : "pg"}>About & Rules</span>
                <ul className="sort cleanlist">
                  <li className="active">Latest</li>
                  <li>Hot</li>
                  <li>Top</li>
                  <li>Controversial</li>
                </ul>
              </div>
              <PostForm id={this.state.currentTable}  />
            {/*<LinkForm id={this.state.currentTable} /> */}
              <Stream   type={"t"} id={this.state.currentTable}  />
            </div>

            
          </div>
       	)
    },

    _onChange: function() {
    this.setState(getStateFromStores());
  }
});

module.exports = Home;

