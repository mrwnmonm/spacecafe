var React = require('react');
var http = require('../../lib/http');
var RouteActions = require('../../actions/RouteActions');
var History = require('react-router').History;
var Textarea = require('react-textarea-autosize');
var marked = require('marked');
var renderer = new marked.Renderer();

var TableCreate = React.createClass({
    mixins: [History],
    getInitialState: function() {
        renderer.image = function (href, title, text) {
          return "!["+text+"]("+href+")";
        }
        return {
            btnDisabled: false,
            nameErr: "",
            name: "",
            descCounter: 140,
            about: ""
        }
    },
    create: function(e) {
        e.preventDefault();
        var bd = true;
        this.setState({
            btnDisabled: true,
            nameErr: ""
        });

        var name = this.refs.name.value.trim();
        var desc = this.refs.desc.value.trim();
        var about = this.refs.about.value;
        if (desc.length > 140 || desc == "" || name == "") {return};


        // TODO: validation name length or other things
        if (bd) {
            params = {
                name: name,
                desc: desc,
                about: about,
                type: this.refs.restricted.checked ? "1" : "0"
            }
            http.post("/tables", params, function(data, status) {
                this.history.pushState(null, "/"+name);
            }.bind(this), function(data, status) {
                if (status == 420) {
                    this.setState({
                        btnDisabled: false,
                        nameErr: "this table already exist, <strong><a href='/" + name + "'>take a look</a>"
                    });
                }
            }.bind(this));
        }
        ;
    },
    onChangeName: function(){
        var name = this.refs.name.value;
        if (name == "") {
            this.setState({nameErr: "Table name can't blank", name: name})
            return
        };
        if(name.length > 25){
            this.setState({nameErr: "Table name can't be more than 25 character", name: name.substring(0,25)});
            return;
        }
        var reg = new RegExp('^[a-z0-9_]{1,25}$');
        var x = name.match(reg);
        if(x == null){
          this.setState({nameErr: "Only use small letters, numbers and '_'", name: name.trim()})
          return
        }
        this.setState({nameErr: "", name: name.trim()})
    },
    onChangeDesc: function(){
        var desc = this.refs.desc.value;
        this.setState({descCounter: 140 - desc.length});
    },
    preview: function(){
        this.setState({about: marked(this.refs.about.value, { renderer: renderer })})
    },
    render: function() {
        return (
            <div className="col2">
              <div className="header">
                <span className="title">Create New Table</span>
                <ul className="ctrl cleanlist">
                  <li className="tab"><button onClick={this.create} className="btn bluebtn joinbtn" type="submit" disabled={ this.btnDisabled }>
                    Create
                  </button>
                  </li>
                </ul>
               
              </div>
              <div className="sidebar">
                <div className="sc">
                  <div className="title"><span>About</span></div>
                  <div><span></span></div>
                </div>
                
              </div>
    
              <div className="content">
                <div id="tblfrm">
                  <form onSubmit={ this.create }>
                    <div className="form-group">
                        <input tabIndex="1" style={{width: "100%"}} value={this.state.name} onChange={this.onChangeName} type="text" placeholder="Name (choose naming the group over the topic)" ref="name" />
                        <span className="error hint" dangerouslySetInnerHTML={ {    __html: this.state.nameErr} }></span>
                    </div>
                    <div className="form-group">
                        <Textarea tabIndex="2" style={{paddingRight: "35px"}} onChange={this.onChangeDesc} rows={2} placeholder="Short Description" ref="desc"></Textarea>
                        <span style={{top: "5px"}} className={ this.state.descCounter < 0 ? "error hint" : "hint"}>{this.state.descCounter}</span>
                    </div>
                    <div className="form-group">
                        <Textarea tabIndex="3" onChange={this.onChangeAbout} rows={10} ref="about" placeholder="About (long description, rules, references, etc...)"></Textarea>
                    </div>
                    <div className="form-ctrl">
                        <input tabIndex="4" style={{float: "left", marginTop: "2px"}} type="checkbox" ref="restricted" value="" /> 
                        <span style={{lineHeight: "1.3", marginLeft: 6, float: "left", textAlign: "left", fontSize: "13px", color: '#555', fontWeight: 'bold'}}>Restricted Table<br /><span style={{fontWeight:"normal", color: "#999"}}>Anyone can view, but only approved members can post</span></span>
                        <button tabIndex="6" className="btn bigbtn" type="submit" disabled={ this.btnDisabled }>Create</button>
                        <button tabIndex="5" className="btn bigbtn" type="button" onClick={this.preview}>Preview</button>
                    </div>
                  </form>
                  <div dangerouslySetInnerHTML={{__html: this.state.about}}></div>
                </div>
              </div>
            </div>
            )
    }
});

module.exports = TableCreate;