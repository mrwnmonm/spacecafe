var React = require('react');
var http = require('../../lib/http');
var Post = require('../Post.react');

var PostShow = React.createClass({
  getInitialState: function() {
    this.getPost();
    return {post: {}}
  },
 
  getPost: function(){
    var self = this;
    http.post('/posts/show', {post_id: this.props.params.id}, 
    function(data, status){ self.setState({post: data}); }, function(data, status){});
  },

  render: function(){
    return (
      <div className="postshow">
       <Post post={this.state.post} post_id={this.props.params.id} type={"show"} />
      </div>
    )
  }
});

module.exports = PostShow;