var React = require('react');
var http = require('../../lib/http');
var cookies = require('../../lib/cookies');
var AccountStore = require('../../stores/AccountStore');

var Welcome = React.createClass({
  getInitialState: function() {
    return{btnDisabled: false}
  },
 componentWillMount: function(){
      if(AccountStore.isLoggedIn()){this.props.history.pushState(null, '/'); return;}
  },
  login: function(e){
    e.preventDefault();
    cookies.set('remember_me', this.refs.remember.checked.toString(), Infinity)
    http.post("/login", {state: "init"}, function(data, status){
      var afterhours = 1;
      var expiretime = new Date().getTime() + (3600 * 1000 * afterhours);
      cookies.set('rtoken', data.rtoken, expiretime);
      cookies.set('rsecret', data.rsecret, expiretime);
      window.location.href = (data.url);
    })
  },

  render: function (){
    return (
      <div>
        <p><span className="bold h3">Login</span><span className="error">{this.state.loginErr}</span></p>
        <p className="bold h5 hint" dangerouslySetInnerHTML={{__html: this.state.loginHint}}></p>
        <form onSubmit={this.login}>
          <input type="checkbox" ref="remember" value="" /> 
          <span style={{marginLeft: 3, position: "absolute", fontSize: "13px", color: '#fff', textShadow: '0 1px 1px #719BC7', fontWeight: 'bold'}}>Remember me</span>
          <br />
          <button className="btn" type="submit">Login with twitter</button>
        </form>

      </div>
      )
  }
});

module.exports = Welcome;