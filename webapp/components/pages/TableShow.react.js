var React = require('react');
var Stream = require('../Stream.react');
var http = require('../../lib/http');

var TableShow = React.createClass({
    getInitialState: function() {
         http.post("/tables/show", {name: this.props.params.table}, function(data, status) {
                this.setState({
                  isMember: data.is_member,
                  table: data.table
                })
            }.bind(this), function(data, status) {
                
            }.bind(this));
         return {
          isMember: false,
          table: {members_count: 1}
        }
     },

    componentDidMount: function() {
        // StreamStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function() {
        // StreamStore.removeChangeListener(this._onChange);
    },

    toggleMembership: function() {},

    render: function() {
        var joinBtnTitle = this.state.isMember ? "Leave" : "Join"
        return (
            <div className="col2">
              <div className="header">
                <span className="title">{this.state.table.name}</span>
                <span className="tlbl">Restricted</span>
                <ul className="ctrl cleanlist">
                  <li className="tab"><p className="lbl">Admins</p><p><strong>{3}</strong></p></li>
                  <li className="tab"><p className="lbl">Members</p><p><strong>{this.state.table.members_count}</strong></p></li>
                  <li className="tab"><button onClick={this.toggleMembership} className="btn bluebtn joinbtn" type="submit" disabled={ this.btnDisabled }>
                    { joinBtnTitle }
                  </button>
                  </li>
                </ul>
               
              </div>
              <div className="sidebar">
                <div className="sc">
                  <div className="title"><span>About</span></div>
                  <div><span dangerouslySetInnerHTML={{ __html: this.state.table.about}}></span></div>
                </div>
                
              </div>
    

              <div className="content">
                <div className="strmhead">
                  <span className="t">{this.state.table.name}</span>
                  <ul className="sort cleanlist">
                    <li className="active">Latest</li>
                    <li>Hot</li>
                    <li>Top</li>
                    <li>Controversial</li>
                  </ul>
                </div>
                <Stream type={ "t" } id={ this.props.params.table } />
              </div>
            </div>
            )
    },
    _onChange: function() {
        this.setState(getStateFromStores());
    }
});

module.exports = TableShow;