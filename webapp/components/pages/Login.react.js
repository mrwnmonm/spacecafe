var React = require('react');
var http = require('../../lib/http');
var cookies = require('../../lib/cookies');
var AccountStore = require('../../stores/AccountStore');
var AccountActions = require('../../actions/AccountActions');

var Login = React.createClass({
  componentWillMount: function(){
    if(typeof this.props.location.query.oauth_token == 'undefined' || typeof this.props.location.query.oauth_verifier == 'undefined'){
      this.props.history.pushState(null, '/welcome');
    }
    var rtoken = cookies.get('rtoken');
    var rsecret = cookies.get('rsecret');
    var JsonData = {
      rtoken: rtoken,
      rsecret: rsecret,
      oauth_token: this.props.location.query.oauth_token,
      oauth_verifier: this.props.location.query.oauth_verifier
    }
    http.post("/login", JsonData, function(data, status){
      cookies.del('rtoken');
      cookies.del('rsecret');
      cookies.del('remember_me');
      if (data.remember_me) {
        cookies.set('xtoken', data.xtoken, Infinity);
        cookies.set('id',data.user.id, Infinity);
        cookies.set("name", data.user.name, Infinity);
      }else{
        cookies.set('xtoken', data.xtoken);
        cookies.set('id',data.user.id);
        cookies.set("name", data.user.name);
      };
      AccountActions.login(data.user.id, data.user.name, data.xtoken);
      this.props.history.pushState(null, '/')
    }.bind(this))
  },

  render: function (){
    return (
      <div>
        <h1>Login...</h1>
      </div>
      )
  }
});

module.exports = Login;