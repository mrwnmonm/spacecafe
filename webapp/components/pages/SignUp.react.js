var React = require('react');
var http = require('../../lib/http');

var SignUp = React.createClass({
  getInitialState: function() {
    return {formStyle: {}, btnDisabled: false, signUpErr: "", nameErr:"",usernameErr: "", emailErr:"", passErr:"", passConfErr:"", msg:{title:"Sign Up", body:""}}
  },
  signup: function(e){
    e.preventDefault();
    this.setState({btnDisabled: true, signUpErr: "", nameErr:"", usernameErr: "", emailErr:"", passErr:"", passConfErr:""});

    // validations
    // if (this.refs.name.value == "") {
    //     this.setState({btnDisabled: false, nameErr: "enter your name"});
    // };

    // if (this.refs.name.value > 25) {
    //     this.setState({btnDisabled: false, nameErr: "Name can't be more than 25 charachters"});
    // };


    // TODO: validate username

    
    // if (this.refs.email.value == "") {
    //     this.setState({btnDisabled: false, emailErr: "enter your email"});
    // };

    // if (this.refs.email.value > 320){
    //     this.setState({btnDisabled: false, emailErr: "Email can't be more than 320 characters"});
    // }

    // if (this.refs.password.value == "") {
    //     this.setState({btnDisabled: false, passErr: "enter your password"});
    // };

    // if (this.refs.password.value != "" && this.refs.password.value.length < 6) {
    //     this.setState({btnDisabled: false, passErr: "password can't be less than 6 charachters"});
    // };

    // if (this.refs.password.value.length > 160) {
    //     this.setState({btnDisabled: false, passErr: "password can't be more than 160 characters"});
    // };

    // if (this.refs.passwordConf.value == "") {
    //     this.setState({btnDisabled: false, passConfErr: "enter your password again"});
    // };

    // if ((this.refs.password.value != "" && this.refs.passwordConf.value != "")&&(this.refs.password.value != this.refs.passwordConf.value)) {
    //     this.setState({btnDisabled: false, passConfErr: "passwords doesn't match"});
    // };

    if (this._pendingState.btnDisabled){
      params = {
        name: this.refs.name.value,
        username: this.refs.username.value,
        email: this.refs.email.value,
        password: this.refs.password.value,
        img: this.refs.img.files[0]
      }
      http.upload("/signup", params, this.onSuccess ,this.onError, 0, null)
    };

    // this.setState({btnDisabled: false});
  },
  onSuccess: function (data, status) {
    this.setState({formStyle: {display:"none"}, msg:{title: "Account Created. Now Check Your Email",
                        body: "We sent you a confirmation link. To continue, please go to your email and click that link."}})
  },
  onError: function (data, status) {  
    if (status == 420) {
       this.setState({btnDisabled: false, nameErr: data.name, usernameErr: data.username, emailErr: data.email, passErr: data.password});
    }
  },

  render: function (){
    return (
      <div>
        <p><span className="bold h4">{this.state.msg.title}</span><span className="error">{this.state.signUpErr}</span></p>
        <p className="bold h5"  >{this.state.msg.body}</p>
        <form onSubmit={this.signup} style={this.state.formStyle}>
          <input type="text" placeholder="Name" ref="name" /><span className="error">{this.state.nameErr}</span><br/>
          <input type="text" placeholder="Username" ref="username" /><span className="error">{this.state.usernameErr}</span><br/>
          <input type="text" placeholder="Email" ref="email" /><span className="error">{this.state.emailErr}</span><br/>
          <input type="password" placeholder="Password" ref="password" /><span className="error">{this.state.passErr}</span><br/>
          <input type="password" placeholder="Password again" ref="passwordConf" /><span className="error">{this.state.passConfErr}</span><br/>
          <input id="imgupload" ref="img" name="img" type="file" /><br />
          <button className="btn" type="submit" disabled={this.state.btnDisabled}>Create my account</button>
        </form>
      </div>
      )
  }
});

module.exports = SignUp;