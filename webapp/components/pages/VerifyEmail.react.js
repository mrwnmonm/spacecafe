var React = require('react');
var http = require('../../lib/http');
var RouteActions = require('../../actions/RouteActions');

var VerifyEmail = React.createClass({
  getInitialState: function() {
    return{title:"",body:""}
  },
  componentWillMount: function() {
    this.verify();
  },
  verify: function(){
    var secret = this.props.params.secret;
    var id = this.props.params.id;
    if ((typeof secret == 'undefined') || (typeof id == 'undefined')){
        RouteActions.redirect("/"); 
    }else{
      http.post("/verify_email", {secret: secret, id: id}, this.onSuccess ,this.onError)
    }
      
  },
  onSuccess: function (data, status) {
      this.setState({title: "Your email successfully confirmed", body: "<strong><a href='#/login'>Go to login page</a></strong>"});
  },
  onError: function  (data, status) {
    if(status == 420){
      this.setState({title: "Expired :(", body: "This confirmation link has expired, <strong><a href='#/reconfirm_email'>request new confirmation link</a><strong>."});
    };
    if (status == 404) {
      this.setState({title: "Bad link", body: "This confirmation link is not valid, <strong><a href='#/reconfirm_email'>request new confirmation link</a><strong>."});
    };
    if (status == 320) {
      RouteActions.redirect('/login');
    }
  },

  render: function (){
    return (
      <div>
        <h3 className="text-center" style={{color:"#333"}}>{this.state.title}</h3>
        <p style={{color:"#777"}} className="text-center" dangerouslySetInnerHTML={{__html: this.state.body}}></p>
      </div>
      )
  }
});

module.exports = VerifyEmail;