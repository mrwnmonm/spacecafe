var React = require('react');
var http = require('../lib/http');
var url = require('../lib/url');
var AccountStore = require('../stores/AccountStore');
var StreamActions = require('../actions/StreamActions');
var StreamStore = require('../stores/StreamStore');
var assign = require('object-assign');
var conf = require('../lib/conf');
var Select = require('react-select');
var Textarea = require('react-textarea-autosize');


var getInitPreview = function(){return ({
  url: "",
  title: "",
  fetchedtitle: "",
  host: "",
  desc: "",
  img: "",
  visible: false
  })
};

var isSelectRequired = function(props){
	return (props.type == "s" || (props.type == "t" && props.id == conf.DefaultTable));
}

var LinkForm = React.createClass({
	getInitialState: function() {
        return {
        type: 10, // 10s for links, 20s for text
		    preview: getInitPreview(),
		    selectRequired: isSelectRequired(this.props),
		    btnDisabled: false,
		    selectAlert: false // used when trying posting before choosing a table
		  }
    },

  selectedTables: "",

  resetPreview: function() {
  	this.setState({preview: getInitPreview()});	
  },

  updatePreview: function(p) {
  	this.setState({preview: assign(this.state.preview, p)});
  },

  titleOnChange: function(){
    var url = this.refs.url.value;
    if (url == "") {
      return;
    };
    var title = this.refs.title.value;
    if (title == ""){
      this.updatePreview({title: this.state.preview.fetchedtitle});
    }else{
      this.updatePreview({title: title});
    }
    // if didn't paset url yet
    // if write then delete, set the fetched title
  },

	urlOnChange: function(){
    var url = this.refs.url.value;
    this.setState({btnDisabled: false});

    // Check if is valid url

    if (url == "") {
      this.resetPreview();
      return;
    };

    // reset preview and set the URL
    if(url != "" && url != this.state.preview.url){
      this.updatePreview({
        url: url, 
        title: "",
        host: "",
        desc: "",
        img: "",
        visible: false
      });

      http.post("/posts/scrape", {url:url}, function(data, status){
        // ensure the preview is for the latest inserted url
        if (data.url == this.state.preview.url) {
          var t = this.refs.title.value;
          var tx = t != "" ? t : data.title;
          this.updatePreview({
            title: tx,
            fetchedtitle: data.title,
            host: data.host,
            desc: data.desc,
            img: data.img,
            visible: true
          });
  
        }
        // if (data.url == this.state.preview.url && data.preview_type == 0) {
        //     this.resetPreview();
        // }
    }.bind(this) ,this.resetPreview, 20000, this.resetPreview);
	
    }
	},

  createPost: function(e) {
    e.preventDefault();

    // Check if table selected
    if (this.state.selectRequired && this.selectedTables == ""){
      this.setState({btnDisabled: false, selectAlert: true});
      return;
    }
    
    // Initialize
    this.setState({btnDisabled: true, selectAlert: false});
    var body = this.refs.postbody.value;
    var url = this.refs.url.value;
    var title = this.refs.title.value;
    var preview = this.state.preview;

    // Validation
    if (this.state.type == 10) {
      // TODO: check if valid url
      if (url == "") {
        this.setState({btnDisabled: false});
        return;
      }
    }else{
      if(body.length > 20000 || body == ""){ // TODO: set the right max body length
        this.setState({btnDisabled: false});
        return;
      }
    }

    var postData = {
      type: this.state.type,
      table: this.selectedTables,
      body: body,
      stage_id: this.props.type == "s" ? this.props.id : 0,
      title: preview.title,
      url: preview.url,
      host: preview.host,
      desc: preview.desc,
      img: preview.img
    };

    http.post("/posts/new", postData, function(data, status) {
      this.refs.postbody.value = "";
      this.refs.postbody.recalculateSize();
      this.refs.url.value = "";
      this.refs.title.value = "";
   		if(this.state.selectRequired){
   			this.selectedTables = "";
   		}
      this.setState({btnDisabled: false});
      this.resetPreview();
      StreamActions.unshift({
        id: data.id,
        body: data.body,
        tables: [postData.table],
        type: data.type,
        title: postData.title,
        url: postData.url,
        host: postData.host,
        description: postData.desc,
        img: postData.img,
        created: data.created,
        comments_count: 0,
        author_name: AccountStore.getName(),
        author_id: AccountStore.getId(),
        author_img: "", // TODO
        author_type: this.props.type == "s" ? "stage" : "user"
      });
    }.bind(this), function(data, status) {
    	//TODO
    	this.setState({btnDisabled: false});
    }.bind(this));
  },

  onSelectTable: function(value){
  	this.selectedTables = value;
  	this.setState({selectAlert: false});
  },

  getOptions: function(input, callback){
    var options;
    http.post("/tables/search", {query: input}, function(data, status){
      options = data.tables;
    } ,function(data, status){})
    setTimeout(function() {
      callback(null, {
            options: options,
            complete: true
        });
    }, 500);
  },

  componentWillReceiveProps: function(nextProps){
  	x = isSelectRequired(nextProps);
  	this.setState({selectRequired: x, selectAlert: false});
  },

  componentWillUpdate: function(nextProps, nextState){
  	if(nextState.selectRequired && this.props.id !== nextProps.id){
  		this.selectedTables = "";
  	}else if (nextProps.type == "t" && nextProps.id != conf.DefaultTable) {
  		this.selectedTables = nextProps.id;
  	};
  },

	render: function  () {
		var selectOptions = [];
		var usertables = AccountStore.getTables(); 
    for (var i = 1; i < usertables.length; i++) { // ignore the first (default) table
    	 selectOptions.push({value: usertables[i], label: usertables[i]});
    };

    var table;
    var selectClass;
    if (this.state.selectRequired){
    	table = "";
    	selectClass = "";
    }else{
    	table = this.props.id;
    	selectClass = "hide";
    }

    var selectTableLableClass = this.state.selectAlert ? "red bold" : "";
    var pvfclass = this.state.preview.visible ? "pvf" : "hide";
    var pvfimgclass = this.state.preview.img != "" ? "pvimgbox" : "hide";

    // just removing the value attribute when there is no value (selectedTables is empty)
    // to show the placeholder when there is no value
    var selectTag;
    switch(this.props.type){
      case "s":
        if(this.selectedTables == ""){
          selectTag = <Select name="tableselector" multi={true} asyncOptions={this.getOptions} autoload={false} className={selectClass} matchProp="value" matchPos="any" options={selectOptions} onChange={this.onSelectTable} />;      
        }else{
          selectTag = <Select name="tableselector" multi={true} asyncOptions={this.getOptions} autoload={false} value={this.selectedTables} className={selectClass} matchProp="value" matchPos="any" options={selectOptions} onChange={this.onSelectTable} />;
        }
      break;
      case "t":
        if(this.selectedTables == ""){
          selectTag = <Select name="tableselector" className={selectClass} matchProp="value" matchPos="any" options={selectOptions} onChange={this.onSelectTable} />;      
        }else{
          selectTag = <Select name="tableselector" value={this.selectedTables} className={selectClass} matchProp="value" matchPos="any" options={selectOptions} onChange={this.onSelectTable} />;
        }
      break;
    }
    

		return (
      <div id="pstfrm" className="lnkfrm">
        <form>
          <Textarea onChange={this.postOnChange} ref="postbody" placeholder="Share something with the table"></Textarea>
          <table>
            <tr>
              <td className="lftdl">
                <label>Link<span className="lftdls">:</span></label>
              </td>
              <td>
                <input type="text" onChange={this.urlOnChange} placeholder="Paste your link here" ref="url" />
              </td>
            </tr>
            <tr>
              <td className="lftdl">
                <label>Title<span className="lftdls">:</span></label>
              </td>
              <td>
                <input type="text" onChange={this.titleOnChange} placeholder="Add custom title (optional)" ref="title" style={{color: "#333"}} />
              </td>
            </tr>
          </table>
        </form>

          <div className={pvfclass}>
          <table>
            <tr>
              <td style={{display: "inline-block"}}>
                <div className={pvfimgclass}>
                  <div className="pvfimgclose">
                    <a onClick={function(){this.updatePreview({img:""})}.bind(this)}>remove</a>
                  </div>
                  <img src={this.state.preview.img} />
                </div>
              </td>
              <td>
               <div className="pvmeta">
                  <div className="pvtitle">{StreamStore.getNicePreviewTitle(this.state.preview.title, this.state.preview.haveimg)}</div>
                  <div className="pvdisc">{StreamStore.getNicePreviewDesc(this.state.preview.desc, this.state.preview.haveimg)}</div>
                  <div className="pvhost">{this.state.preview.host}</div>
                </div>
              </td>   
            </tr>
          </table>
          </div> 
        

        <div id="ctrl">
        <div className="selectbox">
          <span className="lbl"><span className={selectTableLableClass}>Table:</span> <span className="bold">{table}</span></span>
          {selectTag}
        </div>
          <button className="pstbtn btn right" type="button" disabled={this.state.btnDisabled} onClick={this.createPost}>Post</button>
        </div>
      </div>
		)
	}
});

module.exports = LinkForm;
