var React = require('react');
var Post = require('./Post.react');
var InfiniteScroll = require('react-infinite-scroll')(React);
var StreamStore = require('../stores/StreamStore');
var StreamActions = require('../actions/StreamActions');
var http = require('../lib/http');


function getStateFromStores() {
  return {
  	posts: StreamStore.getPosts()
  };
}

var Stream = React.createClass({
	  getInitialState: function() {
      return getStateFromStores();
    },
    page: 0,
    hasMore: true,
    componentDidMount: function() {
      StreamStore.addChangeListener(this._onChange);
      document.addEventListener('click', this.handleBodyClick);
      StreamActions.clear();
    },

    handleBodyClick: function(e) {
      if (e.target.matches('html') || e.target.matches('body') || e.target.matches('#app') || e.target.matches('.content') || e.target.matches('.col2')) {
        StreamActions.collapseAll();
      };
    },

    componentWillUnmount: function() {
      StreamStore.removeChangeListener(this._onChange);
    },

    loadPosts: function(){
      if(!this.hasMore){return;}
      var type = this.props.type;
      var id = this.props.id.toLowerCase();
      var request_id = type + ":" + id.toString() + ":" + this.page.toString();
      var jsonData = {page: this.page, id: id, type: type, request_id: request_id};
      this.page = this.page + 1;
      var that = this;
      http.post("/posts/index", jsonData ,function(data, status) {
      if(data.request_id == request_id && that.props.id.toLowerCase() == data.request_id.split(":")[1]){
        StreamActions.push(data.posts);
      }
      // TODO: make "5" a global variable or some kind of configrations
      if (data.posts.length < 5) {
          that.hasMore = false;
      };
      });
      
    },

    componentDidUpdate: function(prevProps){
      if(this.props.id != prevProps.id){
        StreamActions.clear();
        this.page = 0;
        this.hasMore = true;
        this.loadPosts();
      }  
    },

	render: function  () {
		return (
			<div id="strm" className="col">
        <InfiniteScroll
            pageStart={this.page}
            loadMore={this.loadPosts}
            hasMore={this.hasMore}
            threshold={100}
            loader={<div className="loader"></div>}>
          <ul className="list cleanlist">
           {this.state.posts.map(function(post, i) {
              return ( 
                <li className={"postbox " + post.expandclass.join(" ")} key={post.id}><Post type={"stream"} post={post} comments={post.comments} index={i} /></li>
              );
            })}
          </ul>
        </InfiniteScroll>
              
            </div>
		)
	},
	_onChange: function() {
    this.setState(getStateFromStores());
  }
});

module.exports = Stream;