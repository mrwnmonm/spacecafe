var React = require('react');
var AccountStore = require('../stores/AccountStore');
var StreamStore = require('../stores/StreamStore');
var StreamActions = require('../actions/StreamActions');
var http = require('../lib/http');
var date = require('../lib/date');
var Link = require('react-router').Link;
var History = require('react-router').History;
var Textarea = require('react-textarea-autosize');

var Post = React.createClass({
    mixins: [History],
    getInitialState: function() {
        if (this.props.type == "show") {
            this.getCommnets();
        }
        return {
            replyto: 0,
            comments: [],
            commentFormExpanded: false,
            viewastree: this.props.type == "stream" ? false : true
        }
    },


    // indexes to save all comments according to their depth
    index1: [], // comments indexes that have depth equal 1, to decrease processing time
    indexIds: {}, // map comments ids to thier index in the comments array
    childernIdx: {}, // map comments ids with thier direct childern indexes
    movedUnderParent: [], // comments ids that moved under parents
    // shownCommentsCount: 0,
    commentLengthLimit: 320,


    reindex: function(comments) {
        var that = this;
        that.indexIds = {};
        that.index1 = [];
        that.childernIdx = {};

        comments.map(function(comment, i) {
            that.indexIds[comment.id] = i;

            if (comment.parent_id != 0) {
                that.childernIdx[comment.parent_id] = that.childernIdx[comment.parent_id] || [];
                that.childernIdx[comment.parent_id].push(i);
            }
            ;

            if (comment.depth == 0) {
                that.index1.push(i)
            }
        });
    },

    getCommnets: function() {
        var that = this;
        http.post('/comments/index', {
            post_id: this.props.post_id
        }, function(data, status) {
            var comments = data;
            that.reindex(comments);
            if (!that.state.viewastree) {
                that.collapse(1, comments);
            } else {
                that.setState({
                    comments: comments
                });
            }
        }, function(data, status) {});
    },

    showComments: function(e, postId, index) {
      if (this.props.type != "stream") {return}
      if(!e.target.matches('.show') && !e.target.matches('p')){return}
      if (!this.props.post.expanded) {
        http.post("/comments/indexflat", {post_id: postId}, function(data, status) {
         // StreamActions.update(index, {comments: data});
         this.setState({comments: data});
         // this.reindex(data);
        }.bind(this), function(data, status) {

        });
      }
      StreamActions.expand(index);
      this.setState({expanded: !this.props.post.expanded, commentFormExpanded: false})
    },

    comment: function(e, hasParent, parentdepth, parentindex) {
        e.preventDefault();
        var depth = hasParent ? (parentdepth + 1) : 0;
        var index = hasParent ? (parentindex + 1) : 0; // parent comment show at the top after posting
        var parent_id = hasParent ? this.state.replyto : 0;
        var body = hasParent ? this.refs.replybody : this.refs.commentbody;
        var that = this;
        if (body.value != "") {
            var jsonData = {
                post_id: this.props.type == "stream" ? this.props.post.id : this.props.post_id,
                parent_id: parent_id,
                body: body.value
            };
            http.post('/comments', jsonData, function(data, status) {
                var comment = {
                    id: data.id,
                    body: data.body,
                    user_id: AccountStore.getId(),
                    user_name: AccountStore.getName(),
                    parent_id: parent_id,
                    depth: depth,
                    visible: true,
                    expanded: false,
                    replies_count: 0,
                    created: data.created
                };

                // this should run before changing the state
                // because when setting replyto to 0, the ref will not exist and getDOMNode will cause error
                body.value = "";

                var c = that.state.comments;
                c.splice(index, 0, comment)

                // reindex
                that.reindex(c);

                if (hasParent) {
                    c[parentindex].replies_count = c[parentindex].replies_count + 1;

                    // we show all replies for the parent comment to show all the disscution
                    // and to avoid the following problem
                    // "the new reply goes under the parent, so the "hide replies" appears and "show replies" disappear, when the replies are already hidden" because we check if childernVisible with the first child visiability
                    that.toggleChildern(comment.parent_id, 1);
                } else {
                    // TODO: increase comments count for the post
                }

                that.setState({
                    comments: c,
                    replyto: 0,
                    commentFormExpanded: false
                });

                if (that.props.type == "stream") {
                    StreamActions.increaseComments(that.props.index);
                };

            }, function(data, status) {});
        }
    },

    like: function(id, index) {
        if (!this.props.post.liked) {
            StreamActions.update(index, {likes: this.props.post.likes + 1, liked: true});
            http.post("/vote", {type: "0", id: id}, function(data, status) {});
        }else{
            StreamActions.update(index, {likes: this.props.post.likes - 1, liked: false});
            http.post("/vote", {type: "1", id: id}, function(data, status) {});
        }
    },

    vote: function(id, index, type){
      switch (type) {
        case 0:
            StreamActions.update(index, {votes: this.props.post.votes + 1, voted: "up"});
            break;
        case 1:
            StreamActions.update(index, {votes: this.props.post.votes - 1, voted: "down"});
            break;
        case 2:
           
            break;
        case 3:
           
            break;
        }
        http.post("/vote", {type: type, id: id}, function(data, status) {}); 
    },

    moveUnderParent: function(parentid, index) {
        var parentindex = this.indexIds[parentid];
        var middleindexes = [];
        var comments = this.state.comments;
        for (var i = parentindex + 1; i < index; i++) {
            middleindexes.push(i);
        }
        ;
        middleindexes.map(function(i) {
            comments[i].visible = false;
        })
        this.movedUnderParent.push(comments[index].id);
        this.setState({
            comments: comments
        });
    },

    // 0: show all childern
    // 1: hide all childern
    _toggleChildern: function(comments, id, type) {
        var that = this;
        if (typeof this.childernIdx[id] !== "undefined") {

            switch (type) {
            case 0:
                this.childernIdx[id].map(function(idx) {
                    comments[idx].visible = true;

                    // remove id from moved under parent to show the move button again
                    for (i in that.movedUnderParent) {
                        if (comments[idx].id == that.movedUnderParent[i]) {
                            that.movedUnderParent.splice(i, 1)
                        }
                    }
                    that._toggleChildern(comments, comments[idx].id, type);
                })
                break;
            case 1:
                this.childernIdx[id].map(function(idx) {
                    comments[idx].visible = false;
                    that._toggleChildern(comments, comments[idx].id, type);
                })
                break;
            }


        }
        return comments;
    },

    // 0: show direct childern
    // 1: show all childern
    // 2: hide all childern
    toggleChildern: function(id, type) {
        if (this.props.type == "stream") {
            // TODO: show reply form on the post page
            this.history.pushState(null, '/post/' + this.props.post.id + "#c" + id);
        };
        var c = this.state.comments;

        if (typeof this.childernIdx[id] == "undefined") {
            return;
        }

        switch (type) {
        case 0:
            this.childernIdx[id].map(function(idx) {
                c[idx].visible = true;
            })
            break;
        case 1:
            c = this._toggleChildern(c, id, 0);
            break;
        case 2:
            c = this._toggleChildern(c, id, 1);
            break;
        }


        this.setState({
            comments: c
        });
    },


    collapse: function(level, comments) {
        // hide comments with have depth more than level number
        var that = this;
        var comments = comments || that.state.comments;

        switch (level) {
        case 0:
            comments.map(function(comment) {
                comment.visible = true;
            });
            that.movedUnderParent = [];
            this.setState({
                comments: comments,
                viewastree: true
            });
            break;
        case 1:
            comments.map(function(comment) {
                comment.visible = false;
            });
            this.index1.map(function(i) { // i -> index in this.state.comments array
                comments[i].visible = true;
            })
            this.setState({
                comments: comments,
                viewastree: false
            });
            break;
        }
    },

    expand: function(index) {
        var c = this.state.comments;
        c[index].expanded = !c[index].expanded;
        this.setState({
            comments: c
        });
    },

    expandCommentForm: function() {
        this.setState({commentFormExpanded: true})
    },

    showReply: function (commentId) {
        if (this.props.type == "stream") {
            this.history.pushState(null, '/post/' + this.props.post.id + "#c" + commentId);
        };
        this.setState({replyto: commentId})
    },

    componentDidUpdate: function(prevProps, prevState) {
        if (prevState.replyto != this.state.replyto && this.state.replyto != 0) {
            this.refs.replybody.focus();
        }
    },

    render: function() {
        var that = this;
        var post = this.props.post;
        var comments_count = post.comments_count;
        if (Object.keys(post).length == 0 || typeof post == "undefined") {
            return <div></div>
        }

        return (
            <div className={post.liked ? "post liked" : "post"}>
              <div className="posthead show" onClick={function(e){that.showComments(e, post.id, that.props.index)}}>
                <div className="postmeta show">
                  <span className="img show"><img className="show" src={ "/public/img/users/" + post.user_id + ".png" } /></span>
                  <div className="name show">
                    <a href={ "/user/" + post.user_id }>
                      { post.user_name }
                    </a>
                  </div>
                </div>
                <div className="body show" dangerouslySetInnerHTML={ {    __html: post.body} }></div>
                <div className="postactions show">
                
                  <span><span className="action voteaction upvote" onClick={ function(){that.like(post.id, that.props.index)} }><i className="fa fa-angle-up"></i></span></span>
                  <span className={ post.likes == 0 ? "hide" : "points"} onClick={function(){that.like(post.id, that.props.index)}}>{ post.likes == 0 ? "" : post.likes }</span>
                  
                  <span className="separator">•</span>
                  <span><span className="action" onClick={ function(){that.showComments(post.id, that.props.index)} }>{ (comments_count > 0 ? comments_count + " " : "") + (comments_count > 1 ? "Comments" : "Comment") }</span></span>
                  <span className="separator">•</span>
                  <Link className="action" to={ "/post/" + post.id }>
                    { date.getRelativeTime(new Date(post.created)) }
                  </Link>
                  <span className="separator">•</span>
                  <Link className="action" to={ "/" + post.table }>
                    { post.table }
                  </Link>
                </div>
              </div>
              <div className={this.props.type == "show" ? "cmntsbox" : "cmntsbox strmcmnts" }>
                <form className={comments_count == 0 ? "cmntform nocmnts" : "cmntform"} onSubmit={ function(e){that.comment(e, false)} }>
                    <span className="cmntfrmcaret"></span>
                    <img className="cmntrimg" src={ '/public/img/users/' + AccountStore.getId() + '.png' } />
                    <Textarea onClick={this.expandCommentForm} rows={ 1 } ref="commentbody" placeholder="Add Comment ..."></Textarea>
                    <div className={this.state.commentFormExpanded ? "form-ctrl-post" : "hide"}>
                        <button className="btn" type="submit">Reply</button>
                        <button className="btn" type="button" onClick={ function(){that.setState({commentFormExpanded: false})} }>Cancel</button>    
                    </div>
                </form>
                <div className="cmntsinfo">
                  <span>{ comments_count } Comments</span>
                </div>
                <div className="viewcontrollers">
                  <span>View :</span>
                  <button className={ that.state.viewastree ? "btn active" : "btn" } type="button" onClick={ function(){that.collapse(0)} }>Tree</button>
                  <button className={ that.state.viewastree ? "btn" : "btn active" } type="button" onClick={ function(){that.collapse(1)} }>One by One</button>
                </div>
                <ul className={ this.state.comments.length > 0 ? "cleanlist cmnts" : "hide" }>
                  { this.state.comments.map(function(comment, index) {
                    
                        var childernVisible = false;
                        if (typeof that.childernIdx[comment.id] !== "undefined") {
                            childernVisible = that.state.comments[that.childernIdx[comment.id][0]].visible;
                        }
                    
                        var isUnderParent = true;
                    
                        if (comment.parent_id != 0) {
                            var parentindex = that.indexIds[comment.parent_id];
                            if (parentindex != (index - 1)) {
                                isUnderParent = false;
                            }
                        }
                    
                        // see if it is already moved under parent to hide the move button
                        for (i in that.movedUnderParent) {
                            if (comment.id == that.movedUnderParent[i]) {
                                isUnderParent = true
                            }
                        }
                    
                        var replyform = "";
                        if (that.state.replyto == comment.id) {
                            replyform = <form className="rplform" onSubmit={ function(e){that.comment(e, true, comment.depth, index)} }>
                                          <Textarea rows={ 1 } ref="replybody" placeholder="Add Comment ..."></Textarea>
                                          <div className="form-ctrl-post">
                                            <button className="btn" type="submit">Reply</button>
                                            <button className="btn" type="button" onClick={ function(){that.setState({replyto: 0})} }>Cancel</button>
                                          </div>
                                        </form>;
                        }
                    
                        var body = comment.expanded || (comment.body.length < that.commentLengthLimit) ? comment.body : comment.body.slice(0, that.commentLengthLimit) + "...";
                        var liclass = comment.parent_id == 0 ? "cmnt topparent" : "cmnt"
                        return (
                            <li id={ "c" + comment.id } key={ comment.id } className={ comment.visible ? liclass : "hide" } style={{ paddingLeft: 40 * (comment.depth + 1)} }>
                              <img className="cmntrimg" src={ "/public/img/users/" + comment.user_id + ".png" } style={{left: 40 * comment.depth}} />
                              <div className="cmntcntnt">
                                <span className="cmntr">{ comment.user_name }</span>
                                <div className="cmntbody" dangerouslySetInnerHTML={ {    __html: body} }></div>
                                <button className={ comment.body.length < that.commentLengthLimit ? "hide" : "showmore btn" } type="button" onClick={ function(){that.expand(index)} }>
                                  { comment.expanded ? "Show Less" : "Show More" }
                                </button>
                              </div>
                              <div className="cmntsctrl">
                                <span><span className="action voteaction upvote" onClick={ function(){that.vote(comment.id, index, 2)} }><i className="fa fa-angle-up"></i></span></span>
                                <span className="points">{ comment.votes }</span>
                                <span><span className="action voteaction downvote" onClick={ function(){that.vote(comment.id, index, 3)} }><i className="fa fa-angle-down"></i></span></span>
                  
                                <button className="btn" type="button" onClick={function(){that.showReply(comment.id)}}><span className="separator">•</span>Reply</button>
                                <span className="separator">•</span>
                                <Link className="cmntdate" to={ "/post/" + post.id + "#c" + comment.id }>
                                  { date.getRelativeTime(new Date(comment.created)) }
                                </Link>
                                <button type="button" onClick={ function(){that.moveUnderParent(comment.parent_id, index);} } className={ that.state.viewastree && !isUnderParent ? "btn" : "hide" }><span className="separator">•</span>Move Under Parent</button>
                                <button type="button" onClick={ function(){that.toggleChildern(comment.id, 0)} } className={ that.state.viewastree || (comment.replies_count == 0 || childernVisible) ? "hide" : "btn" }><span className="separator">•</span>
                                  { comment.replies_count }
                                  { comment.replies_count > 1 ? " Replies" : " Reply" }
                                </button>
                                <button type="button" onClick={ function(){that.toggleChildern(comment.id, 1)} } className={ that.state.viewastree && (comment.replies_count != 0) && !childernVisible ? "btn" : "hide" }><span className="separator">•</span>Show All Replies</button>
                                <button type="button" onClick={ function(){that.toggleChildern(comment.id, 2)} } className={ (comment.replies_count == 0) || (!childernVisible) ? "hide" : "btn" }><span className="separator">•</span>Hide Replies</button>
                              </div>
                              { replyform }
                            </li>
                            )
                    }) }
                </ul>

                <div className={ (comments_count > 5 && this.props.type == "stream") ? "readall" : "hide"}>
                    <Link to={ "/post/" + post.id}>See All Comments ({comments_count})</Link>
                </div>
              </div>
            </div>
            );
    }
});

module.exports = Post;
