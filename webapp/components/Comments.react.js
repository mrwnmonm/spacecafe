var React = require('react');
var AccountStore = require('../stores/AccountStore');
var StreamStore = require('../stores/StreamStore');
var http = require('../lib/http');
var date = require('../lib/date');
var Textarea = require('react-textarea-autosize');

var Post = React.createClass({

  getInitialState: function() {
    if (this.props.type == "show") {
      this.getCommnets();
    };
    return {replyto: 0, comments: [], viewastree: true}
  },


  // indexes to save all comments according to their depth
  index1: [], // comments indexes that have depth equal 1, to decrease processing time
  indexIds: {}, // map comments ids to thier index in the comments array
  childernIdx: {}, // map comments ids with thier direct childern indexes
  movedUnderParent: [], // comments ids that moved under parents
  // shownCommentsCount: 0,
  commentLengthLimit: 320,


  reindex: function(comments){
    var that = this;
    that.indexIds = {};
    that.index1 = [];
    that.childernIdx = {};

    comments.map(function(comment, i){
      that.indexIds[comment.id] = i;

      if (comment.parent_id != 0) {
        that.childernIdx[comment.parent_id] = that.childernIdx[comment.parent_id] || [];
        that.childernIdx[comment.parent_id].push(i);
      };
      
      if(comment.depth == 0){that.index1.push(i)}
    });
  },

  getCommnets: function(){
    var that = this;
    http.post('/comments/index', {post_id: this.props.post_id}, 
    function(data, status){ 
      var comments = data;
      that.reindex(comments);
      if (!that.state.viewastree) {
        that.collapse(1, comments);
      }else{
        that.setState({comments: comments}); 
      }
    }, function(data, status){});
  },

  // showComments: function(postId, index) {
  //   http.post("/comments/index", {post_id: postId}, function(data, status) {
  //      StreamActions.update(index, {comments: data});
  //     }, function(data, status) {
        
  //     });
  // },

  comment: function(e, hasParent, parentdepth, parentindex){
    e.preventDefault();
    var depth = hasParent ? (parentdepth + 1) : 0;
    var index = hasParent ? (parentindex + 1) : 0; // parent comment show at the top after posting
    var parent_id = hasParent ? this.state.replyto : 0;
    var body = hasParent ? this.refs.replybody : this.refs.commentbody;
    var that = this;
    if (body.value != "") {
      var jsonData = {post_id: this.props.post_id, 
                      parent_id: parent_id, 
                      body: body.value
                    };
      http.post('/comments', jsonData, function(data, status){ 
        var comment = {id: data.id,
                       body: data.body,
                       author_id: AccountStore.getId(),
                       author_name: AccountStore.getName(),
                       parent_id: parent_id,
                       depth: depth,
                       visible: true,
                       expanded: false,
                       replies_count: 0,
                       created: data.created};
        
        // this should run before changing the state
        // because when setting replyto to 0, the ref will not exist and getDOMNode will cause error
        body.value = ""; 
        body.recalculateSize();

        var c = that.state.comments;
        c.splice(index, 0, comment)

        // reindex
        that.reindex(c);

        if (hasParent) {
          c[parentindex].replies_count = c[parentindex].replies_count + 1;
          
          // we show all replies for the parent comment to show all the disscution
          // and to avoid the following problem
          // "the new reply goes under the parent, so the "hide replies" appears and "show replies" disappear, when the replies are already hidden" because we check if childernVisible with the first child visiability
          that.toggleChildern(comment.parent_id, 1);
        }else{
          // TODO: increase comments count for the post
        }

        that.setState({comments: c, replyto: 0});

      }, function(data, status){});
    }
  },

  // vote: function(postId, index, voteUp){
  //   if(voteUp){
  //     StreamActions.update(index, {likes_count: this.props.post.likes_count + 1});
  //   }else{
  //     StreamActions.update(index, {likes_count: this.props.post.likes_count - 1});
  //   } 
  // },

  moveUnderParent: function(parentid, index){
    var parentindex = this.indexIds[parentid];
    var middleindexes = [];
    var comments = this.state.comments;
    for (var i = parentindex + 1; i < index; i++) {
      middleindexes.push(i);
    };
    middleindexes.map(function(i){
      comments[i].visible = false;
    })
    this.movedUnderParent.push(comments[index].id);
    this.setState({comments: comments});
  },

  // 0: show all childern
  // 1: hide all childern
  _toggleChildern: function(comments, id, type){ 
    var that = this;
    if (typeof this.childernIdx[id] !== "undefined") {

      switch(type){
        case 0:
          this.childernIdx[id].map(function(idx){
            comments[idx].visible = true;

            // remove id from moved under parent to show the move button again
            for(i in that.movedUnderParent){
              if (comments[idx].id == that.movedUnderParent[i]){that.movedUnderParent.splice(i, 1)}
            } 
            that._toggleChildern(comments, comments[idx].id, type);
          })
        break;
        case 1:
          this.childernIdx[id].map(function(idx){
            comments[idx].visible = false;
            that._toggleChildern(comments, comments[idx].id, type);
          })
        break;
      }

      
    }
    return comments;
  },

  // 0: show direct childern
  // 1: show all childern
  // 2: hide all childern
  toggleChildern: function(id, type){
    var c = this.state.comments;

    if (typeof this.childernIdx[id] == "undefined") {
      return;
    }

    switch(type){
      case 0:
        this.childernIdx[id].map(function(idx){
          c[idx].visible = true;
        })
      break;
      case 1:
        c = this._toggleChildern(c, id, 0);
      break;
      case 2:
        c = this._toggleChildern(c, id, 1);
      break;
    }

    
    this.setState({comments: c});
  },


  collapse: function(level, comments){
    // hide comments with have depth more than level number
    var that = this;
    var comments = comments || that.state.comments;

    switch (level){
      case 0:
        comments.map(function(comment){
          comment.visible = true;
        });
        that.movedUnderParent = [];
        this.setState({comments: comments, viewastree: true});
      break;
      case 1:
        comments.map(function(comment){
          comment.visible = false;
        }); 
        this.index1.map(function(i){ // i -> index in this.state.comments array
          comments[i].visible = true;
        })
        this.setState({comments: comments, viewastree: false});
      break;
    }
  },

  expand: function(index){
    var c = this.state.comments;
    c[index].expanded = !c[index].expanded;
    this.setState({comments: c});
  },

  render: function(){
    var that = this;
    var post = this.props.post;
    if (Object.keys(post).length == 0 || typeof post == "undefined"){return <div></div>};
    var preview;
    var pvmetaclass = post.img != "" ? "pvmeta" : "pvmeta full";
    switch(post.preview_type) {
        case 0:
            preview = "";
            break;
        case 1:
            preview = <div className="pv">
                        <div className={pvmetaclass}>
                          <div className="pvtitle">
                            <a href={post.url} target="_blank">{StreamStore.getNicePreviewTitle(post.title, (post.img != ""))}</a>
                          </div>
                          <div className="pvdisc">{StreamStore.getNicePreviewDesc(post.description, (post.img != ""))}</div>
                          <div className="pvhost">{post.host}</div>
                        </div>
                      </div>;
            break;
        default:
            preview = <div className="pv">
                        <div className="pvimgbox">
                          <img src={post.img} />
                        </div>
                        <div className={pvmetaclass}>
                          <div className="pvtitle">
                            <a href={post.url} target="_blank">{StreamStore.getNicePreviewTitle(post.title, (post.img != ""))}</a>
                          </div>
                          <div className="pvdisc">{StreamStore.getNicePreviewDesc(post.description, (post.img != ""))}</div>
                          <div className="pvhost">{post.host}</div>
                        </div>
                      </div>;
    }

      return ( 
        <div className="post">
          <div className="posthead">
            <div className="postmeta">
              <span className="img"><img src={"/public/img/users/"+ post.author_id + "_32.jpg"} /></span>
              <div className="name"><a href={(post.author_type == "user" ? ("/user/") : ("/stage/")) + post.author_id}>{post.author_name}</a></div>
            </div>
            <div className="body" dangerouslySetInnerHTML={{__html: post.body}}></div>
            {preview}
          </div>
          <div className="postactions">
            <span><span className="action voteaction" onClick={function(){that.vote(post.id, that.props.index, true)}}><i className="fa fa-angle-up"></i></span></span>
            <span className="points">{post.likes_count}</span>
            <span><span className="action voteaction" onClick={function(){that.vote(post.id, that.props.index, false)}}><i className="fa fa-angle-down"></i></span></span>
            <span className="separator">•</span>
            <span><span className="action" onClick={function(){that.showComments(post.id, that.props.index)}}>Comment</span><span className={post.comments_count > 0 ? "count" : "hide"}>{post.comments_count}</span></span>
            <span className="separator">•</span>
            <a className="action" href={"/post/" + post.id}>{date.getRelativeTime(new Date(post.created))}</a>
            <span className="separator">•</span>
            {
              post.tables.map(function(table){
                return (
                <a  className="action" key={post.id.toString + table} href={"/#/"+ table}>{table}</a>   
                )
              })
            }
          </div>
          <div className="posttail">

             <form className="cmntform" onSubmit={function(e){that.comment(e, false)}}>
              <Textarea ref="commentbody" placeholder="Add Comment ..."></Textarea>
              <button className="bluebtn btn" type="submit">Reply</button>
            </form>
            <div className="cmntsinfo">
              <span>{post.comments_count} Comments</span>
            </div>
            <div className="viewcontrollers">
              <span>View :</span>
              <button className={that.state.viewastree ? "btn active" : "btn"} type="button" onClick={function(){that.collapse(0)}}>Tree</button>
              <button className={that.state.viewastree ? "btn" : "btn active"} type="button" onClick={function(){that.collapse(1)}}>One by One</button>
            </div>
            <ul className={this.state.comments.length > 0 ? "cleanlist cmnts" : "hide"}>
              {
                this.state.comments.map(function(comment, index){

                  var childernVisible = false;
                  if (typeof that.childernIdx[comment.id] !== "undefined") {
                    childernVisible =  that.state.comments[that.childernIdx[comment.id][0]].visible;
                  }

                  var isUnderParent = true;

                  if (comment.parent_id != 0) {
                    var parentindex = that.indexIds[comment.parent_id];
                    if (parentindex != (index - 1)) {
                      isUnderParent = false;
                    }
                  }

                  // see if it is already moved under parent to hide the move button
                  for(i in that.movedUnderParent){
                    if (comment.id == that.movedUnderParent[i]){isUnderParent = true}
                  } 
                  
                  var replyform = "";
                  if(that.state.replyto == comment.id){
                    replyform = <form className="rplform" onSubmit={function(e){that.comment(e, true, comment.depth, index)}}>
                                <Textarea ref="replybody" placeholder="Add Comment ..."></Textarea>
                                <button className="btn" type="submit">Reply</button>
                                <button className="btn" type="button" onClick={function(){that.setState({replyto: 0})}}>Cancel</button>
                                </form>;
                  }
                 
                  var body = comment.expanded || (comment.body.length < that.commentLengthLimit) ? comment.body : comment.body.slice(0,that.commentLengthLimit) + "...";
                  var liclass = comment.parent_id == 0 ? "cmnt topparent" : "cmnt"
                  return (
                    <li id={"c" + comment.id} key={comment.id} className={comment.visible ? liclass : "hide"} style={{marginLeft: 40 * comment.depth}}>
                      <img className="cmntrimg" src="/public/img/img2.png"  />
                      <div className="cmntcntnt">
                        <span className="cmntr">{comment.author_name}</span>
                        <div className="cmntbody" dangerouslySetInnerHTML={{__html: body}}></div>
                        <button className={comment.body.length < that.commentLengthLimit ? "hide" : "showmore btn"} type="button"  onClick={function(){that.expand(index)}}>{comment.expanded ? "Show Less" : "Show More"}</button>
                      </div>
                      <div className="cmntsctrl">
                        <button className="btn" type="button">Like</button>
                        <button className="btn" type="button" onClick={function(){that.setState({replyto: comment.id})}}><span className="separator">•</span>Reply</button>  
                        <span className="separator">•</span><a className="cmntdate" href={"/post/" + post.id + "#c" + comment.id}>{date.getRelativeTime(new Date(comment.created))}</a>
                        <button type="button" onClick={function(){that.moveUnderParent(comment.parent_id, index);}} className={that.state.viewastree && !isUnderParent ? "btn" : "hide"}><span className="separator">•</span>Move Under Parent</button>
                        <button type="button" onClick={function(){that.toggleChildern(comment.id, 0)}} className={that.state.viewastree || (comment.replies_count == 0 || childernVisible) ? "hide" : "btn"}><span className="separator">•</span>{comment.replies_count} Replies</button>
                        <button type="button" onClick={function(){that.toggleChildern(comment.id, 1)}} className={that.state.viewastree && (comment.replies_count != 0) && !childernVisible ? "btn" : "hide"}><span className="separator">•</span>Show All Replies</button>
                        <button type="button" onClick={function(){that.toggleChildern(comment.id, 2)}} className={(comment.replies_count == 0) || (!childernVisible) ? "hide" : "btn"}><span className="separator">•</span>Hide Replies</button>
                      </div>

                      
                    
                      {replyform}

                    </li>  
                  )
                })
              }
            </ul>

          </div>
        </div>
  );
}});

module.exports = Post;
