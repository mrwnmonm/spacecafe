var React = require('react');
var AccountStore = require('../stores/AccountStore');
var AccountActions = require('../actions/AccountActions');
var http = require('../lib/http');
var Scrollbars = require('react-custom-scrollbars').Scrollbars;
var History = require('react-router').History;
var Link = require('react-router').Link;

var Navbar = React.createClass({
    mixins: [History],
    _top: 0,
    getInitialState: function() {
        this.fireNotifications();
        return {
          notificationsCount: AccountStore.getNotificationsCount(),
          notificationsMenuVisible: false
        }
    },
    longPollingEnabled: false,
    logout: function(e) {
        e.preventDefault();
        AccountStore.logout();
        this.longPollingEnabled = false;
        this.history.pushState(null, '/welcome');
    },

    connect: function(date) {
      if (!AccountStore.isLoggedIn()) {return};
      // date = 1450041008823;

      http.post("/connect?timeout=600" + "&category=" + AccountStore.getId() + "&since_time=" + date.toString(), {}, function(data, status) {
          if (!AccountStore.isLoggedIn()) {return}; // in case the user logout while the reqeust is still pending

          // TODO: handel all situations
          AccountActions.increaseNotifications(data.events.length);
          this.connect(data.events[data.events.length - 1].timestamp);
      }.bind(this))
    },
    fireNotifications: function(){
      if (AccountStore.isLoggedIn() && !this.longPollingEnabled) {
          this.longPollingEnabled = true;
          this.checkNotifications();
          this.connect(Date.now());
        }
    },
    componentWillUpdate: function() {
        this.fireNotifications();
    },
    handleNotificationScroll: function(e) {
      var scrollarea = this.refs.notificationslist.parentNode;
      var dir =  (e.detail<0) ? 1 : (e.wheelDelta>0) ? 1 : -1;
      if ((dir == 1 && scrollarea.scrollTop == 0) || (dir == -1 && scrollarea.scrollTop == scrollarea.scrollHeight-scrollarea.clientHeight)){
        e.preventDefault();
      }
    },
    handleGlobalClick: function(e){
      if (!e.target.matches('.dropbtn')) {
        this.setState({notificationsMenuVisible: false});
      }
    },
    componentDidMount: function() {
        this.refs.notificationslist.parentNode.addEventListener('scroll', this.handleNotificationScroll);
        this.refs.notificationslist.parentNode.addEventListener('wheel', this.handleNotificationScroll);
        document.addEventListener('click', this.handleGlobalClick);
        AccountStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function() {
        this.refs.notificationslist.parentNode.removeChangeListener('scroll', this.handleNotificationScroll);
        this.refs.notificationslist.parentNode.removeChangeListener('wheel', this.handleNotificationScroll);
        document.addEventListener('click', this.handleGlobalClick);
        AccountStore.removeChangeListener(this._onChange);
    },
    showNotificationsMenu: function(){
      if (this.state.notificationsCount > 0) {
        http.post("/notifications/read", {}, function(data, status) {
            AccountActions.setNotifications(0);
        })
      };
      this.setState({notificationsMenuVisible: true});
    },
    _onChange: function() {
        this.setState({
          notificationsCount: AccountStore.getNotificationsCount()
        })
    },
     checkNotifications: function(){
      http.post("/notificationscount", {}, function(data, status) {
            AccountActions.setNotifications(data.unread_notifications);
      })
    },
    render: function() {
        return (
            <div id="navbar">
              <div className="container">
                <Link to="/"><span id="logo">Spacecafe</span></Link>
                <ul className={ AccountStore.isLoggedIn() ? "navgroup left" : "hide" }>
                  <li>
                    <Link to="/">
                    <span className="tab">Home</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="/table/new">
                    <span className="tab">Explore</span>
                    </Link>
                  </li>
                  <li>
                    <a href="">
                      <span className="tab" onClick={ this.logout }>Logout</span>
                    </a>
                  </li>
                  <li>
                    <Link to="" className="dropbtn" onClick={this.showNotificationsMenu}>
                      <span className="tab dropbtn">Notifications</span>
                      <span id="notificationscount" className={this.state.notificationsCount == 0 ? "hide" : "dropbtn"}>{ this.state.notificationsCount }</span>
                    </Link>
                    <div id="notificationsbox" className={!this.state.notificationsMenuVisible ? "hide":"dropbtn"}>
                    <div className="nheader dropbtn">
                    <span className="ntitle dropbtn">Notifications</span>
                    <span className="naction dropbtn">Mark All as Read</span>
                    </div>
                    <Scrollbars style={{ width: "100%" , height: 324}}>
                      <ul ref="notificationslist" className="list cleanlist">
                        <li className="notification" key={1}>
                          <div>
                            <table>
                            <tbody>
                              <tr>
                                <td className="imgbox">
                                  <img className="img" src={ '/public/img/users/' + AccountStore.getId() + '.png' } />
                                </td>
                                <td>
                                  <div className="body"><span className="doer">Hisam</span> commented on your post: 
                                    <div className="msg">"away to track all activities on my posts..."</div>
                                  </div>
                                  <div className="date">2 min</div>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                        </li>
                        <li className="notification" key={2}>
                          <div>
                            <table>
                            <tbody>
                              <tr>
                                <td className="imgbox">
                                  <img className="img" src={ '/public/img/users/16.png' } />
                                </td>
                                <td>
                                  <div className="body"><span className="doer">Hisam</span> commented on your post: 
                                    <div className="msg">"away to track all"</div>
                                  </div>
                                  <div className="date">2 min</div>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                        </li>
                        <li className="notification" key={3}>
                          <div>
                            <table>
                            <tbody>
                              <tr>
                                <td className="imgbox">
                                  <img className="img" src={ '/public/img/users/9.png' } />
                                </td>
                                <td>
                                  <div className="body"><span className="doer">Hisam</span> commented on your post: 
                                    <div className="msg">"away to track all activities on my posts"</div>
                                  </div>
                                  <div className="date">2 min</div>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                        </li>
                        <li className="notification" key={4}>
                          <div>
                            <table>
                            <tbody>
                              <tr>
                                <td className="imgbox">
                                  <img className="img" src={ '/public/img/users/' + AccountStore.getId() + '.png' } />
                                </td>
                                <td>
                                  <div className="body"><span className="doer">Hisam</span> commented on your post: 
                                    <div className="msg">"away to track all activities on my posts..."</div>
                                  </div>
                                  <div className="date">2 min</div>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                        </li>
                        <li className="notification" key={5}>
                          <div>
                            <table>
                            <tbody>
                              <tr>
                                <td className="imgbox">
                                  <img className="img" src={ '/public/img/users/16.png' } />
                                </td>
                                <td>
                                  <div className="body"><span className="doer">Hisam</span> commented on your post: 
                                    <div className="msg">"away to track all"</div>
                                  </div>
                                  <div className="date">2 min</div>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                        </li>
                        <li className="notification" key={6}>
                          <div>
                            <table>
                            <tbody>
                              <tr>
                                <td className="imgbox">
                                  <img className="img" src={ '/public/img/users/9.png' } />
                                </td>
                                <td>
                                  <div className="body"><span className="doer">Hisam</span> commented on your post: 
                                    <div className="msg">"away to track all activities on my posts"</div>
                                  </div>
                                  <div className="date">2 min</div>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                        </li>
                      </ul>
                    </Scrollbars>
                    </div>
                  </li>
                  
                </ul>
                <ul className={ AccountStore.isLoggedIn() ? "navgroup right" : "hide" }>
                  <li>
                    <a href="#" style={ {    paddingTop: 11} }>
                      <img className="profimg" src={ '/public/img/users/' + AccountStore.getId() + '.png' } />
                      <span className="tab">{ AccountStore.getName() }</span>
                    </a>
                  </li>
                  <li>
                    <input id="search" type="text" className="form-control" placeholder="Search" />
                  </li>
                  { /*<li>
                                       <a href="#">
                                         <img src="/public/img/menu.png" />
                                       </a>
                                       <ul>
                                         <li><a style={{borderRadius: "4px 4px 0 0"}} href="#/table/new">Create Table</a></li>
                                         <li><a href="/login">Settings</a></li>
                                         <li><a style={{borderRadius: "0 0 4px 4px"}} onClick={this.logout} href="">Logout</a></li>
                                       </ul>
                                     </li>*/ }
                </ul>
              </div>
            </div>
            );
    }
});

module.exports = Navbar;
