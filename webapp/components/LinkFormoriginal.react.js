var React = require('react');
var http = require('../lib/http');
var url = require('../lib/url');
var AccountStore = require('../stores/AccountStore');
var StreamActions = require('../actions/StreamActions');
var StreamStore = require('../stores/StreamStore');
var assign = require('object-assign');
var conf = require('../lib/conf');

var LinkForm = React.createClass({
	getInitialState: function() {
        return {btnDisabled: false}
    },

  createPost: function(e) {
    e.preventDefault();
    var url = this.refs.url.value;
    var title = this.refs.title.value;
     
    this.setState({btnDisabled: true});

    var postData = {
      tbl: this.props.id,
      url: url,
      title: title
    };

    var that = this;
    http.post("/posts/new", postData, function(data, status) {
      that.refs.url.value = "";
      that.refs.title.value = "";
      that.setState({btnDisabled: false});

      StreamActions.unshift({
        id: data.id,
        tbl: postData.tbl,
        title: postData.title,
        url: postData.url,
        host: postData.host,
        created: data.created,
        comments_count: 0,
        likes_count: 0,
        username: AccountStore.getUsername()
      });
    }, function(data, status) {
    	//TODO
    	that.setState({btnDisabled: false});
    });
  },

  // componentWillReceiveProps: function(nextProps){
  	// x = isSelectRequired(nextProps);
  	// this.setState({selectRequired: x, selectAlert: false});
  // },

  // componentWillUpdate: function(nextProps, nextState){
  	// if(nextState.selectRequired && this.props.id !== nextProps.id){
  	// 	this.selectedTables = "";
  	// }else if (nextProps.type == "t" && nextProps.id != conf.DefaultTable) {
  	// 	this.selectedTables = nextProps.id;
  	// };
  // },

	render: function  () {
		return (
      <div id="lnkfrm">
        <form>
          <input type="text" placeholder="URL" ref="url" /><br/>
          <input type="text" placeholder="Title" ref="title" /><br/>
        </form>

        <h3 id="ctrl">
          <button className="pstbtn btn right" type="button" disabled={this.state.btnDisabled} onClick={this.createPost}>Post</button>
        </h3>
      </div>
		)
	}
});

module.exports = LinkForm;