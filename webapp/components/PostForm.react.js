var React = require('react');
var http = require('../lib/http');
var url = require('../lib/url');
var AccountStore = require('../stores/AccountStore');
var StreamActions = require('../actions/StreamActions');
var StreamStore = require('../stores/StreamStore');
var assign = require('object-assign');
var conf = require('../lib/conf');
var Select = require('react-select');
var Textarea = require('react-textarea-autosize');


var isSelectRequired = function(props){
	return (props.id == conf.DefaultTable);
}

var PostForm = React.createClass({
	getInitialState: function() {
        return {
		    selectRequired: isSelectRequired(this.props),
        counter: 140,
		    btnDisabled: false,
        selectedTable: "",
		    selectAlert: false // used when trying posting before choosing a table
		  }
    },

  postOnChange: function(){
    var body = this.refs.postbody.value;
    this.setState({counter: 140 - body.length});
  },

  createPost: function(e) {
    e.preventDefault();

    // Check if table selected
    if (this.state.selectRequired && this.state.selectedTable == ""){
      this.setState({btnDisabled: false, selectAlert: true});
      return;
    }
    
    // Initialize
    this.setState({btnDisabled: true, selectAlert: false});
    var body = this.refs.postbody.value;

    // Validation
    if(body.length > 140 || body == ""){ // TODO: set the right max body length
      this.setState({btnDisabled: false});
      return;
    }
    

    var postData = {
      table: this.state.selectedTable,
      body: body
    };

    http.post("/posts/new", postData, function(data, status) {
      this.refs.postbody.value = "";
   		if(this.state.selectRequired){
        this.setState({selectedTable: ""});
   		}
      this.setState({btnDisabled: false, counter: 140});
      StreamActions.unshift({
        id: data.id,
        body: data.body,
        table: postData.table,
        created: data.created,
        comments_count: 0,
        likes_count: 0,
        user_name: AccountStore.getName(),
        user_id: AccountStore.getId()
      });
    }.bind(this), function(data, status) {
    	//TODO
    	this.setState({btnDisabled: false});
    }.bind(this));
  },

  onSelectTable: function(value){
    this.setState({selectedTable: value.value, selectAlert: false});
  },

  // getOptions: function(input, callback){
  //   var options;
  //   http.post("/tables/search", {query: input}, function(data, status){
  //     options = data.tables;
  //   } ,function(data, status){})
  //   setTimeout(function() {
  //     callback(null, {
  //           options: options,
  //           complete: true
  //       });
  //   }, 500);
  // },

  componentWillReceiveProps: function(nextProps){
  	var selectRequired = isSelectRequired(nextProps);
    if(selectRequired && this.props.id !== nextProps.id){
      this.setState({selectedTable: "", selectRequired: selectRequired, selectAlert: false});
    }else if (nextProps.id != conf.DefaultTable) {
      this.setState({selectedTable: nextProps.id, selectRequired: selectRequired, selectAlert: false});
    };
  },

	render: function  () {
		var selectOptions = [];
		var usertables = AccountStore.getTables(); 
    for (var i = 1; i < usertables.length; i++) { // ignore the first (default) table
    	 selectOptions.push({value: usertables[i], label: usertables[i]});
    };

    var table;
    var selectClass;
    if (this.state.selectRequired){
    	table = "";
    	selectClass = "";
    }else{
    	table = this.props.id;
    	selectClass = "hide";
    }

    var selectTableLableClass = this.state.selectAlert ? "red bold" : "";
    var counterclass = this.state.counter < 0 ? "red" : "";
    var selectTag;
    if(this.state.selectedTable == ""){
      selectTag = <Select name="tableselector"  className={selectClass} matchProp="value" matchPos="any" options={selectOptions} onChange={this.onSelectTable} />;      
    }else{
      // set empty value to show the placeholder
      selectTag = <Select name="tableselector" value={this.state.selectedTable} className={selectClass} matchProp="value" matchPos="any" options={selectOptions} onChange={this.onSelectTable} />;
    }

		return (
      <div id="pstfrm" className="postform">
        <form>
          <Textarea rows={2} onChange={this.postOnChange} ref="postbody" placeholder="Share something with the table"></Textarea>
        </form>

        <div id="ctrl">
        <div className="selectbox">
          <span className="lbl"><span className={selectTableLableClass}>Table:</span> <span className="bold">{table}</span></span>
          {selectTag}
        </div>
          <span className={counterclass + " counter"}>{this.state.counter.toString()}</span>
          <button className="pstbtn btn right" type="button" disabled={this.state.btnDisabled} onClick={this.createPost}>Post</button>
        </div>
      </div>
		)
	}
});

module.exports = PostForm;
