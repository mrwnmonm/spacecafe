var React = require('react');
var http = require('../lib/http');
var url = require('../lib/url');
var AccountStore = require('../stores/AccountStore');
var StreamActions = require('../actions/StreamActions');
var StreamStore = require('../stores/StreamStore');
var assign = require('object-assign');
var conf = require('../lib/conf');
var Select = require('react-select');
var Textarea = require('react-textarea-autosize');

var getInitPreview = function(){return ({
  url: "",
  title: "",
  host: "",
  desc: "",
  img: "",
  haveimg: false,
  visible: false
  })
};

var isSelectRequired = function(props){
	return (props.type == "s" || (props.type == "t" && props.id == conf.DefaultTable));
}

var PostForm = React.createClass({
	getInitialState: function() {
        return {
		    preview: getInitPreview(),
		    selectRequired: isSelectRequired(this.props),
		    btnDisabled: false,
		    selectAlert: false // used when trying posting before choosing a table
		  }
    },

  selectedTables: "",

  resetPreview: function() {
  	this.setState({preview: getInitPreview()});	
  },

  updatePreview: function(p) {
  	this.setState({preview: assign(this.state.preview, p)});
  },

	postOnChange: function(){
    var body = this.refs.postbody.value;
    this.setState({btnDisabled: false});
    // if(counter < 0){
    //   this.setState({btnDisabled: true});
    // }

    var re = /(^|[\s\n]|<br\/?>)((?:https?|ftp):\/\/[\-A-Z0-9+\u0026\u2019@#\/%?=()~_|!:,.;]*[\-A-Z0-9+\u0026@#\/%=~()_|])/gi;
    var u = new RegExp(re);
    var m = body.match(u);
    var url = "";
    if(m != null){
      url = m[0].trim();
    }
    if(url != "" && url != this.state.preview.url && !this.state.preview.visible){
      this.updatePreview({
        url: url, 
        title: "",
        host: "",
        desc: "",
        img: "",
        haveimg: false,
        visible: true
      });

	  var that = this;
      http.post("/posts/scrape", {url:url},function(data, status){
      // $scope.post.loadingVisible = false;
      // if the user cancel the preview, don't do anything
      if (that.state.preview.visible) {
          // ensure the preview is for the latest inserted url
          if (data.url == that.state.preview.url && data.preview_type != 0) {
              if (data.images.length > 0) {
                that.updatePreview({
                  title: data.title,
                  host: data.host,
                  desc: data.desc,
                  img: data.images[0],
                  haveimg: true,
                  visible: true
                });
              }else{ // no image
                that.updatePreview({
                  title: data.title,
                  host: data.host,
                  desc: data.desc,
                  haveimg: false,
                  visible: true
                });
              };  
          }
          if (data.url == that.state.preview.url && data.preview_type == 0) {
              that.resetPreview();
          }

      }

    } ,that.resetPreview, 20000, that.resetPreview);
	
    }
	},

  createPost: function(e) {
    e.preventDefault();
    var body = this.refs.postbody.value;

    if(body.length > 20000){ // TODO: set the right max body length
      return;
    }
     
    this.setState({btnDisabled: true, selectAlert: false});

    var preview = this.state.preview;
    if (body == "" && !this.state.preview.visible) {
    	  this.setState({btnDisabled: false});
        return;
    }

    if (this.state.selectRequired && this.selectedTables == ""){
    	this.setState({btnDisabled: false, selectAlert: true});
    	return;
    }


    var postData = {
      table: this.selectedTables,
      body: body,
      stage_id: this.props.type == "s" ? this.props.id : 0,
      title: preview.title,
      url: preview.url,
      host: preview.host,
      desc: preview.desc,
      img: preview.img
    };

    var that = this;
    http.post("/posts/new", postData, function(data, status) {
      that.refs.postbody.value = "";
      that.refs.postbody.recalculateSize();
 		if(that.state.selectRequired){
 			that.selectedTables = "";
 		}
 		
      that.setState({btnDisabled: false});
      that.resetPreview();
      StreamActions.unshift({
        id: data.id,
        body: data.body,
        tables: [postData.table],
        preview_type: data.preview_type,
        title: postData.title,
        url: postData.url,
        host: postData.host,
        description: postData.desc,
        img: postData.img,
        created: data.created,
        comments_count: 0,
        author_name: AccountStore.getName(),
        author_id: AccountStore.getId(),
        author_img: "" // TODO
        // author_type: // TODO
      });
    }, function(data, status) {
    	//TODO
    	that.setState({btnDisabled: false});
    });
  },

  onSelectTable: function(value){
  	this.selectedTables = value;
  	this.setState({selectAlert: false});
  },

  getOptions: function(input, callback){
    var options;
    http.post("/tables/search", {query: input}, function(data, status){
      options = data.tables;
    } ,function(data, status){})
    setTimeout(function() {
      callback(null, {
            options: options,
            complete: true
        });
    }, 500);
  },

  componentWillReceiveProps: function(nextProps){
  	x = isSelectRequired(nextProps);
  	this.setState({selectRequired: x, selectAlert: false});
  },

  componentWillUpdate: function(nextProps, nextState){
  	if(nextState.selectRequired && this.props.id !== nextProps.id){
  		this.selectedTables = "";
  	}else if (nextProps.type == "t" && nextProps.id != conf.DefaultTable) {
  		this.selectedTables = nextProps.id;
  	};
  },

	render: function  () {
		var selectOptions = [];
		var usertables = AccountStore.getTables(); 
    for (var i = 1; i < usertables.length; i++) { // ignore the first (default) table
    	 selectOptions.push({value: usertables[i], label: usertables[i]});
    };

    var table;
    var selectClass;
    if (this.state.selectRequired){
    	table = "";
    	selectClass = "";
    }else{
    	table = this.props.id;
    	selectClass = "hide";
    }

    var selectTableLableClass = this.state.selectAlert ? "red bold" : "";
    var pvfclass = this.state.preview.visible ? "pvf" : "hide";
    // var pvfimgclass = this.state.preview.haveimg ? "pvimgbox" : "hide";
    var pvmetaclass = this.state.preview.haveimg ? "pvmeta" : "pvmeta full";

    // var counterclass = this.state.counter < 0 ? "red" : "";
    // just removing the value attribute when there is no value (selectedTables is empty)
    // to show the placeholder when there is no value
    var selectTag;
    switch(this.props.type){
      case "s":
        if(this.selectedTables == ""){
          selectTag = <Select name="tableselector" multi={true} asyncOptions={this.getOptions} autoload={false} className={selectClass} matchProp="value" matchPos="any" options={selectOptions} onChange={this.onSelectTable} />;      
        }else{
          selectTag = <Select name="tableselector" multi={true} asyncOptions={this.getOptions} autoload={false} value={this.selectedTables} className={selectClass} matchProp="value" matchPos="any" options={selectOptions} onChange={this.onSelectTable} />;
        }
      break;
      case "t":
        if(this.selectedTables == ""){
          selectTag = <Select name="tableselector" className={selectClass} matchProp="value" matchPos="any" options={selectOptions} onChange={this.onSelectTable} />;      
        }else{
          selectTag = <Select name="tableselector" value={this.selectedTables} className={selectClass} matchProp="value" matchPos="any" options={selectOptions} onChange={this.onSelectTable} />;
        }
      break;
    }
    

		return (
      <div id="pstfrm">
        <form>
        	<Textarea onChange={this.postOnChange} ref="postbody" placeholder="Share something with the table"></Textarea>
          <div className={pvfclass}>
          	<button type="button" className="pvfclose" onClick={this.resetPreview}>&times;</button>
              <div className={pvmetaclass}>
                <div className="pvtitle">{StreamStore.getNicePreviewTitle(this.state.preview.title, this.state.preview.haveimg)}</div>
                <div className="pvhost">{this.state.preview.host}</div>
                <div className="pvdisc">{StreamStore.getNicePreviewDesc(this.state.preview.desc, this.state.preview.haveimg)}</div>
              </div>
          </div> 
        </form>

        <h3 id="ctrl">
        <div className="selectbox">
          <span className="lbl"><span className={selectTableLableClass}>Table:</span> <span className="bold">{table}</span></span>
          {selectTag}
        </div>
          {/*<span className={counterclass + " counter"}>{this.state.counter.toString()}</span>*/}
          <button className="pstbtn btn right" type="button" disabled={this.state.btnDisabled} onClick={this.createPost}>Post</button>
        </h3>
      </div>
		)
	}
});

module.exports = PostForm;
// <div className={pvfimgclass}>
// 	                  <div className="pvfimgclose">
// 	                  	<a onClick={function(){this.updatePreview({haveimg:false})}}>remove</a>
// 	                  </div>
// 	                  <img src={this.state.preview.img} />
// 	              	</div>
