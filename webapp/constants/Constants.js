var keyMirror = require('keymirror');

module.exports = keyMirror({
  // ACCOUNT_CREATE: null,
  // ACCOUNT_LOGIN: null,
  // ACCOUNT_RESET_PASS: null

  ROUTE_RENDER: null,
  ROUTE_REDIRECT: null,

  STREAM_EXPAND: null,
  STREAM_INCREASE_COMMENTS: null,
  STREAM_COLLAPSE_ALL: null,
  STREAM_PUSH: null,
  STREAM_UNSHIFT: null,
  STREAM_CLEAR: null,
  STREAM_UPDATE: null,


  // STREAM_UPDATE_PREVIEW: null,
  // STREAM_RESET_PREVIEW: null,

  //Home
  HOME_CHANGE_TABLE: null,
  LOGIN: null,
  SET_TABLES: null,
  INCREASE_NOTIFICATIONS: null,
  SET_NOTIFICATIONS: null
});
