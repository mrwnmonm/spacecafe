var React = require('react');
var ReactDOM = require('react-dom');
var AccountStore = require('./stores/AccountStore');
var Router = require('react-router').Router;
var Route = require('react-router').Route;
var IndexRoute = require('react-router').IndexRoute;
const createBrowserHistory = require('history/lib/createBrowserHistory');

// TODO: review imports in all js files

// Page Components
var Welcome = require('./components/pages/Welcome.react');
var Login = require('./components/pages/Login.react');
var Home = require('./components/pages/Home.react');
var Explore = require('./components/pages/Explore.react');
var App = require('./components/pages/App.react');
var NotFound = require('./components/pages/NotFound.react');
var PostShow = require('./components/pages/PostShow.react');
var TableShow = require('./components/pages/TableShow.react');
var TableCreate = require('./components/pages/TableCreate.react');

AccountStore.init();

ReactDOM.render((
    <Router history={ createBrowserHistory() }>
      <Route path="/" component={ App }>
        <IndexRoute component={ Home } />
        <Route path="welcome" component={ Welcome } />
        <Route path="explore" component={ Explore } />
        <Route path="login" component={ Login } />
        <Route path="table/new" component={ TableCreate } />
        <Route path=":table" component={ TableShow } />
        <Route path="post/:id" component={ PostShow } />
        <Route path="*" component={ NotFound } />
      </Route>
    </Router>
    ), document.getElementById('app'))


    // var routes = (
    //     <Route path="login" component={Login} />
    //     <Route path="loginwait" component={LoginWait} />
    //     <Route path="/" component={Home}>
    //       <Route path="post/:id" component={PostShow} />
    //       <Route path="*" component={NotFound}/>
    //     </Route>
    // );


    // Router.run(
    //     routes,
    //     Router.HistoryLocation,
    //     function(Handler) {
    //         React.render(<Handler />,document.getElementById('app'));
    //     }
    // );


    // var routes = {
    //     '/login': function() {
    //         if (AccountStore.isLoggedIn()){
    //           require.ensure([], function() { 
    //             React.render(React.createElement(require('./components/pages/Home.react')), appElement);
    //           });
    //         }else{
    //           require.ensure([], function() { 
    //             React.render(React.createElement(require('./components/pages/Login.react')), appElement);
    //           });
    //         };
    //     },
    //     '/signup': function() {
    //         if (AccountStore.isLoggedIn()){
    //           require.ensure([], function() { 
    //             React.render(React.createElement(require('./components/pages/Home.react')), appElement);
    //           });
    //         }else{
    //           require.ensure([], function() { 
    //             React.render(React.createElement(require('./components/pages/SignUp.react')), appElement);
    //           });
    //         };
    //     },
    //     '/verify_email/:id/:secret': function(id, secret) {
    //         if (AccountStore.isLoggedIn()){
    //           require.ensure([], function() { 
    //             React.render(React.createElement(require('./components/pages/Home.react')), appElement);
    //           });
    //         }else{
    //           require.ensure([], function() { 
    //             React.render(React.createElement(require('./components/pages/VerifyEmail.react') , {params: {id: id, secret: secret}}), appElement);
    //           });
    //         };
    //     },
    //     'post/:id': function(id){
    //       require.ensure([], function() { 
    //         React.render(React.createElement(require('./components/pages/PostShow.react'), {params: {id: id}}), appElement);
    //       });
    //     },
    //     '/table/new':function(table){
    //       require.ensure([], function() { 
    //         React.render(React.createElement(require('./components/pages/TableCreate.react')), appElement);
    //       });
    //     },
    //     '/:table':function(table){
    //       require.ensure([], function() { 
    //         React.render(React.createElement(require('./components/pages/TableShow.react'), {params: {table: table}}), appElement);
    //       });
    //     },
    //     '/': function() {
    //         if (AccountStore.isLoggedIn()){
    //           require.ensure([], function() { 
    //             React.render(React.createElement(require('./components/pages/Home.react')), appElement);
    //           });
    //         }else{
    //           require.ensure([], function() { 
    //             React.render(React.createElement(require('./components/pages/Login.react')), appElement);
    //           });
    //         };
    //     }
    // };

    // var router = Router(routes).configure({notfound: 
    //   require.ensure([], function() { 
    //     React.render(React.createElement(require('./components/pages/NotFound.react')), appElement);
    //   })
    // });
    // router.init('/');
    // RouteStore.setRouter(router);

    // var SpacecafeApp = React.createClass({
    //  getInitialState: function() {
    //         return {
    //          component: RouteStore.getComponent(),
    //          params: RouteStore.getParams()
    //         };
    //     },
    //     componentDidMount: function() {
    //         RouteStore.addChangeListener(this._onChange);
    //     },
    //     componentWillUnmount: function() {
    //         RouteStore.removeChangeListener(this._onChange);
    //     },
    //     render: function() {
    //      switch(this.state.component) {
    //          case "SignUp":
    //            return (<SignUp />);
    //            break;
    //          case "Login":
    //              return (<Login />);
    //              break;
    //          case "VerifyEmail":
    //              return (<VerifyEmail />);
    //              break;
    //          case "Home":
    //              return (<Home />);
    //           break;
    //         case "PostShow":
    //           return (<PostShow params={this.state.params} />);
    //           // var PostShow = require('./components/pages/PostShow.react');
    //           // var PostShow;
    //           // require.ensure([], function() { 
    //           //   PostShow = require('./components/pages/PostShow.react');
    //           // });
    //           // for (var i = 1; i == 0; i++) {
    //           //   if(typeof PostShow == "function"){
    //           //     console.log(PostShow);
    //           //     return React.createElement(PostShow, {params: this.state.params});
    //           //     break;
    //           //   }
    //           // };
    //           // var PostShow2 = require('./components/pages/PostShow.react');
    //           // return React.createElement(PostShow2, {params: this.state.params});
    //           break;
    //         case "TableShow":
    //           return (<TableShow params={this.state.params} />);
    //           break;
    //          default:
    //            return (<NotFound />);
    //        }
    //     },
    //     _onChange: function() {
    //     this.setState({component: RouteStore.getComponent()});
    //   }
    // });

    // React.render(
    //   <SpacecafeApp />,
    //   document.getElementById('app')
    // );
