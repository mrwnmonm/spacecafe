module.exports = {
  devtool: 'source-map',
  entry: './main.js',
  output: {
  	path: '../public/js',
    filename: 'bundle.js',
    publicPath: "../public/js/"      
  },

  module: {
    loaders: [
      { test: /\.js$/, loader: 'jsx-loader' }
    ]
  }
};