package api

import (
	"net/http"

	"github.com/mrwnmonm/spacecafe/api/g"
)

func Vote(w http.ResponseWriter, r *http.Request) *Response {
	user, p, db, er := prepare(r, "type", "id")
	if er != nil {
		return er
	}

	var itemExist bool
	var err error

	redisConn := g.RedisPool.Get()
	defer redisConn.Close()

	switch p["type"] {
	case "0": // like post
		if itemExist = db.IsPostExist(p["id"]); itemExist {
			redisConn.Do("hset", "p"+p["id"], user.IdStr, "")
		}
	case "1": // unlike post
		if itemExist = db.IsPostExist(p["id"]); itemExist {
			redisConn.Do("hdel", "p"+p["id"], user.IdStr)
		}
	case "2":
		itemExist = db.IsCommentExist(p["id"])
	case "3":
		itemExist = db.IsCommentExist(p["id"])
	default:
		return Error(400, nil)
	}

	if !itemExist {
		return Error(400, nil, "Item doesn't exist")
	}

	// _, err = redisConn.Do("hset", initial+p["id"], user.IdStr, vote)
	if err != nil {
		g.ERROR.Println(err)
	}

	// TODO: reflect
	// TODO: expire key

	return Success()
}
