package api

const (
	SaltStrength                     int    = 7
	MaximumNotificationsPerUser      int    = 25
	NotificationsToDelete            int    = 10
	UserRedisHashUnreadNotifications string = "u"

	ConsumerKey       = "5SaCykDcwg7mtAsOkDASAXNYR"
	ConsumerSecret    = "wzgbaQBTAF3uNC04wVjDmkkuMYOuoNlU4caxbJeYCBpcCO5WGV"
	AccessToken       = "520144436-nVvTSeZG3sqsEjYgwbqYYpMmJom4QwBkdQhekKF5"
	AccessTokenSecret = "pqw3fq5lxjvV8J97UiB8PhI3GN6fJxiDoe7ZIVZl3vVPf"
	CallbackUrl       = "http://localhost:8000/login"

	Port string = "5000"

	ResetPassKeyLifeTime float64 = 20.00
	SessionLifeTime      string  = "7200"     // 2 hours in seconds
	RememberUserLifeTime string  = "15552000" // 6 month

	DbDriver string = "postgres"
	// TODO: choose strong password, and check sslmode
	DbSpec string = "user=postgres password='pass.Marwan2' dbname=spacecafe sslmode=disable port=5432"

	HerokuDb string = "user= rrndykzlxvgwrs password='RZPZpOvSEoEWqD4xTEUL79UKqO' dbname=d6m7ns9q7o95vr sslmode=disable port=5432"

	DBMaxIdleConns int = 10
	DBMaxOpenConns int = 10

	// Post Preview configrations
	// this path is used to save the preview images and while posting, then deleting it after posting
	// this is to save some space, to not save images that not choosen to post
	// and to not save images for a post the user decided to not post it
	PostsPreviewImagesPath string = "public/img/posts/preview/"
	PostsImagesPath        string = "public/img/posts/posts"
	MaxImageSize           int64  = 2 * 1024 * 1024 // 2mb
	MinImageSize           int64  = 2 * 1024        // 2kb

	// this is not the max for specific view, but the max that title and desc could be to sent for any view
	MaxTitleLength int = 100
	MaxDescLength  int = 220

	RedisPort      string = "6379"
	RedisMaxIdle   int    = 5
	RedisMaxActive int    = 10
)