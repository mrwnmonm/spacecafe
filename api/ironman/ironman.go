package ironman

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"text/template"

	"github.com/agtorre/gocolorize"
	"github.com/gorilla/securecookie"
	"github.com/mrjones/oauth"
	"github.com/mrwnmonm/spacecafe/api/models"
)

var (
	trace ironLogger
	info  ironLogger
	warn  ironLogger
	eror  ironLogger
	lg    ironLogger

	TRACE *log.Logger
	INFO  *log.Logger
	WARN  *log.Logger
	ERROR *log.Logger
	LOG   *log.Logger

	OAuthConsumer *oauth.Consumer

	URLRE *regexp.Regexp
)

const (
	ConsumerKey       = "5SaCykDcwg7mtAsOkDASAXNYR"
	ConsumerSecret    = "wzgbaQBTAF3uNC04wVjDmkkuMYOuoNlU4caxbJeYCBpcCO5WGV"
	AccessToken       = "520144436-nVvTSeZG3sqsEjYgwbqYYpMmJom4QwBkdQhekKF5"
	AccessTokenSecret = "pqw3fq5lxjvV8J97UiB8PhI3GN6fJxiDoe7ZIVZl3vVPf"
	CallbackUrl       = "http://localhost:8000/login"
)

var hashKey = []byte{0xfc, 0xc2, 0x10, 0x19, 0xb3, 0xc7, 0x81, 0x22, 0x21, 0x4c, 0x47, 0x10, 0x65, 0xb7, 0xb3, 0x86}
var blockKey = []byte{0xa, 0xab, 0x10, 0xeb, 0xd4, 0xfc, 0x28, 0x34, 0x5e, 0x4a, 0xe9, 0x51, 0x7c, 0xa8, 0xf6, 0xd1}
var SecureCookie = securecookie.New(hashKey, blockKey)

////////////////// Iron Loggers (Colorized Loggers)///////////////////////
// TODO: https://pinboard.in/u:mrwnmonm/tabs/113326/

type ironLogger struct {
	c gocolorize.Colorize
	w io.Writer
}

func (r *ironLogger) Write(p []byte) (n int, err error) {
	return r.w.Write([]byte(r.c.Paint(string(p))))
}

func InitOAuth() {
	TwitterService := oauth.ServiceProvider{
		RequestTokenUrl:   "https://api.twitter.com/oauth/request_token",
		AuthorizeTokenUrl: "https://api.twitter.com/oauth/authenticate",
		AccessTokenUrl:    "https://api.twitter.com/oauth/access_token",
		HttpMethod:        "POST",
	}
	OAuthConsumer = oauth.NewConsumer(ConsumerKey, ConsumerSecret, TwitterService)
}

// create and register colored loggers in global variables
func InitLoggers() {
	trace = ironLogger{gocolorize.NewColor("magenta"), os.Stdout}
	info = ironLogger{gocolorize.NewColor("green"), os.Stdout}
	warn = ironLogger{gocolorize.NewColor("yellow"), os.Stderr}
	eror = ironLogger{gocolorize.NewColor("red"), os.Stderr}
	lg = ironLogger{gocolorize.NewColor("blue"), os.Stdout}

	// Loggers
	TRACE = log.New(&trace, "TRACE ", log.Ldate|log.Ltime|log.Lshortfile)
	INFO = log.New(&info, "INFO  ", log.Ldate|log.Ltime|log.Lshortfile)
	WARN = log.New(&warn, "WARN  ", log.Ldate|log.Ltime|log.Lshortfile)
	ERROR = log.New(&eror, "ERROR ", log.Ldate|log.Ltime|log.Lshortfile)
	LOG = log.New(&lg, "", 0)

	// http://www.regexr.com/39psu
	URLRE = regexp.MustCompile(`((https?|ftp)://|www\.)[^\s/$.?#].[^\s]*`)
}

// type JsonResponse struct {
// 	Status int
// 	Writer http.ResponseWriter
// 	Data   interface{}
// }

// RespondWithJson used to make a response with a json
// it takes the response status ,the response writer and the data to marshal to json
// it also set header called status, this is just to be able to get the response status
// from the top handler to log it

// TODO: check gzip
func RespondWithJson(status int, w http.ResponseWriter, data interface{}) {
	jsonData, err := json.Marshal(data)
	// TODO: see if you need to use htmlescape here or not
	// json.HTMLEscape(dst, src)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Status", "400")
		w.WriteHeader(400)
		w.Write([]byte{})
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Status", strconv.Itoa(status))
	w.WriteHeader(status)
	w.Write(jsonData)
	return
}

func Json(status int, data interface{}) *models.Response {
	return &models.Response{Type: "json", Data: &data}
}

func Error(status int, err error, message ...string) *models.Response {
	return &models.Response{status, err, message[0], "error", nil}
}

func Respond(status int, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Status", strconv.Itoa(status))
	w.WriteHeader(status)
	w.Write([]byte{})
	return
}

// get json params
func GetParams(w http.ResponseWriter, r *http.Request) (map[string]interface{}, bool) {
	var params map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&params)

	if err != nil {
		ERROR.Printf("Error decoding Json: %s", err)
		Respond(400, w)
		return nil, false
	}
	return params, true
}

// func ValidateParams(r *http.Response, params []string) bool {
// 	for p := range params {
// 		if r.FormValue(p) == "" {
// 			return false
// 		}
// 	}
// 	return true
// }

// Authenticate reads session id from "X-TOKEN" header, and fetch it from redis
// if not found it returns 401
func Authenticate(w http.ResponseWriter, r *http.Request) (int, int, bool) {
	token := r.Header.Get("X-TOKEN")
	if token == "" {
		return 0, 401, false
	}
	// remove the leading and the trailing double parentheses from the header value
	token = strings.TrimPrefix(token, "\"")
	token = strings.TrimSuffix(token, "\"")

	// the token should be on the form userId:unixtime:code
	s := strings.Split(token, ":")

	if len(s) != 3 {
		return 0, 401, false
	}

	id, err := strconv.Atoi(s[0])

	var salt string
	txn, err := Db.Begin()
	if err != nil {
		ERROR.Println(err)
		return 0, 500, false
	}
	defer txn.Commit()
	err = txn.QueryRow(`SELECT salt FROM users WHERE id = $1`, id).Scan(&salt)
	if err != nil {
		ERROR.Println(err)
		return 0, 401, false
	}

	var code string
	err = SecureCookie.Decode("code", s[2], &code)
	if err != nil {
		ERROR.Println(err)
		return 0, 401, false
	}

	if (s[0] + ":" + s[1] + ":" + salt) != code {
		return 0, 401, false
	}

	if err != nil {
		ERROR.Println(err)
		return 0, 401, false
	}
	return id, 200, true
}

func RandString(n int) string {
	const alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	var bytes = make([]byte, n)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphanum[b%byte(len(alphanum))]
	}
	return string(bytes)
}

func GetShaString(x string) string {
	h := sha256.New()
	h.Write([]byte(x))
	return base64.URLEncoding.EncodeToString(h.Sum(nil))
}

// func RandNum(n int) string {
// 	const alphanum = "0123456789"
// 	var bytes = make([]byte, n)
// 	rand.Read(bytes)
// 	for i, b := range bytes {
// 		bytes[i] = alphanum[b%byte(len(alphanum))]
// 	}
// 	return string(bytes)
// }

func BeautifyBody(body string) string {
	// TODO: see what else has to do to the body string
	body = template.HTMLEscapeString(body)

	// hyperlink web links
	body = URLRE.ReplaceAllString(body, `<a href="$0" target="_blank" rel="nofollow">$0</a>`)

	// reduce newlines to just one, and make right paragraphs and line breaks
	body = "<p>" + body + "</p>"
	x := strings.Split(body, "\n")
	last := 0 // 0 means (<p> or <br>)   1 means </p>
	for i, _ := range x {
		x[i] = strings.Trim(x[i], " ")
		if i == 0 {
			continue
		}
		if x[i] == "" {
			last = 1
		} else {
			switch last {
			case 0:
				x[i] = "<br>" + x[i]
				last = 0
			case 1:
				x[i] = "</p><p>" + x[i]
				last = 0
			}
		}
	}

	final := strings.Join(x, "")

	if final == "<p></p>" {
		final = ""
	}

	return final
}
