package ironman

import (
	"database/sql"
	"flag"
	"strconv"
	"strings"
	"time"

	"github.com/garyburd/redigo/redis"
	_ "github.com/lib/pq"
	"github.com/mrwnmonm/spacecafe/api/conf"
	"github.com/mrwnmonm/spacecafe/api/models"
)

var (
	Db        *sql.DB
	RedisPool *redis.Pool
)

// TODO : @INFO: If you're using Redis on a production environment, you should set vm.overcommit_memory to 1.

func InitDb() {
	// Open a connection.
	var err error
	Db, err = sql.Open(conf.DbDriver, conf.DbSpec)
	if err != nil {
		ERROR.Println("Unable to connect to the Database")
		panic(err)
	}
	err = Db.Ping()
	if err != nil {
		ERROR.Println(err)
		panic(err)
	}
	Db.SetMaxIdleConns(conf.DBMaxIdleConns)
	Db.SetMaxOpenConns(conf.DBMaxOpenConns)

	INFO.Println("PostgreSql connection established")
}

func InitRedis() {
	redisServer := flag.String("rredisServer", ":"+conf.RedisPort, "")
	redisPassword := flag.String("rredisPassword", "", "")
	RedisPool = newRedisPool(*redisServer, *redisPassword)

	redisconn := RedisPool.Get()
	defer redisconn.Close()
	_, err := redisconn.Do("PING")
	if err != nil {
		ERROR.Println(err)
		panic(err)
	}
	INFO.Println("Redis connection established")
	INFO.Printf("RedisPort: %s\n", conf.RedisPort)
	INFO.Printf("RedisMaxIdle: %d\n", conf.RedisMaxIdle)
	INFO.Printf("RedisMaxActive: %d\n", conf.RedisMaxActive)
}

func newRedisPool(server, password string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     conf.RedisMaxIdle,
		MaxActive:   conf.RedisMaxActive,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			// TODO
			// if _, err := c.Do("AUTH", password); err != nil {
			// 	c.Close()
			// 	return nil, err
			// }
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

// TODO http://blog.codinghorror.com/give-me-parameterized-sql-or-give-me-death/
// func GetTables(queryString string) (*[]models.Table, error) {
// 	tables := []models.Table{}
// 	rows, err := Db.Query(`SELECT * FROM tables where private = false ` + queryString)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer rows.Close()
// 	for rows.Next() {
// 		table := models.Table{}
// 		err := rows.Scan(&table.Id, &table.Name, &table.Private, &table.Members, &table.Posts, &table.Likes, &table.Comments, &table.Created, &table.Updated)
// 		if err != nil {
// 			return nil, err
// 		}
// 		tables = append(tables, table)
// 	}
// 	if err := rows.Err(); err != nil {
// 		return nil, err
// 	}
// 	return &tables, err
// }

// // TODO, all db functions should return errors like GetTables
// func GetTableByName(name string) *models.Table {
// 	row := Db.QueryRow(`SELECT *
//   	FROM tables where name = $1`, name)
// 	if row == nil {
// 		return nil
// 	}
// 	table := models.Table{}
// 	err := row.Scan(&table.Id, &table.Name, &table.Private, &table.Members, &table.Posts, &table.Likes, &table.Comments, &table.Created, &table.Updated)
// 	if err != nil {
// 		return nil
// 	}
// 	return &table
// }

// func GetTableById(id int64) *models.Table {
// 	row := Db.QueryRow(`SELECT *
//   	FROM tables where id = $1`, id)
// 	if row == nil {
// 		return nil
// 	}
// 	table := models.Table{}
// 	err := row.Scan(&table.Id, &table.Name, &table.Private, &table.Members, &table.Posts, &table.Likes, &table.Comments, &table.Created, &table.Updated)
// 	if err != nil {
// 		return nil
// 	}
// 	return &table
// }

// func GetUserByUsername(username string) *models.User {
// 	row := Db.QueryRow(`SELECT id, name, email, username, hashedpassword, created, updated,
//     tablesids, tablesnames FROM users where username = $1`, username)
// 	if row == nil {
// 		return nil
// 	}

// 	user := models.User{}
// 	err := row.Scan(&user.Id, &user.Name, &user.Email, &user.Username, &user.HashedPassword, &user.Created, &user.Updated, &user.TablesIds, &user.TablesNames)
// 	if err != nil {
// 		return nil
// 	}
// 	return &user
// }

// func GetUserByEmail(email string) *models.User {
// 	row := Db.QueryRow(`select id, name, email, username, hashedpassword, tablesids, tablesnames, created, updated
//     from users where email = $1`, email)

// 	user := models.User{}
// 	err := row.Scan(&user.Id, &user.Name, &user.Email, &user.Username, &user.HashedPassword, &user.TablesIds, &user.TablesNames, &user.Created, &user.Updated)
// 	if err != nil {
// 		return nil
// 	}
// 	return &user
// }

// if user exists return true, if not return false
// TODO: email lower case search on login and signup
func CheckUserByEmail(txn *sql.Tx, email string) (bool, error) {
	email = strings.ToLower(email)
	exists := false
	err := txn.QueryRow(`select exists(select 1 from users where lower(email)=$1) AS "exists"`, email).Scan(&exists)
	if err != nil || !exists {
		return false, err
	}
	return true, nil
}

// if user exists return true, if not return false
func CheckUserByUsername(txn *sql.Tx, username string) (bool, error) {
	exists := false
	err := txn.QueryRow(`select exists(select 1 from users where username=$1) AS "exists"`, username).Scan(&exists)
	if err != nil || !exists {
		return false, err
	}
	return true, nil
}

// TODO
// check all uses of user email and tables name
// and their indexes
// and see if all is done right

// if table exists return true, if not return false
func CheckTableByName(txn *sql.Tx, name string) bool {
	exists := false
	err := txn.QueryRow(`select exists(select 1 from tables where lower(name)=$1) AS "exists"`, name).Scan(&exists)
	if err != nil || !exists {
		return false
	}
	return true
}

func CheckMembership(txn *sql.Tx, id int, name string) bool { // userId and table name
	name = strings.ToLower(name)
	exists := false
	err := txn.QueryRow(`select exists(select 1 from membership where user_id=$1 and table_name=$2) AS "exists"`, id, name).Scan(&exists)
	TRACE.Println(name)
	if err != nil || !exists {
		return false
	}
	return true
}

func IsMember(id int64, table string) bool {
	idstr := strconv.FormatInt(id, 10)
	redisconn := RedisPool.Get()
	defer redisconn.Close()
	isMember := false
	isMember, err := redis.Bool(redisconn.Do("SISMEMBER", "u:"+idstr+":tables", table))
	if err != nil || !isMember {
		ERROR.Println(err)
		return false
	}
	return true
}

func GetPost(id int64) *models.Post {
	row := Db.QueryRow(`SELECT p.id, p.body, p.user_id, p.user_name, p.table, p.comments_count, p.likes_count, p.created from posts p where p.id = $1`, id)
	if row == nil {
		return nil
	}
	post := models.Post{}
	err := row.Scan(&post.Id, &post.Body, &post.UserId, &post.UserName, &post.Table, &post.CommentsCount, &post.LikesCount, &post.Created)
	if err != nil {
		ERROR.Println(err)
		return nil
	}
	return &post
}

// func GetCommentById(id int64) *models.Comment {
// 	row := Db.QueryRow(`SELECT id, feedid, userid, username, body, created
//   	FROM comments where id = $1`, id)
// 	if row == nil {
// 		return nil
// 	}
// 	comment := models.Comment{}
// 	err := row.Scan(&comment.Id, &comment.FeedId, &comment.UserId, &comment.UserName, &comment.Body, &comment.Created)
// 	if err != nil {
// 		return nil
// 	}
// 	return &comment
// }

// func GetAllComments(postId int64) (*[]models.Comment, error) {
// 	comments := []models.Comment{}
// 	rows, err := Db.Query(`SELECT id, post_id, user_id, body, created FROM comments where post_id = $1 order by created ASC`, postId)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer rows.Close()
// 	for rows.Next() {
// 		comment := models.Comment{}
// 		err := rows.Scan(&comment.Id, &comment.PostId, &comment.UserId, &comment.Body, &comment.Created)
// 		if err != nil {
// 			return nil, err
// 		}
// 		comments = append(comments, comment)
// 	}
// 	if err := rows.Err(); err != nil {
// 		return nil, err
// 	}
// 	return &comments, err
// }

// func GetPageById(id string) *models.Page {
// 	row := Db.QueryRow(`SELECT *
//   	FROM pages where id = $1`, id)
// 	if row == nil {
// 		return nil
// 	}
// 	page := models.Page{}
// 	err := row.Scan(&page.Id, &page.Name, &page.UserId)
// 	if err != nil {
// 		return nil
// 	}
// 	return &page
// }
