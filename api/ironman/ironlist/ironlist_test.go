package ironlist

import (
	"testing"
)

func TestContains(t *testing.T) {
	s := "golang,js,programming"
	if Contains(s, "go") {
		t.Errorf("%s doesn't contain \"go\" but it think it is, and return true")
	}
}

func TestAppendInt(t *testing.T) {
	s := "1,2,3"
	if x := AppendInt(s, 4); x != "1,2,3,4" {
		t.Errorf("appending 4 to \"1,2,3\" should get \"1,2,3,4\" but gets %s", x)
	}
}

func TestAppendString(t *testing.T) {
	s := "golang,js"
	if x := AppendString(s, "html"); x != "golang,js,html" {
		t.Errorf("appending \"html\" to \"golang,js\" should get \"golang,js,html\" but gets %s", x)
	}
}

func TestCut(t *testing.T) {
	s := "golang,js,html"
	if x := Cut(s, "go"); x != "golang,js,html" {
		t.Errorf("cutting \"go\" from \"golang,js,html\" shouldn't do anything, but gets %s", x)
	}

	if x := Cut(s, "js"); x != "golang,html" {
		t.Errorf("cutting \"js\" from \"golang,js,html\" shouldn get \"golang,html\", but gets %s", x)
	}
}
