// ironlist package works on strings in this format "3,4,5" or "golang,js,html", we called it ironlist
package ironlist

import (
	"strconv"
	"strings"
)

// Contains checks if an ironlist contains a certain value or not
// Why not using strings.Contains?
// because we must check the values between the ",",
// but strings.Contains will consider "100,50" contains "10"
// which is wrong in our case
func Contains(s, substr string) bool {
	strslice := strings.Split(s, ",")
	for _, v := range strslice {
		if v == substr {
			return true
		}
	}
	return false
}

func AppendInt(s string, i int) string {
	if s == "" {
		return strconv.Itoa(i)
	}
	return s + "," + strconv.Itoa(i)
}

func AppendString(s, substr string) string {
	if s == "" {
		return substr
	}
	return s + "," + substr
}

func Cut(s, substr string) string {
	slice := strings.Split(s, ",")
	for i, v := range slice {
		if v == substr {
			slice = append(slice[:i], slice[i+1:]...) // delete element at index i
		}
	}
	result := strings.Join(slice, ",")
	return result
}
