package api

import (
	"time"
	"io"
	"github.com/agtorre/gocolorize"
)

type response struct {
	Code    int          `json:"status"`
	Error   error        `json:"-"`
	Message string       `json:"message"`
	Type    string       `json:"-"`
	Data    *interface{} `json:"-"`
}

type logger struct {
	c gocolorize.Colorize
	w io.Writer
}

type user struct {
	Id    int
	IdStr string
	Name  string
}

type post struct {
	Id            int64     `json:"id"`
	Body          string    `json:"body"`
	Table         string    `json:"table"`
	UserId        int       `json:"user_id"`
	UserName      string    `json:"user_name"`
	CommentsCount int       `json:"comments_count"`
	Likes         int       `json:"likes"`
	Liked         bool      `json:"liked"`
	Created       time.Time `json:"created"`
}

type table struct {
	Name         string    `json:"name"`
	Type         int       `json:"type"`
	Private      bool      `json:"-"`
	UserId       int       `json:"-"`
	MembersCount int       `json:"members_count"`
	Desc         string    `json:"desc"`
	About        string    `json:"about"`
	Created      time.Time `json:"-"`
}

type comment struct {
	Id           int64     `json:"id"`
	PostId       int64     `json:"-"`
	Body         string    `json:"body"`
	UserId       int       `json:"user_id"`
	UserName     string    `json:"user_name"`
	ParentId     int64     `json:"parent_id"`
	Depth        int8      `json:"depth"`
	RepliesCount int       `json:"replies_count"`
	Votes        int       `json:"votes"`
	Voted        string    `json:"voted"`
	Visible      bool      `json:"visible"`
	Expanded     bool      `json:"expanded"`
	Created      time.Time `json:"created"`
}

type notification struct {
	Id     int `json:"id"`
	UserId int `json:"-"`
	// c: notification for direct comments to posts
	// r: notification for replyies to comments
	Type    string    `json:"type"`
	Body    string    `json:"body"`
	Img     string    `json:"img"`
	Url     string    `json:"url"`
	Created time.Time `json:"created"`
}


// type User struct {
// 	Id           int       `json:"id"`
// 	Name         string    `json:"name"`
// 	AccessToken  string    `json:"-"`
// 	AccessSecret string    `json:"-"`
// 	Created      time.Time `json:"-"`
// }

// type Table struct {
// 	Name         string    `json:"name"`
// 	Private      bool      `json:"-"`
// 	UserId       string    `json:"-"`
// 	MembersCount int       `json:"members_count"`
// 	Created      time.Time `json:"-"`
// }

// type Response struct {
// 	Code    int
// 	Error   error
// 	Message string
// 	Type    string
// 	Data    *interface{}
// }

// type Comment struct {
// 	Id           int64     `json:"id"`
// 	PostId       int64     `json:"-"`
// 	Body         string    `json:"body"`
// 	UserId       int       `json:"user_id"`
// 	UserName     string    `json:"user_name"`
// 	ParentId     int64     `json:"parent_id"`
// 	Depth        int8      `json:"depth"`
// 	RepliesCount int       `json:"replies_count"`
// 	Visible      bool      `json:"visible"`
// 	Expanded     bool      `json:"expanded"`
// 	Created      time.Time `json:"created"`
// }

// type Post struct {
// 	Id            int64     `json:"id"`
// 	Body          string    `json:"body"`
// 	Table         string    `json:"table"`
// 	UserId        int       `json:"user_id"`
// 	UserName      string    `json:"user_name"`
// 	CommentsCount int       `json:"comments_count"`
// 	LikesCount    int       `json:"likes_count"`
// 	Created       time.Time `json:"created"`
// }

// type Preview struct {
// 	Url         string   `json:"url"`
// 	Host        string   `json:"host"`
// 	Title       string   `json:"title"`
// 	Description string   `json:"description"`
// 	ImagesPaths []string `json:"images_paths"`
// 	PreviewType int8     `json:"preview_type"`
// 	Img         string   `json:"img"`
// }

// type SignupError struct {
// 	Name     string `json:"name"`
// 	Username string `json:"username"`
// 	Email    string `json:"email"`
// 	Password string `json:"password"`
// }
