package api

import (
	"flag"
	"strconv"
	"time"
	"os"
	"github.com/eaigner/jet"
	"github.com/garyburd/redigo/redis"
	_ "github.com/lib/pq"
)

var (
	JetDb     *jet.Db
	RedisPool *redis.Pool
)

type Db struct {
	Tx *jet.Tx
}

func (db *Db) GetUser() {
	print("test")
}

func InitDb() {
	// Open a connection.
	var err error
	if os.Getenv("DATABASE_URL") != ""{
		JetDb, err = jet.Open(DbDriver, HerokuDb)
	}else{
		JetDb, err = jet.Open(DbDriver, DbSpec)
	}

	if err != nil {
		ERROR.Println("Unable to connect to the Database")
		panic(err)
	}
	err = JetDb.Ping()
	if err != nil {
		panic(err)
	}

	JetDb.LogFunc = func(queryId, query string, args ...interface{}) {
		LOG.Printf("%v  %v", query, args)
	}
	JetDb.SetMaxIdleConns(DBMaxIdleConns)
	JetDb.SetMaxOpenConns(DBMaxOpenConns)

	INFO.Println("PostgreSql connection established")
	INFO.Printf("PostgreSql MaxIdleConnections: %d\n", DBMaxIdleConns)
	INFO.Printf("PostgreSql MaxOpenConnecitons: %d\n", DBMaxOpenConns)
}

func newRedisPool(server, password string) *redis.Pool {
	if password == ""{
		return &redis.Pool{
		MaxIdle:     RedisMaxIdle,
		MaxActive:   RedisMaxActive,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			// TODO
			// if _, err := c.Do("AUTH", password); err != nil {
			// 	c.Close()
			// 	return nil, err
			// }
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	}
	return &redis.Pool{
		MaxIdle:     RedisMaxIdle,
		MaxActive:   RedisMaxActive,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			// TODO
			if _, err := c.Do("AUTH", password); err != nil {
				c.Close()
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func InitRedis() {
	redisServer := flag.String("redisServer", ":"+RedisPort, "")
	redisPassword := flag.String("redisPassword", "", "")
	if os.Getenv("DATABASE_URL") != "" {
		RedisPool = newRedisPool(*redisServer, *flag.String("redisPassword", "YPya1iHQl7na1pBZ", ""))
	}else{
		RedisPool = newRedisPool(*redisServer, *redisPassword)
	}

	redisconn := RedisPool.Get()
	defer redisconn.Close()
	_, err := redisconn.Do("PING")
	if err != nil {
		ERROR.Println(err)
		panic(err)
	}
	INFO.Println("Redis connection established")
	INFO.Printf("RedisPort: %s\n", RedisPort)
	INFO.Printf("RedisMaxIdle: %d\n", RedisMaxIdle)
	INFO.Printf("RedisMaxActive: %d\n", RedisMaxActive)
}

func (db *Db) HomePosts(tables string, page int) ([]post, error) {
	posts := []Post{}
	err := db.Tx.Query(`
		SELECT p.id, p.body, p.user_id, p.user_name, p.table, p.comments_count, p.likes, p.created FROM posts p 
		where p.table = ANY (string_to_array($1, ',', '')) 
		ORDER BY id DESC offset $2 limit 5; `, tables, 5*page).Rows(&posts)
	return posts, err
}

func (db *Db) IsTableExist(name string) bool {
	exists := false
	err := db.Tx.Query(`select exists(select 1 from tables where lower(name)=$1) AS "exists"`, name).Rows(&exists)
	if err != nil || !exists {
		return false
	}
	return true
}

func (db *Db) IsPostExist(id string) bool {
	exists := false
	err := db.Tx.Query(`select exists(select 1 from posts where id=$1) AS "exists"`, id).Rows(&exists)
	if err != nil || !exists {
		return false
	}
	return true
}

func (db *Db) IsCommentExist(id string) bool {
	exists := false
	err := db.Tx.Query(`select exists(select 1 from comments where id=$1) AS "exists"`, id).Rows(&exists)
	if err != nil || !exists {
		return false
	}
	return true
}

func (db *Db) CreateTable(table *table) error {
	err := db.Tx.Query(`INSERT INTO tables(name, user_id, type, "desc", about)
	VALUES ($1, $2, $3, $4, $5)`, table.Name, table.UserId, table.Type, table.Desc, table.About).Run()
	if err != nil {
		return err
	}
	return nil
}

func (db *Db) JoinTable(userId int, name string) error {
	err := db.Tx.Query(`INSERT INTO membership(user_id, "table")
	VALUES ($1, $2)`, userId, name).Run()
	if err != nil {
		return err
	}
	redisconn := RedisPool.Get()
	defer redisconn.Close()
	_, err = redisconn.Do("zadd", "t"+strconv.Itoa(userId), 0, name)
	if err != nil {
		return err
	}
	return nil
}

func (db *Db) Posts(table string, page int) ([]post, error) {
	posts := []post{}
	err := db.Tx.Query(`
		SELECT p.id, p.body, p.user_id, p.user_name, p.table, p.comments_count, p.likes, p.created FROM posts p
		WHERE $1 = p.table 
		ORDER BY ((p.likes * 4) + p.comments_count)/extract(epoch from (age(p.created) + '1 day')) desc offset $2 limit 5`, table, 5*page).Rows(&posts)
	// and (date, id) < (prev_date, prev_id)
	return posts, err
}

func (db *Db) GetPost(id int) *post {
	post := post{}
	err := db.Tx.Query(`SELECT p.id, p.body, p.user_id, p.user_name, p.table, p.comments_count, p.likes, p.created from posts p where p.id = $1`, id).Rows(&post)
	if err != nil {
		return nil
	}
	return &post
}

func Notify(tx *jet.Tx, id string, n *notification) {
	var nid int
	err := tx.Query(`INSERT INTO notifications(user_id, type, body, img, url)
	    	VALUES ($1, $2, $3, $4, $5) returning id`, n.UserId, n.Type, n.Body, n.Img, n.Url).Rows(&nid)
	if err != nil {
		ERROR.Println(err)
		return
	}
	redisConn := RedisPool.Get()
	defer redisConn.Close()
	notificationsCount, err := redis.Int(redisConn.Do("hget", "u"+id, "n"))
	if err != nil {
		return
	}

	if notificationsCount >= MaximumNotificationsPerUser {
		// delete lateset 10 notifications http://stackoverflow.com/a/5171473
		tx.Query(`DELETE FROM notifications WHERE ctid IN (
    			SELECT ctid
    			FROM notifications
    			where user_id = $1
    			ORDER BY created desc
    			LIMIT $2
				)`, id, NotificationsToDelete)
	}

	// increase unread notifications count
	redisConn.Do("HINCRBY", "u"+id, "u", 1)

	n.Id = nid
	NotificationsLongPoll.Publish(id, n)
	// TODO
	// clear buffer after every publish
	// clear buffer after sometime
	// TRACE.Printf("%#v", NotificationsLongPoll.SubManager.SubEventBuffer)
}

// func AssignVoting(conn redis.Conn, posts []Post, userId string) []Post{
// 	for i := range posts {
// 		posts[i].Voted = ""
// 		postIdStr := strconv.FormatInt(posts[i].Id, 10)
// 		voted, _ := redis.Int(conn.Do("hget", "p"+postIdStr, userId))
// 		switch voted {
// 		case 1:
// 			posts[i].Voted = "up"
// 		case -1:
// 			posts[i].Voted = "down"
// 		}
// 	}
// 	return posts
// }

// func AssignOneVoting(conn redis.Conn, post *Post, userId string) *Post{
// 	post.Voted = ""
// 	postIdStr := strconv.FormatInt(post.Id, 10)
// 	voted, _ := redis.Int(conn.Do("hget", "p"+postIdStr, userId))
// 	switch voted {
// 	case 1:
// 		post.Voted = "up"
// 	case -1:
// 		post.Voted = "down"
// 	}
// 	return post
// }

func AssignLiking(conn redis.Conn, posts []post, userId string) []post {
	for i := range posts {
		posts[i].Liked = false
		postIdStr := strconv.FormatInt(posts[i].Id, 10)
		exist, _ := redis.Bool(conn.Do("hexists", "p"+postIdStr, userId))
		if exist {
			posts[i].Liked = true
		}
	}
	return posts
}

func AssignOneLiking(conn redis.Conn, post *post, userId string) *post {
	post.Liked = false
	postIdStr := strconv.FormatInt(post.Id, 10)
	exist, _ := redis.Bool(conn.Do("hexists", "p"+postIdStr, userId))
	if exist {
		post.Liked = true
	}
	return post
}
