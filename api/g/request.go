package g

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/eaigner/jet"
	"github.com/garyburd/redigo/redis"
	"github.com/gorilla/context"
	"github.com/gorilla/securecookie"
	"github.com/mrwnmonm/spacecafe/api/g/longpoll"
)

var hashKey = []byte{0xfc, 0xc2, 0x10, 0x19, 0xb3, 0xc7, 0x81, 0x22, 0x21, 0x4c, 0x47, 0x10, 0x65, 0xb7, 0xb3, 0x86}
var blockKey = []byte{0xa, 0xab, 0x10, 0xeb, 0xd4, 0xfc, 0x28, 0x34, 0x5e, 0x4a, 0xe9, 0x51, 0x7c, 0xa8, 0xf6, 0xd1}
var SecureCookie = securecookie.New(hashKey, blockKey)

var NotificationsLongPoll *longpoll.LongpollManager

func InitLongPoll() {
	var err error
	NotificationsLongPoll, err = longpoll.CreateCustomManager(600, 5, true)
	if err != nil {
		panic(err)
	}
}

func SetTx(r *http.Request, tx *jet.Tx) {
	context.Set(r, "tx", tx)
}

func Tx(r *http.Request) *jet.Tx {
	return context.Get(r, "tx").(*jet.Tx)
}

type Response struct {
	Code    int          `json:"status"`
	Error   error        `json:"-"`
	Message string       `json:"message"`
	Type    string       `json:"-"`
	Data    *interface{} `json:"-"`
}

func Json(data interface{}) *Response {
	return &Response{Code: 200, Type: "json", Data: &data}
}

func Error(status int, err error, message ...string) *Response {
	if len(message) == 0 {
		return &Response{status, err, "", "error", nil}
	}
	return &Response{status, err, message[0], "error", nil}
}

func Success() *Response {
	return &Response{Type: "success"}
}

func Respond(w http.ResponseWriter, status int, msg ...string) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Status", strconv.Itoa(status))
	w.WriteHeader(status)
	var b []byte
	if len(msg) > 0 {
		b = []byte(msg[0])
	} else {
		b = []byte{}
	}
	w.Write(b)
	return
}

// RespondWithJson used to make a response with a json
// it takes the response status ,the response writer and the data to marshal to json
// it also set header called status, this is just to be able to get the response status
// from the top handler to log it

// TODO: check gzip
func RespondWithJson(status int, w http.ResponseWriter, data interface{}) {
	jsonData, err := json.Marshal(data)
	// TODO: see if you need to use htmlescape here or not
	// json.HTMLEscape(dst, src)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Status", "400")
		w.WriteHeader(400)
		w.Write([]byte{})
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Status", strconv.Itoa(status))
	w.WriteHeader(status)
	w.Write(jsonData)
	return
}

// Authenticate reads session id from "X-TOKEN" header, and fetch it from redis
// if not found it returns 401
// TODO: server can't expire certain session
// it only can expire all sessions per user
func Auth(r *http.Request) (*User, bool) {
	token := r.Header.Get("X-TOKEN")
	if token == "" {
		return nil, false
	}
	// remove the leading and the trailing double parentheses from the header value
	token = strings.TrimPrefix(token, "\"")
	token = strings.TrimSuffix(token, "\"")

	// the token should be on the form userId:unixtime:code
	s := strings.Split(token, ":")

	if len(s) != 3 {
		return nil, false
	}

	id, err := strconv.Atoi(s[0])

	redisConn := RedisPool.Get()
	defer redisConn.Close()
	salt, err := redis.String(redisConn.Do("hget", "u"+s[0], "s"))
	if err != nil {
		ERROR.Println(err)
		return nil, false
	}

	var code map[string]string
	err = SecureCookie.Decode("code", s[2], &code)
	if err != nil {
		ERROR.Println(err)
		return nil, false
	}

	if (s[0] + ":" + s[1] + ":" + salt) != code["token"] {
		return nil, false
	}

	u := User{id, strconv.Itoa(id), code["name"]}
	return &u, true
}

// Params takes request and it's required parameters as strings,
// parse the form and ensure this params existance (empty values don't cause errors),
// then return params map with strings values, and ok value
func _params(r *http.Request, p []string) (map[string]string, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, err
	}
	params := make(map[string]string)
	for _, v := range p {
		s := strings.Split(v, ":")
		err = validate(r, s)
		if err != nil {
			return nil, err
		}
		params[s[0]] = r.FormValue(s[0])
	}
	return params, nil
}

func Params(r *http.Request, p ...string) (map[string]string, error) {
	return _params(r, p)
}

// blank: can be blank
// numeric: should be numerical value
// max=x: can't be more than x
func validate(r *http.Request, field []string) error { // field ex: 'name:blank,numeric'
	var s []string
	if len(field) < 2 {
		goto CheckEmpty
	}
	s = strings.Split(field[1], ",")
	for _, v := range s {
		if v == "blank" && r.FormValue(field[0]) == "" {
			return nil
		}
		if strings.Contains(v, "=") {
			vs := strings.Split(v, "=")
			switch vs[0] {
			case "max":
				max, _ := strconv.Atoi(vs[1])
				if len(r.FormValue(field[0])) > max {
					return errors.New("Parameter '" + field[0] + "' can't be more than " + vs[1] + " characters")
				}
			}
		} else {
			switch v {
			case "numeric":
				_, err := strconv.Atoi(r.FormValue(field[0]))
				if err != nil {
					return errors.New("Parameter '" + field[0] + "' should be an integer string")
				}
			}
		}
	}
CheckEmpty:
	if r.FormValue(field[0]) == "" {
		return errors.New("Parameter '" + field[0] + "' can't be blank")
	}
	return nil
}

func Prepare(r *http.Request, p ...string) (*User, map[string]string, *Db, *Response) {
	user, ok := Auth(r)
	if !ok {
		e := "Not Authorized"
		return nil, nil, nil, Error(400, errors.New(e), e)
	}

	params, err := _params(r, p)
	if err != nil {
		return nil, nil, nil, Error(400, err, err.Error())
	}

	return user, params, &Db{Tx(r)}, nil
}

func PrepareNoAuth(r *http.Request, p ...string) (map[string]string, *Db, bool) {
	params, err := _params(r, p)
	if err != nil {
		return nil, nil, false
	}
	return params, &Db{Tx(r)}, true
}

// func PrepareNoDb(r *http.Request, p ...string) (*User, map[string]string, bool){
// 	user, ok := Auth(r)
// 	if !ok {
// 		return nil, nil, nil, false
// 	}
// 	params, ok := params(r, p)
// 	if !ok {
// 		return nil, nil, nil, false
// 	}
// 	return user, params, true
// }
