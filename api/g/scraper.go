package g

import (
	"errors"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"math/rand"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"code.google.com/p/go-html-transform/h5"
	"code.google.com/p/go.net/html"
	"github.com/disintegration/imaging"
)

type Preview struct {
	URL         string   `json:"url"`
	Host        string   `json:"host"`
	Title       string   `json:"title"`
	Description string   `json:"desc"`
	Images      []string `json:"images"`
	Img         string   `json:"img"`
	VideoID     string   `json:"video_id"`
	VStart      string   `json:"vstart"`
}

// findAttribute returns the value of the provided key
// within the attributes of the given Node
func findAttribute(n *html.Node, key string) string {
	if a := n.Attr; a != nil {
		for i := range a {
			if a[i].Key == key {
				return a[i].Val
			}
		}
	}
	return ""
}

// addImage takes slice of string (image urls), url (page url), requested image url as string,
// and image type
// it does some operations and checks on the requested image url, fetch it and save it
// if that happens successfully, it will add the saved image url to the images slice
// if not it will return the image slice as received
// last argument typ refer to the type of the image being added, it could be
// 0: normal image
// 1: facebook opengraph image
func addImage(images []string, u *url.URL, val string, typ int) ([]string, error) {
	TRACE.Printf("adding image(%d): %s", typ, val)

	if strings.HasPrefix(val, "//") {
		val = u.Scheme + ":" + val
	} else if strings.HasPrefix(val, "/") || strings.HasPrefix(val, "#") {
		val = u.Scheme + "://" + u.Host + val
	} else if !hasProtocolAsPrefix(val) {
		val = u.Scheme + "://" + u.Host + "/" + val
	}

	imgURL, err := url.Parse(val)
	if err != nil {
		ERROR.Println(err)
		return images, err
	}

	val = imgURL.String()

	if (typ == 0) && !(strings.HasSuffix(imgURL.Path, ".png") || strings.HasSuffix(imgURL.Path, ".jpeg") || strings.HasSuffix(imgURL.Path, ".jpg")) {
		ERROR.Println("format not allowed: " + imgURL.Path)
		return images, errors.New("format not allowed: " + imgURL.Path)
	}

	// avoid getting very small images or too bing
	// Fire Head request to the image, and check ContentLength
	res, err := http.Head(val)
	if err != nil {
		ERROR.Println(err)
		return images, err
	}
	// ensure the size is good
	if ((res.ContentLength < MaxImageSize) && (res.ContentLength > MinImageSize)) || (typ == 1 && res.ContentLength == -1) {
		previewImage, err := savePreviewImage(val, 130, 130)
		if err != nil {
			ERROR.Println(err)
			return images, err
		}
		images = append(images, previewImage)
	} else {
		ERROR.Println("image size not suitable")
		return images, errors.New("image size not suitable")
	}

	return images, nil
}

// timeoutDialer sets a connection with a given timeout
func timeoutDialer(secs int) func(net, addr string) (c net.Conn, err error) {
	return func(netw, addr string) (net.Conn, error) {
		c, err := net.Dial(netw, addr)
		if err != nil {
			return nil, err
		}
		c.SetDeadline(time.Now().Add(time.Duration(secs) * time.Second))
		return c, nil
	}
}

// savePreviewImage takes image url, then download the image, save it in this form (timestamp+image name)
// and return the img relative path ex: (/public/img/posts/preivew/20923798439_img.png)
func savePreviewImage(url string, width int, height int) (string, error) {
	TRACE.Println("fetch and save: " + url)

	response, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	img, _, err := image.Decode(response.Body)
	if err != nil {
		return "", err
	}

	// create uniq name
	time := time.Now().UnixNano()
	rnd := rand.Intn(1000)
	rndStr := strconv.FormatInt(int64(rnd), 10)
	timeStr := strconv.FormatInt(time, 10)
	previewImg := PostsPreviewImagesPath + timeStr + rndStr + ".png"

	dst := imaging.Thumbnail(img, width, height, imaging.CatmullRom)

	// dst = imaging.Blur(dst, 5)

	err = imaging.Save(dst, previewImg)

	// TODO: compress images

	if err != nil {
		return "", err
	}

	TRACE.Println("saved and converted to png successfuly: " + previewImg)
	return "/" + previewImg, nil
}

// fixURL adds the Scheme to the URL and
// and fixes the Host and Path
// in case Scheme is empty
func fixURL(u *url.URL) *url.URL {
	if u.Scheme == "" {
		s := strings.Split(u.String(), "/")
		u.Scheme = "http"
		u.Host = s[0]
		u.Path = u.Path[len(s[0]):]
	}
	return u
}

// newScraper parses the given url and scrapes the site, returning
// a scraper object with all the site info and meta tags
func Scrape(urlStr string, timeout int) (*Preview, error) {

	u, err := url.Parse(urlStr)
	if err != nil {
		return nil, err
	}
	u = fixURL(u)

	var title string
	var description string
	host := u.Host
	var videoID string
	var vStart string // viedo begining time
	images := make([]string, 0)
	fbimage := ""
	imagesTries := 0 // (startpoint) number of tries to get an image
	allResolved := false

	scrpr := func(n *html.Node) {
		// return if all needed info is resolved
		if title != "" && description != "" && fbimage != "" {
			allResolved = true
			return
		}

		if title != "" && description != "" && len(images) == 1 {
			allResolved = true
			return
		}

		switch n.Data {
		case "title":
			title = n.FirstChild.Data
		case "meta":
			name := findAttribute(n, "name")
			switch name {
			case "description":
				description = findAttribute(n, "content")
			}
			// check for facebook open graph image
			// as described here https://developers.facebook.com/docs/opengraph/using-objects#selfhosted
			p := findAttribute(n, "property")
			switch p {
			case "og:image":
				image := findAttribute(n, "content")
				if len(images) < 1 && imagesTries < 5 {
					imagesTries = imagesTries + 1
					images, err = addImage(images, u, image, 1)
					if err == nil {
						fbimage = image
					}
				}
			}
		case "img":
			src := findAttribute(n, "src")
			if len(images) < 1 && imagesTries < 5 && src != "" && fbimage == "" {
				imagesTries = imagesTries + 1
				images, _ = addImage(images, u, src, 0)
			}
		}
	}

	// check if the url is an image url
	if strings.HasSuffix(u.Path, ".png") || strings.HasSuffix(u.Path, ".jpeg") || strings.HasSuffix(u.Path, ".jpg") {
		images, err := addImage(images, u, u.String(), 0)
		if err != nil {
			return &Preview{
				URL:         u.String()}, err
		}
		title = urlStr
		if len(title) > MaxTitleLength {
			title = title[:(MaxTitleLength)]
		}
		return &Preview{
			URL:         u.String(),
			Host:        host,
			Title:       title,
			Images:      images}, nil
	}

	// if the url is not an image url, request the html page and parse it
	cl := http.Client{
		Transport: &http.Transport{
			Dial: timeoutDialer(timeout),
		},
	}

	resp, err := cl.Get(u.String())
	if err != nil {
		return &Preview{
			URL:         u.String()}, err
	}

	defer resp.Body.Close()

	tree, err := h5.New(resp.Body)
	if err != nil {
		return &Preview{
			URL:         u.String()}, err
	}

	var WalkNodes func(n *html.Node, f func(*html.Node))
	// instead of using (Walk) function from the h5 package
	// we implement here what it is doing, Read More: https://code.google.com/p/go-html-transform/source/browse/h5/node.go#66
	// to be able to break when all what we need is resolved, and not wait until parsing the whole page
	WalkNodes = func(n *html.Node, f func(*html.Node)) {
		if allResolved {
			return
		}
		if n != nil {
			f(n)
			for c := n.FirstChild; c != nil; c = c.NextSibling {
				WalkNodes(c, f)
			}
		}
	}
	WalkNodes(tree.Top(), scrpr)

	if len(title) > MaxTitleLength {
		title = title[:(MaxTitleLength)]
	}

	if len(description) > MaxDescLength {
		description = description[:(MaxDescLength)]
	}

	var img string
	if len(images) > 0{
		img = images[0]
	}

	return &Preview{
		URL:         u.String(),
		Host:        host,
		Title:       title,
		Description: description,
		Images:      images,
		Img:         img,
		VideoID:     videoID,
		VStart:      vStart}, nil
}

// hasProtocolAsPrefix returns true if the value belongs
// to some protocol
func hasProtocolAsPrefix(val string) bool {
	return strings.HasPrefix(val, "http://") ||
		strings.HasPrefix(val, "https://") ||
		strings.HasPrefix(val, "ftp://") ||
		strings.HasPrefix(val, "s3://")
}
