package g

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
	"text/template"
	"time"

	"github.com/agtorre/gocolorize"
	"github.com/mrjones/oauth"
)

const (
	SaltStrength                     int    = 7
	MaximumNotificationsPerUser      int    = 25
	NotificationsToDelete            int    = 10
	UserRedisHashUnreadNotifications string = "u"

	ConsumerKey       = "5SaCykDcwg7mtAsOkDASAXNYR"
	ConsumerSecret    = "wzgbaQBTAF3uNC04wVjDmkkuMYOuoNlU4caxbJeYCBpcCO5WGV"
	AccessToken       = "520144436-nVvTSeZG3sqsEjYgwbqYYpMmJom4QwBkdQhekKF5"
	AccessTokenSecret = "pqw3fq5lxjvV8J97UiB8PhI3GN6fJxiDoe7ZIVZl3vVPf"
	CallbackUrl       = "http://localhost:8000/login"
)

var (
	trace logger
	info  logger
	warn  logger
	eror  logger
	lg    logger

	TRACE *log.Logger
	INFO  *log.Logger
	WARN  *log.Logger
	ERROR *log.Logger
	LOG   *log.Logger

	URLRE *regexp.Regexp

	OAuthConsumer *oauth.Consumer

	Port string = "5000"

	ResetPassKeyLifeTime float64 = 20.00
	SessionLifeTime      string  = "7200"     // 2 hours in seconds
	RememberUserLifeTime string  = "15552000" // 6 month

	DbDriver string = "postgres"
	// TODO: choose strong password, and check sslmode
	DbSpec string = "user=postgres password='pass.Marwan2' dbname=spacecafe sslmode=disable port=5432"

	// Host
	// ec2-54-83-52-144.compute-1.amazonaws.com
	// Database
	// d6m7ns9q7o95vr
	// User
	// rrndykzlxvgwrs
	// Port
	// 5432
	// Password
	// Hide RZPZpOvSEoEWqD4xTEUL79UKqO

	HerokuDb string = "user= rrndykzlxvgwrs password='RZPZpOvSEoEWqD4xTEUL79UKqO' dbname=d6m7ns9q7o95vr sslmode=disable port=5432"

	DBMaxIdleConns int = 10
	DBMaxOpenConns int = 10

	// Post Preview configrations
	// this path is used to save the preview images and while posting, then deleting it after posting
	// this is to save some space, to not save images that not choosen to post
	// and to not save images for a post the user decided to not post it
	PostsPreviewImagesPath string = "public/img/posts/preview/"
	PostsImagesPath        string = "public/img/posts/posts"
	MaxImageSize           int64  = 2 * 1024 * 1024 // 2mb
	MinImageSize           int64  = 2 * 1024        // 2kb

	// this is not the max for specific view, but the max that title and desc could be to sent for any view
	MaxTitleLength int = 100
	MaxDescLength  int = 220

	RedisPort      string = "6379"
	RedisMaxIdle   int    = 5
	RedisMaxActive int    = 10
)	

func InitOAuth() {
	TwitterService := oauth.ServiceProvider{
		RequestTokenUrl:   "https://api.twitter.com/oauth/request_token",
		AuthorizeTokenUrl: "https://api.twitter.com/oauth/authenticate",
		AccessTokenUrl:    "https://api.twitter.com/oauth/access_token",
		HttpMethod:        "POST",
	}
	OAuthConsumer = oauth.NewConsumer(ConsumerKey, ConsumerSecret, TwitterService)
}

func Init() {
	InitLoggers()
	InitDb()
	InitRedis()
	InitLongPoll()
	InitOAuth()

	// http://www.regexr.com/39psu
	URLRE = regexp.MustCompile(`((https?|ftp)://|www\.)[^\s/$.?#].[^\s]*`)
}

type logger struct {
	c gocolorize.Colorize
	w io.Writer
}

func (r *logger) Write(p []byte) (n int, err error) {
	return r.w.Write([]byte(r.c.Paint(string(p))))
}

func RandString(n int) string {
	const alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	var bytes = make([]byte, n)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphanum[b%byte(len(alphanum))]
	}
	return string(bytes)
}

func GetShaString(x string) string {
	h := sha256.New()
	h.Write([]byte(x))
	return base64.URLEncoding.EncodeToString(h.Sum(nil))
}

// create and register colored loggers in global variables
func InitLoggers() {
	trace = logger{gocolorize.NewColor("magenta"), os.Stdout}
	info = logger{gocolorize.NewColor("green"), os.Stdout}
	warn = logger{gocolorize.NewColor("yellow"), os.Stderr}
	eror = logger{gocolorize.NewColor("red"), os.Stderr}
	lg = logger{gocolorize.NewColor("blue"), os.Stdout}

	// Loggers
	TRACE = log.New(&trace, "TRACE ", log.Ldate|log.Ltime|log.Lshortfile)
	INFO = log.New(&info, "INFO  ", log.Ldate|log.Ltime|log.Lshortfile)
	WARN = log.New(&warn, "WARN  ", log.Ldate|log.Ltime|log.Lshortfile)
	ERROR = log.New(&eror, "ERROR ", log.Ldate|log.Ltime|log.Lshortfile)
	LOG = log.New(&lg, "", 0)
}

func EliminateNewLines(body string) string {
	body = template.HTMLEscapeString(body)
	// hyperlink web links
	body = URLRE.ReplaceAllString(body, `<a href="$0" target="_blank" rel="nofollow">$0</a>`)

	body = "<p>" + body + "</p>"
	x := strings.Split(body, "\n")
	var s []string
	for i := range x {
		x[i] = strings.Trim(x[i], " ")
		if x[i] != "" {
			s = append(s, x[i])
		}
	}
	return strings.Join(s, " ")
}

func BeautifyBody(body string) string {
	// TODO: see what else has to do to the body string
	body = template.HTMLEscapeString(body)

	// hyperlink web links
	body = URLRE.ReplaceAllString(body, `<a href="$0" target="_blank" rel="nofollow">$0</a>`)

	// reduce multilines to just one, and make right paragraphs and line breaks
	body = "<p>" + body + "</p>"
	x := strings.Split(body, "\n")
	last := 0 // 0 means (<p> or <br>)   1 means </p>
	for i := range x {
		x[i] = strings.Trim(x[i], " ")
		if i == 0 {
			continue
		}
		if x[i] == "" {
			last = 1
		} else {
			switch last {
			case 0:
				x[i] = "<br>" + x[i]
				last = 0
			case 1:
				x[i] = "</p><p>" + x[i]
				last = 0
			}
		}
	}

	final := strings.Join(x, "")

	if final == "<p></p>" {
		final = ""
	}

	return final
}

//////////////// API Types /////////////////

type User struct {
	Id    int
	IdStr string
	Name  string
}

type Post struct {
	Id            int64     `json:"id"`
	Body          string    `json:"body"`
	Table         string    `json:"table"`
	UserId        int       `json:"user_id"`
	UserName      string    `json:"user_name"`
	CommentsCount int       `json:"comments_count"`
	Likes         int       `json:"likes"`
	Liked         bool      `json:"liked"`
	Created       time.Time `json:"created"`
}

type Table struct {
	Name         string    `json:"name"`
	Type         int       `json:"type"`
	Private      bool      `json:"-"`
	UserId       int       `json:"-"`
	MembersCount int       `json:"members_count"`
	Desc         string    `json:"desc"`
	About        string    `json:"about"`
	Created      time.Time `json:"-"`
}

type Comment struct {
	Id           int64     `json:"id"`
	PostId       int64     `json:"-"`
	Body         string    `json:"body"`
	UserId       int       `json:"user_id"`
	UserName     string    `json:"user_name"`
	ParentId     int64     `json:"parent_id"`
	Depth        int8      `json:"depth"`
	RepliesCount int       `json:"replies_count"`
	Votes        int       `json:"votes"`
	Voted        string    `json:"voted"`
	Visible      bool      `json:"visible"`
	Expanded     bool      `json:"expanded"`
	Created      time.Time `json:"created"`
}

type Notification struct {
	Id     int `json:"id"`
	UserId int `json:"-"`
	// c: notification for direct comments to posts
	// r: notification for replyies to comments
	Type    string    `json:"type"`
	Body    string    `json:"body"`
	Img     string    `json:"img"`
	Url     string    `json:"url"`
	Created time.Time `json:"created"`
}

func GetCommentNotification(receiverId int, doer *User, postIdStr string, commentIdStr string, created time.Time) *Notification {
	return &Notification{
		UserId:  receiverId,
		Type:    "c",
		Body:    "<a href='/users/" + doer.IdStr + "'>" + doer.Name + "</a> commented on your post",
		Img:     "/public/img/users/" + doer.IdStr + ".png",
		Url:     "/posts/" + postIdStr + "#" + commentIdStr,
		Created: created}
}

func GetReplyNotification(receiverId int, doer *User, postIdStr string, commentIdStr string, created time.Time) *Notification {
	return &Notification{
		UserId:  receiverId,
		Type:    "r",
		Body:    "<a href='/users/" + doer.IdStr + "'>" + doer.Name + "</a> replied to your comment",
		Img:     "/public/img/users/" + doer.IdStr + ".png",
		Url:     "/posts/" + postIdStr + "#" + commentIdStr,
		Created: created}
}
