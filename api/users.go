package api

import (
	"database/sql"
	"image"
	"image/png"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/ChimeraCoder/anaconda"
	"github.com/garyburd/redigo/redis"
	"github.com/mrjones/oauth"
	"github.com/mrwnmonm/spacecafe/api/g"
	"github.com/mrwnmonm/spacecafe/api/models"
)

func Login(w http.ResponseWriter, r *http.Request) *Response {
	r.ParseForm()
	if r.FormValue("state") == "init" {
		rtoken, loginUrl, err := g.OAuthConsumer.GetRequestTokenAndUrl(g.CallbackUrl)
		if err != nil {
			return Error(400, err)
		}

		jsonData := struct {
			Url     string `json:"url"`
			RToken  string `json:"rtoken"`
			RSecret string `json:"rsecret"`
		}{
			Url:     loginUrl,
			RToken:  rtoken.Token,
			RSecret: rtoken.Secret,
		}
		return Json(&jsonData)
	} else {
		token := r.FormValue("oauth_token")
		verifier := r.FormValue("oauth_verifier")
		rtoken := r.FormValue("rtoken")
		rsecret := r.FormValue("rsecret")
		if token == "" || verifier == "" || rtoken == "" || rsecret == "" {
			return Error(400, nil)
		}

		if token != rtoken {
			return Error(400, nil)
		}

		atoken, err := g.OAuthConsumer.AuthorizeToken(&oauth.RequestToken{rtoken, rsecret}, verifier)
		if err != nil {
			return Error(400, err)
		}
		anaconda.SetConsumerKey(g.ConsumerKey)
		anaconda.SetConsumerSecret(g.ConsumerSecret)
		api := anaconda.NewTwitterApi(atoken.Token, atoken.Secret)

		user, err := api.GetSelf(url.Values{})

		if user.ScreenName != "mrwnmonm" {
			return Error(400, err)
		}

		// TODO: check if user name or image changed and update them

		redisConn := g.RedisPool.Get()
		defer redisConn.Close()

		var id int
		salt := ""
		txn := g.Tx(r)

		err = txn.Query(`select id from users where twitter_id=$1`, user.Id).Rows(&id)
		switch {
		case err == sql.ErrNoRows: // Create new user
			salt = g.RandString(g.SaltStrength)
			twitterToken, err := g.SecureCookie.Encode("token", atoken.Token+":"+atoken.Secret)
			if err != nil {
				return Error(500, err)
			}
			err = txn.Query(`INSERT INTO users(
            id, name, twitter_token, twitter_id, created)
    		VALUES (nextval('users_id_seq'), $1, $2, $3, $4) RETURNING id`,
				user.Name, twitterToken, user.Id, time.Now()).Rows(&id)
			if err != nil {
				return Error(500, err)
			}

			// get user image
			var response *http.Response
			var file *os.File
			response, err = http.Get(user.ProfileImageURL)
			if err != nil {
				txn.Rollback()
				return Error(500, err)
			}
			defer response.Body.Close()

			file, err = os.Create("./public/img/users/" + strconv.Itoa(id) + ".png")
			if err != nil {
				txn.Rollback()
				return Error(500, err)
			}
			defer file.Close()
			img, _, err := image.Decode(response.Body)
			png.Encode(file, img) // TODO: check transparent images

			// create user hash in redis
			// key u+user_id
			// fields  u:unread_notifications_count   n:notifications_count_in_database   s:salt
			_, err = redisConn.Do("hmset", "u"+strconv.Itoa(id), "u", 0, "n", 0, "s", salt)
			if err != nil {
				txn.Rollback()
				return Error(500, err)
			}

		case err != nil:
			return Error(500, err)
		}

		// CreateSession
		t := strconv.FormatInt(time.Now().Unix(), 10)
		var xtoken string

		if salt == "" { // current user
			salt, err = redis.String(redisConn.Do("hget", "u"+strconv.Itoa(id), "s"))
			if err != nil {
				// user already registered but not have a record in redis, weird
				salt = g.RandString(g.SaltStrength)
				redisConn.Do("hmset", "u"+strconv.Itoa(id), "u", 0, "n", 0, "s", salt)
			}
		}

		code := make(map[string]string)
		code["token"] = strconv.Itoa(id) + ":" + t + ":" + salt
		code["name"] = user.Name

		encoded, err := g.SecureCookie.Encode("code", code)
		if err != nil {
			txn.Rollback()
			return Error(500, err)
		}

		rememberMe := false
		remcookie, err := r.Cookie("remember_me")
		if err == http.ErrNoCookie {
			rememberMe = false
		}
		if remcookie.Value == "true" {
			rememberMe = true
		}
		xtoken = strconv.Itoa(id) + ":" + t + ":" + encoded

		jsonData := struct {
			XToken     string      `json:"xtoken"`
			RememberMe bool        `json:"remember_me"`
			User       models.User `json:"user"`
		}{
			XToken:     xtoken,
			RememberMe: rememberMe,
			User: models.User{
				Id:   id,
				Name: user.Name,
			},
		}

		return Json(jsonData)
	}
}


// logout from all devices means changing user salt in redis
// func LogoutU() {
// }

func Connect(w http.ResponseWriter, r *http.Request) *Response {
	user, ok := Auth(r)
	if !ok {
		return Error(401, nil)
	}

	category := r.URL.Query().Get("category")

	if user.IdStr != category {
		return Error(400, nil)
	}

	g.NotificationsLongPoll.SubscriptionHandler(w, r)

	return nil
}