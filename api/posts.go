package api

import (
	"net/http"
	"strconv"
	"strings"
	"time"
	"github.com/garyburd/redigo/redis"
	"github.com/mrwnmonm/spacecafe/api/g"
)

func NewPost(w http.ResponseWriter, r *http.Request) *response {
	user, p, db, gr := prepare(r, "table", "body:max=140")
	if gr != nil {
		return gr
	}

	body := BeautifyBody(p["body"])

	// check if the user is member of the table
	// isMember := ironman.IsMember(id, table)
	// if !isMember {
	// 	ironman.Respond(401, w)
	// 	return
	// }

	// TODO: check if table exist

	created := time.Now()

	// TODO: escape data before insert into the database
	var postId int64
	err := db.Tx.Query(`INSERT INTO posts(
		id, body, "table", user_id, user_name, created)
		VALUES (nextval('posts_id_seq'), $1, $2, $3, $4, $5) RETURNING id`,
		body, p["table"], user.Id, user.Name, created).Rows(&postId)
	if err != nil {
		return Error(500, err)
	}
	// defer AfterCreate(table)
	jsonData := struct {
		PostId  int64     `json:"id"`
		Body    string    `json:"body"`
		Created time.Time `json:"created"`
	}{
		PostId:  postId,
		Body:    body,
		Created: created,
	}
	return Json(jsonData)
}


func GetPosts(w http.ResponseWriter, r *http.Request) *response {
	user, params, db, er := prepare(r, "page:numeric", "id", "request_id")
	if er != nil {
		return er
	}

	page, _ := strconv.Atoi(params["page"])

	redisconn := RedisPool.Get()
	defer redisconn.Close()

	// TODO: consider ranking (top posts)
	// TODO: implement the way in (Pagination Done the PostgreSQL Way) book
	var posts []Post
	var err error
	if strings.ToLower(params["id"]) == "all" {
		tables, err := redis.Strings(redisconn.Do("zrevrange", "t"+user.IdStr, 0, 9))
		if err != nil {
			return Error(500, err)
		}
		posts, err = db.HomePosts(strings.Join(tables, ","), page)
	} else {
		posts, err = db.Posts(params["id"], page)

		// TODO: do this concurrently

		// run zscore to check if table exist in user tables
		res, _ := redisconn.Do("zscore", "t"+user.IdStr, params["id"])
		if res != nil {
			redisconn.Do("zincrby", "t"+user.IdStr, "1", params["id"])
		}
	}

	if err != nil {
		return Error(500, err)
	}

	AssignLiking(redisconn, posts, user.IdStr)

	response := struct {
		RequestId string   `json:"request_id"`
		Posts     []Post `json:"posts"`
	}{
		RequestId: params["request_id"],
		Posts:     posts,
	}
	return Json(response)
}

func GetPost(w http.ResponseWriter, r *http.Request) *response {
	user, params, db, er := prepare(r, "post_id")
	if er != nil {
		return er
	}

	postId, err := strconv.Atoi(params["post_id"])
	if err != nil {
		return Error(400, nil)
	}

	post := db.GetPost(postId)
	if post == nil {
		return Error(404, nil)
	}

	redisconn := RedisPool.Get()
	defer redisconn.Close()
	AssignOneLiking(redisconn, post, user.IdStr)

	return Json(post)
}


// takes html page url, and parse to find the title, description, and images
// it get 3 images maxmium, and don't get images other than (png, jpeg, jpg)
// and it has a timeout 12 seconds
// i saves the preview images under "public/img/posts/preview"
// which should be deleted after created the posts images under "public/img/posts/posts"
func ScrapeURL(w http.ResponseWriter, r *http.Request) *response {
	_, p, _, er := prepare(r, "url")
	if er != nil {
		return er
	}

	preview, err := g.Scrape(p["url"], 15)
	if err != nil {
		return Error(420, err)
	}

	if len(preview.Images) > 1 {
		preview.Img = preview.Images[0]
	}

	return Json(preview)
}
