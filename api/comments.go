package api

import (
	"net/http"
	"strconv"
	"time"

	"github.com/mrwnmonm/spacecafe/api/g"
)

func GetComments(w http.ResponseWriter, r *http.Request) *response {
	params, err := params(r, "post_id:numeric")
	if err != nil {
		return err(400, err, err.Error())
	}

	postId, err := strconv.ParseInt(params["post_id"], 10, 64)
	if err != nil {
		return err(400, err)
	}

	comments := []comment{}
	err = g.Tx(r).Query(`
		WITH RECURSIVE cte (id, body, user_id, user_name, parent_id, replies_count, depth, created, path)  AS (
		    SELECT  id,
		        body,
		        user_id,
		        user_name, 
		        parent_id,
		        replies_count,
		        1 AS depth,
		        created,
		        array[id] AS path
		    FROM    comments
		    WHERE   parent_id = 0 AND post_id = $1

		    UNION ALL

		    SELECT  comments.id,
		        comments.body,
		        comments.user_id,
		        comments.user_name, 
		        comments.parent_id,
		        comments.replies_count,
		        cte.depth + 1 AS depth,
		        comments.created,
		        cte.path || comments.id
		    FROM    comments
		    JOIN cte ON comments.parent_id = cte.id AND post_id = $2
		    )
		    SELECT id, body, user_id, user_name, parent_id, replies_count, depth, created FROM cte
		ORDER BY path;`, postId, postId).Rows(&comments)
	if err != nil {
		return err(500, err)
	}

	for i := range comments {
		comments[i].Depth = comments[i].Depth - 1
		comments[i].Visible = true
		comments[i].Expanded = false
	}

	return Json(comments)
}


func GetCommentsFlat(w http.ResponseWriter, r *http.Request) *response {

	params, err := Params(r, "post_id:numeric")
	if err != nil {
		return err(400, err, err.Error())
	}

	postId, err := strconv.ParseInt(params["post_id"], 10, 64)
	if err != nil {
		return err(400, err)
	}

	comments := []Comment{}

	// TODO: order with rate
	err = g.Tx(r).Query(`
		    SELECT id, body, user_id, user_name, parent_id, replies_count, created
		    FROM comments
		    WHERE post_id = $1 and parent_id = 0 limit 5`, postId).Rows(&comments)
	if err != nil {
		return err(500, err)
	}

	for i := range comments {
		comments[i].Depth = 0
		comments[i].Visible = true
		comments[i].Expanded = false
	}

	return Json(comments)
}


func NewComment(w http.ResponseWriter, r *http.Request) *response {
	user, params, db, er := prepare(r, "post_id", "parent_id", "body")
	if er != nil {
		return er
	}

	// TODO: do this with reference keys
	var postCreatorId int
	err := db.Tx.Query(`select user_id from posts WHERE id = $1`, params["post_id"]).Rows(&postCreatorId)
	if err != nil {
		return err(420, err, "post doesn't exist")
	}

	body := BeautifyBody(params["body"])
	created := time.Now()

	// TODO: escape data before insert into the database
	// check if parent_id and post_id exist

	var parentCommentCreatorId int
	if params["parent_id"] != "0" {
		err := db.Tx.Query(`select user_id from comments WHERE id = $1`, params["parent_id"]).Rows(&parentCommentCreatorId)
		if err != nil {
			return err(420, err, "comment doesn't exist")
		}
	}

	var commentId int64
	err = db.Tx.Query(`
		INSERT INTO comments( body, post_id, parent_id, user_id, user_name, created)
		VALUES ($1, $2, $3, $4, $5, $6) returning id`,
		body, params["post_id"], params["parent_id"], user.Id, user.Name, created).Rows(&commentId)
	if err != nil {
		return err(500, err)
	}

	jsonData := struct {
		Id      int64     `json:"id"`
		Created time.Time `json:"created"`
		Body    string    `json:"body"`
	}{
		Id:      commentId,
		Created: created,
		Body:    body,
	}

	go afterCreateComment(user, commentId, params["post_id"], postCreatorId, params["parent_id"], parentCommentCreatorId, created)
	return Json(jsonData)
}

func afterCreateComment(user *user, commentId int64, postIdStr string, postCreatorId int, parentId string, parentCommentCreatorId int, created time.Time) {
	tx, err := JetDb.Begin()
	if err != nil {
		return
	}
	defer tx.Commit()

	// increase comments count for the post
	tx.Query(`UPDATE posts SET comments_count= (comments_count + 1) WHERE id = $1`, postIdStr).Run()

	commentIdStr := strconv.FormatInt(commentId, 10)

	if parentId != "0" {
		// increase replies count for the comment
		tx.Query(`UPDATE comments SET replies_count= (replies_count + 1) WHERE id = $1`, parentId).Run()
	}

	// Notify Users
	// TODO: if creator is owner don't notify

	if parentId == "0" { // notify post creator
		// if user.Id == postCreatorId {
		// 	return
		// }
		n := GetCommentNotification(postCreatorId, user, postIdStr, commentIdStr, created)
		Notify(tx, strconv.Itoa(postCreatorId), n)
	} else { // notify parent comment creator
		// if user.Id == parentCommentCreatorId {
		// 	return
		// }
		n := GetReplyNotification(postCreatorId, user, postIdStr, commentIdStr, created)
		Notify(tx, strconv.Itoa(parentCommentCreatorId), n)
	}
}

// /*
// post /comments/delete
// params comment_id
// 420, if the comment is not exist
// 401, if the requester is not the author
// */
// func DeleteC(w http.ResponseWriter, r *http.Request) {
// 	id, s, ok := ironman.Authenticate(w, r)
// 	if !ok {
// 		ironman.Respond(s, w)
// 		return
// 	}

// 	params, ok := ironman.GetParams(w, r)
// 	if !ok {
// 		return
// 	}

// 	if params["comment_id"] == nil || params["comment_id"] == "" {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	commentIdStr, ok := params["comment_id"].(string)
// 	if !ok {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	commentId, err := strconv.ParseInt(commentIdStr, 10, 64)
// 	if err != nil {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	txn, err := ironman.Db.Begin()
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	defer txn.Commit()

// 	var authorId int
// 	var postId int64
// 	err = txn.QueryRow(`select author_id, post_id from comments where id = $1`, commentId).Scan(&authorId, &postId)
// 	if err == sql.ErrNoRows { // comment doesn't exist
// 		ironman.Respond(420, w)
// 		return
// 	}

// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	if id != authorId {
// 		ironman.Respond(401, w)
// 		return
// 	}

// 	_, err = txn.Exec("DELETE FROM comments WHERE id = $1", commentId)
// 	if err != nil {
// 		ironman.ERROR.Println(err)
// 		ironman.Respond(500, w)
// 		return
// 	}

// 	ironman.Respond(200, w)

// 	afterDelete(txn, postId)

// 	return
// }

// func afterDeleteC(txn *sql.Tx, postId int64) {
// 	// decrese comments for the feed
// 	_, _ = txn.Exec(`UPDATE posts SET comments_count= (comments_count - 1) WHERE id = $1`, postId)
// }
