package conf

import (
	"log"
	// "os"
)

var (
	MachineKey string

	Host string
	Port string

	ResetPassKeyLifeTime float64
	SessionLifeTime      string
	RememberUserLifeTime string

	DbDriver       string
	DbSpec         string
	DBMaxIdleConns int
	DBMaxOpenConns int

	PostsPreviewImagesPath string
	PostsImagesPath        string
	MaxImageSize           int64
	MinImageSize           int64
	MaxTitleLength         int
	MaxDescLength          int

	RedisPort      string
	RedisMaxIdle   int
	RedisMaxActive int
)

func InitConf() {
	MachineKey = "1000000"

	Port = "5000"

	ResetPassKeyLifeTime = 20.00
	SessionLifeTime = "7200"          // 2 hours in seconds
	RememberUserLifeTime = "15552000" // 6 month

	// Postgresql Conf
	DbDriver = "postgres"
	// TODO: choose strong password, and check sslmode
	DbSpec = "user=postgres password='pass.Marwan2' dbname=spacecafe sslmode=disable port=5432"
	DBMaxIdleConns = 10
	DBMaxOpenConns = 10

	// Redis Conf
	RedisPort = "6379"
	RedisMaxIdle = 5
	RedisMaxActive = 10

	// Post Preview configrations
	// this path is used to save the preview images and while posting, then deleting it after posting
	// this is to save some space, to not save images that not choosen to post
	// and to not save images for a post the user decided to not post it
	PostsPreviewImagesPath = "public/img/posts/preview/"
	PostsImagesPath = "public/img/posts/posts"
	MaxImageSize = 2 * 1024 * 1024 // 2mb
	MinImageSize = 2 * 1024        // 2kb

	// this is not the max for specific view, but the max that title and desc could be to sent for any view
	MaxTitleLength = 100
	MaxDescLength = 220

}

func LogConf(t *log.Logger) {
	// Log the configrations
	t.Printf("Machine Key: %s\n", MachineKey)
	t.Printf("Host: %s:%s\n", Host, Port)
	t.Printf("DataBase MaxIdleConnections: %d\n", DBMaxIdleConns)
	t.Printf("DataBase MaxOpenConnecitons: %d\n", DBMaxOpenConns)
}
