package api

import (
	j "encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"log"
	"os"
	"regexp"
	"text/template"
	"time"
	"github.com/agtorre/gocolorize"
	"github.com/mrjones/oauth"
	"github.com/eaigner/jet"
	"github.com/garyburd/redigo/redis"
	"github.com/gorilla/context"
	"github.com/gorilla/securecookie"
	"github.com/mrwnmonm/spacecafe/api/g/longpoll"
)

var (
	trace logger
	info  logger
	warn  logger
	eror  logger
	lg    logger

	TRACE *log.Logger
	INFO  *log.Logger
	WARN  *log.Logger
	ERROR *log.Logger
	LOG   *log.Logger

	URLRE *regexp.Regexp

	OAuthConsumer *oauth.Consumer
	SecureCookie = securecookie.New(hashKey, blockKey)
	NotificationsLongPoll *longpoll.LongpollManager

	hashKey = []byte{0xfc, 0xc2, 0x10, 0x19, 0xb3, 0xc7, 0x81, 0x22, 0x21, 0x4c, 0x47, 0x10, 0x65, 0xb7, 0xb3, 0x86}
	blockKey = []byte{0xa, 0xab, 0x10, 0xeb, 0xd4, 0xfc, 0x28, 0x34, 0x5e, 0x4a, 0xe9, 0x51, 0x7c, 0xa8, 0xf6, 0xd1}
)	

func initLongPoll() {
	var err error
	NotificationsLongPoll, err = longpoll.CreateCustomManager(600, 5, true)
	if err != nil {
		panic(err)
	}
}

func setTx(r *http.Request, tx *jet.Tx) {
	context.Set(r, "tx", tx)
}

func tx(r *http.Request) *jet.Tx {
	return context.Get(r, "tx").(*jet.Tx)
}

func json(data interface{}) *response {
	return &response{Code: 200, Type: "json", Data: &data}
}

func err(status int, err error, message ...string) *response {
	if len(message) == 0 {
		return &response{status, err, "", "error", nil}
	}
	return &response{status, err, message[0], "error", nil}
}

func success() *response {
	return &response{Type: "success"}
}

func respond(w http.ResponseWriter, status int, msg ...string) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Status", strconv.Itoa(status))
	w.WriteHeader(status)
	var b []byte
	if len(msg) > 0 {
		b = []byte(msg[0])
	} else {
		b = []byte{}
	}
	w.Write(b)
	return
}

// RespondWithJson used to make a response with a json
// it takes the response status ,the response writer and the data to marshal to json
// it also set header called status, this is just to be able to get the response status
// from the top handler to log it

// TODO: check gzip
func respondWithJson(status int, w http.ResponseWriter, data interface{}) {
	jsonData, err := j.Marshal(data)
	// TODO: see if you need to use htmlescape here or not
	// json.HTMLEscape(dst, src)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Status", "400")
		w.WriteHeader(400)
		w.Write([]byte{})
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Status", strconv.Itoa(status))
	w.WriteHeader(status)
	w.Write(jsonData)
	return
}

// Authenticate reads session id from "X-TOKEN" header, and fetch it from redis
// if not found it returns 401
// TODO: server can't expire certain session
// it only can expire all sessions per user
func auth(r *http.Request) (*user, bool) {
	token := r.Header.Get("X-TOKEN")
	if token == "" {
		return nil, false
	}
	// remove the leading and the trailing double parentheses from the header value
	token = strings.TrimPrefix(token, "\"")
	token = strings.TrimSuffix(token, "\"")

	// the token should be on the form userId:unixtime:code
	s := strings.Split(token, ":")

	if len(s) != 3 {
		return nil, false
	}

	id, err := strconv.Atoi(s[0])

	redisConn := RedisPool.Get()
	defer redisConn.Close()
	salt, err := redis.String(redisConn.Do("hget", "u"+s[0], "s"))
	if err != nil {
		ERROR.Println(err)
		return nil, false
	}

	var code map[string]string
	err = SecureCookie.Decode("code", s[2], &code)
	if err != nil {
		ERROR.Println(err)
		return nil, false
	}

	if (s[0] + ":" + s[1] + ":" + salt) != code["token"] {
		return nil, false
	}

	u := User{id, strconv.Itoa(id), code["name"]}
	return &u, true
}

// Params takes request and it's required parameters as strings,
// parse the form and ensure this params existance (empty values don't cause errors),
// then return params map with strings values, and ok value
func _params(r *http.Request, p []string) (map[string]string, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, err
	}
	params := make(map[string]string)
	for _, v := range p {
		s := strings.Split(v, ":")
		err = validate(r, s)
		if err != nil {
			return nil, err
		}
		params[s[0]] = r.FormValue(s[0])
	}
	return params, nil
}

func params(r *http.Request, p ...string) (map[string]string, error) {
	return _params(r, p)
}

// blank: can be blank
// numeric: should be numerical value
// max=x: can't be more than x
func validate(r *http.Request, field []string) error { // field ex: 'name:blank,numeric'
	var s []string
	if len(field) < 2 {
		goto CheckEmpty
	}
	s = strings.Split(field[1], ",")
	for _, v := range s {
		if v == "blank" && r.FormValue(field[0]) == "" {
			return nil
		}
		if strings.Contains(v, "=") {
			vs := strings.Split(v, "=")
			switch vs[0] {
			case "max":
				max, _ := strconv.Atoi(vs[1])
				if len(r.FormValue(field[0])) > max {
					return errors.New("Parameter '" + field[0] + "' can't be more than " + vs[1] + " characters")
				}
			}
		} else {
			switch v {
			case "numeric":
				_, err := strconv.Atoi(r.FormValue(field[0]))
				if err != nil {
					return errors.New("Parameter '" + field[0] + "' should be an integer string")
				}
			}
		}
	}
CheckEmpty:
	if r.FormValue(field[0]) == "" {
		return errors.New("Parameter '" + field[0] + "' can't be blank")
	}
	return nil
}

func prepare(r *http.Request, p ...string) (*user, map[string]string, *Db, *response) {
	user, ok := Auth(r)
	if !ok {
		e := "Not Authorized"
		return nil, nil, nil, Error(400, errors.New(e), e)
	}

	params, err := _params(r, p)
	if err != nil {
		return nil, nil, nil, Error(400, err, err.Error())
	}

	return user, params, &Db{Tx(r)}, nil
}

func prepareNoAuth(r *http.Request, p ...string) (map[string]string, *Db, bool) {
	params, err := _params(r, p)
	if err != nil {
		return nil, nil, false
	}
	return params, &Db{Tx(r)}, true
}

// func PrepareNoDb(r *http.Request, p ...string) (*User, map[string]string, bool){
// 	user, ok := Auth(r)
// 	if !ok {
// 		return nil, nil, nil, false
// 	}
// 	params, ok := params(r, p)
// 	if !ok {
// 		return nil, nil, nil, false
// 	}
// 	return user, params, true
// }

func initOAuth() {
	TwitterService := oauth.ServiceProvider{
		RequestTokenUrl:   "https://api.twitter.com/oauth/request_token",
		AuthorizeTokenUrl: "https://api.twitter.com/oauth/authenticate",
		AccessTokenUrl:    "https://api.twitter.com/oauth/access_token",
		HttpMethod:        "POST",
	}
	OAuthConsumer = oauth.NewConsumer(ConsumerKey, ConsumerSecret, TwitterService)
}

func Init() {
	InitLoggers()
	InitDb()
	InitRedis()
	InitLongPoll()
	initOAuth()

	// http://www.regexr.com/39psu
	URLRE = regexp.MustCompile(`((https?|ftp)://|www\.)[^\s/$.?#].[^\s]*`)
}

func (r *logger) Write(p []byte) (n int, err error) {
	return r.w.Write([]byte(r.c.Paint(string(p))))
}

func randString(n int) string {
	const alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	var bytes = make([]byte, n)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphanum[b%byte(len(alphanum))]
	}
	return string(bytes)
}

func getShaString(x string) string {
	h := sha256.New()
	h.Write([]byte(x))
	return base64.URLEncoding.EncodeToString(h.Sum(nil))
}

// create and register colored loggers in global variables
func initLoggers() {
	trace = logger{gocolorize.NewColor("magenta"), os.Stdout}
	info = logger{gocolorize.NewColor("green"), os.Stdout}
	warn = logger{gocolorize.NewColor("yellow"), os.Stderr}
	eror = logger{gocolorize.NewColor("red"), os.Stderr}
	lg = logger{gocolorize.NewColor("blue"), os.Stdout}

	// Loggers
	TRACE = log.New(&trace, "TRACE ", log.Ldate|log.Ltime|log.Lshortfile)
	INFO = log.New(&info, "INFO  ", log.Ldate|log.Ltime|log.Lshortfile)
	WARN = log.New(&warn, "WARN  ", log.Ldate|log.Ltime|log.Lshortfile)
	ERROR = log.New(&eror, "ERROR ", log.Ldate|log.Ltime|log.Lshortfile)
	LOG = log.New(&lg, "", 0)
}

func eliminateNewLines(body string) string {
	body = template.HTMLEscapeString(body)
	// hyperlink web links
	body = URLRE.ReplaceAllString(body, `<a href="$0" target="_blank" rel="nofollow">$0</a>`)

	body = "<p>" + body + "</p>"
	x := strings.Split(body, "\n")
	var s []string
	for i := range x {
		x[i] = strings.Trim(x[i], " ")
		if x[i] != "" {
			s = append(s, x[i])
		}
	}
	return strings.Join(s, " ")
}

func beautifyBody(body string) string {
	// TODO: see what else has to do to the body string
	body = template.HTMLEscapeString(body)

	// hyperlink web links
	body = URLRE.ReplaceAllString(body, `<a href="$0" target="_blank" rel="nofollow">$0</a>`)

	// reduce multilines to just one, and make right paragraphs and line breaks
	body = "<p>" + body + "</p>"
	x := strings.Split(body, "\n")
	last := 0 // 0 means (<p> or <br>)   1 means </p>
	for i := range x {
		x[i] = strings.Trim(x[i], " ")
		if i == 0 {
			continue
		}
		if x[i] == "" {
			last = 1
		} else {
			switch last {
			case 0:
				x[i] = "<br>" + x[i]
				last = 0
			case 1:
				x[i] = "</p><p>" + x[i]
				last = 0
			}
		}
	}

	final := strings.Join(x, "")

	if final == "<p></p>" {
		final = ""
	}

	return final
}

func getCommentNotification(receiverId int, doer *user, postIdStr string, commentIdStr string, created time.Time) *notification {
	return &notification{
		UserId:  receiverId,
		Type:    "c",
		Body:    "<a href='/users/" + doer.IdStr + "'>" + doer.Name + "</a> commented on your post",
		Img:     "/public/img/users/" + doer.IdStr + ".png",
		Url:     "/posts/" + postIdStr + "#" + commentIdStr,
		Created: created}
}

func getReplyNotification(receiverId int, doer *user, postIdStr string, commentIdStr string, created time.Time) *notification {
	return &notification{
		UserId:  receiverId,
		Type:    "r",
		Body:    "<a href='/users/" + doer.IdStr + "'>" + doer.Name + "</a> replied to your comment",
		Img:     "/public/img/users/" + doer.IdStr + ".png",
		Url:     "/posts/" + postIdStr + "#" + commentIdStr,
		Created: created}
}
