package api

import (
	"net/http"

	"github.com/garyburd/redigo/redis"
	"github.com/mrwnmonm/spacecafe/api/g"
)

func GetUnreadNotificationCount(w http.ResponseWriter, r *http.Request) *response {
	user, ok := auth(r)
	if !ok {
		return err(401, nil)
	}

	redisConn := RedisPool.Get()
	defer redisConn.Close()
	unreadNotifications, err := redis.Int(redisConn.Do("hget", "u"+user.IdStr, "u"))
	if err != nil {
		return err(500, nil)
	}

	jsonData := struct {
		UnreadNotifications int `json:"unread_notifications"`
	}{
		UnreadNotifications: unreadNotifications,
	}
	return json(jsonData)
}

func GetNotifications(w http.ResponseWriter, r *http.Request) *response {
	user, params, db, er := prepare(r, "page:numeric", "request_id")
	if er != nil {
		return er
	}

	// Todo Limit
	notifications := []*Notification{}
	err := db.Tx.Query(`SELECT id, "type", body, img, url, read, created from notifications where user_id = $1`, user.Id).Rows(&notifications)
	if err != nil {
		return err(401, nil)
	}

	// revel
	// logging
	// goconvey testing
	// golang weekly

	jsonData := struct {
		RequestId string `json:"request_id"`
	}{
		RequestId: params["request_id"],
	}
	return json(jsonData)
}

func MarkNotificationAsRead(w http.ResponseWriter, r *http.Request) *response {
	user, ok := Auth(r)
	if !ok {
		return err(401, nil)
	}

	redisConn := g.RedisPool.Get()
	defer redisConn.Close()
	redisConn.Do("hset", "u"+user.IdStr, "u", 0)

	return success()
}

