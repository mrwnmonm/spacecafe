package api

// import (
// 	"image"
// 	"net/http"
// 	"strconv"
// 	"strings"
// 	"time"

// 	"code.google.com/p/go.crypto/bcrypt"
// 	"github.com/disintegration/imaging"
// 	"github.com/mrwnmonm/spacecafe/api/conf"
// 	"github.com/mrwnmonm/spacecafe/api/ironman"
// 	"github.com/mrwnmonm/spacecafe/api/models"
// )

// // TODO: delete the account that didn't verfiy it's email after certain time
// // eg: register with fake mail
// // POST /users  -  Params: Name, Email, Password
// func Create(w http.ResponseWriter, r *http.Request) {
// 	err := r.ParseMultipartForm(10000000)
// 	if err != nil {
// 		ironman.ERROR.Println(err)
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	// client side handling generating error messages for this kind of validation
// 	// on server just check and respond without messages
// 	if r.FormValue("email") == "" || r.FormValue("password") == "" || r.FormValue("name") == "" || r.FormValue("username") == "" {
// 		ironman.Respond(400, w)
// 		return
// 	}
// 	email := strings.ToLower(r.FormValue("email"))
// 	password := r.FormValue("password")
// 	name := r.FormValue("name")
// 	username := strings.ToLower(r.FormValue("username"))

// 	file, _, err := r.FormFile("img")
// 	if err != nil && err != http.ErrMissingFile {
// 		ironman.ERROR.Println(err)
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	// Validation
// 	errors := models.SignupError{}
// 	hasErrors := false

// 	if len(name) > 25 {
// 		errors.Name = "Name can't be more than 25 charachters"
// 		hasErrors = true
// 		goto respondeErrors
// 	}

// 	// TODO: validate username

// 	// http://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
// 	if len(email) > 320 {
// 		errors.Email = "Email can't be more than 320 characters"
// 		hasErrors = true
// 		goto respondeErrors
// 	}

// 	if len(password) < 6 {
// 		errors.Password = "Password can't be less than 6 characters"
// 		hasErrors = true
// 		goto respondeErrors
// 	}

// 	// https://www.owasp.org/index.php/Password_Storage_Cheat_Sheet
// 	if len(password) > 160 {
// 		errors.Password = "Password can't be more than 160 characters"
// 		hasErrors = true
// 		goto respondeErrors
// 	}
// respondeErrors:
// 	if hasErrors {
// 		ironman.RespondWithJson(420, w, errors)
// 		return
// 	}

// 	txn, err := ironman.Db.Begin()
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	defer txn.Commit()

// 	exists, err := ironman.CheckUserByUsername(txn, username)
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	if exists {
// 		errors.Username = "Username already exists"
// 		hasErrors = true
// 		goto respondeErrors
// 	}

// 	exists, err = ironman.CheckUserByEmail(txn, email)
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	if exists {
// 		errors.Email = "Email already exists"
// 		hasErrors = true
// 		goto respondeErrors
// 	}

// 	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	now := time.Now()
// 	secret := ironman.RandString(20)

// 	var id int64
// 	err = txn.QueryRow(`INSERT INTO users(
//             id,name, email,username, hashed_password, secret, secret_sent_at, created)
//     		VALUES (($1||nextval('users_id_seq')::text)::bigint , $2, $3, $4, $5, $6, $7, $8) RETURNING id`,
// 		conf.MachineKey, name, email, username, hashedPassword, secret, now, now).Scan(&id)

// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	idStr := strconv.FormatInt(id, 64)
// 	if file != nil {
// 		defer file.Close()

// 		img, _, err := image.Decode(file)
// 		if err != nil {
// 			ironman.ERROR.Println(err)
// 			txn.Rollback()
// 			ironman.Respond(500, w)
// 			return
// 		}

// 		dst := imaging.Thumbnail(img, 100, 100, imaging.CatmullRom)
// 		err = imaging.Save(dst, "./public/img/users/"+idStr+"_100.png")
// 		if err != nil {
// 			ironman.ERROR.Println(err)
// 			txn.Rollback()
// 			ironman.Respond(500, w)
// 			return
// 		}

// 		dst2 := imaging.Thumbnail(img, 32, 32, imaging.CatmullRom)
// 		err = imaging.Save(dst2, "./public/img/users/"+idStr+"_32.jpg")
// 		if err != nil {
// 			ironman.ERROR.Println(err)
// 			txn.Rollback()
// 			ironman.Respond(500, w)
// 			return
// 		}

// 		// TODO: compress images
// 	} else {
// 		// TODO: save default image
// 	}

// 	// TODO:send confirmation email, (with secret and id)  ensure the api key, maybe you want to make a new account for spacecafe
// 	// ex: verify_email/userid/secret
// 	// also encode the url
// 	// TODO: also check what is the best, smtp or the web api
// 	// https://github.com/jordan-wright/email
// 	// https://github.com/sendgrid/sendgrid-go
// 	// http://sendgrid.com/marketing/sendgrid-services-v2
// 	// http://mandrill.com/features/
// 	// check  https://education.github.com/pack/offers
// 	// choose mandrill for pricing http://mandrill.com/pricing/
// 	// https://mandrillapp.com/api/docs/

// 	ironman.Respond(200, w)
// 	return
// }

// func Login(w http.ResponseWriter, r *http.Request) {
// 	r.ParseForm()
// 	if r.FormValue("email") == "" || r.FormValue("password") == "" || r.FormValue("remember") == "" {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	email := strings.ToLower(r.FormValue("email"))
// 	password := r.FormValue("password")
// 	rm := r.FormValue("remember")

// 	var rememberMe bool
// 	if rm == "true" {
// 		rememberMe = true
// 	} else if rm == "false" {
// 		rememberMe = false
// 	} else {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	txn, err := ironman.Db.Begin()
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	defer txn.Commit()

// 	row := txn.QueryRow(`select id, name, hashed_password, confirmed
//     from users where lower(email) = $1`, email)

// 	user := models.User{}
// 	err = row.Scan(&user.Id, &user.Name, &user.HashedPassword, &user.Confirmed)
// 	if err == sql.ErrNoRows { // user doesn't exist
// 		ironman.Respond(420, w) // return the same code as if the password is wrong
// 		return
// 	}

// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	// CompareHashAndPassword compares a bcrypt hashed password with its possible plaintext equivalent. Returns nil on success, or an error on failure.
// 	err = bcrypt.CompareHashAndPassword(user.HashedPassword, []byte(password))
// 	if err != nil { //login failed
// 		ironman.Respond(420, w)
// 		return
// 	}

// 	if !user.Confirmed {
// 		ironman.Respond(421, w)
// 		return
// 	}

// 	// create a session/persistent login xtoken for the user
// 	redisconn := ironman.RedisPool.Get()
// 	defer redisconn.Close()

// 	rnd := ironman.RandString(20)
// 	x := ironman.GetShaString(rnd)
// 	t := strconv.FormatInt(time.Now().Unix(), 10)
// 	idstr := strconv.FormatInt(user.Id, 10)

// 	var xtoken string

// 	if rememberMe {
// 		xtoken = "p:" + idstr + ":" + t + ":" + rnd
// 		redisconn.Send("MULTI")
// 		redisconn.Send("SET", "p:"+idstr+":"+t, x)
// 		redisconn.Send("EXPIRE", "p:"+idstr+":"+t, conf.RememberUserLifeTime)
// 		redisconn.Send("SADD", "p:"+idstr, t)
// 		_, err = redisconn.Do("EXEC")
// 	} else {
// 		xtoken = "s:" + idstr + ":" + t + ":" + rnd
// 		redisconn.Send("MULTI")
// 		redisconn.Send("SET", "s:"+idstr+":"+t, x)
// 		redisconn.Send("EXPIRE", "s:"+idstr+":"+t, conf.SessionLifeTime)
// 		_, err = redisconn.Do("EXEC")
// 	}

// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	response := struct {
// 		XToken     string          `json:"xtoken"`
// 		RememberMe bool            `json:"remember_me"`
// 		User       models.UserJson `json:"user"`
// 	}{
// 		XToken:     xtoken,
// 		RememberMe: rememberMe,
// 		User: models.UserJson{
// 			Id:   user.Id,
// 			Name: user.Name,
// 			// Username: user.Username,
// 			// Email: user.Email,
// 			// TODO: return img
// 		},
// 	}

// 	ironman.RespondWithJson(200, w, response)
// 	return
// }


// // POST /users/reconfirm  -  Params Email
// //
// // check if email exists,
// // check if email already confirmed (in that case front end should redirect to login),
// // if all pass, create new key
// func Reconfirm(w http.ResponseWriter, r *http.Request) {
// 	params, ok := ironman.GetParams(w, r)
// 	if !ok {
// 		return
// 	}

// 	if params["email"] == nil || params["email"] == "" {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	email, ok := params["email"].(string)
// 	if !ok {
// 		ironman.Respond(400, w)
// 		return
// 	}
// 	email = strings.ToLower(email)

// 	txn, err := ironman.Db.Begin()
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	defer txn.Commit()

// 	var id int64
// 	var confirmed bool
// 	err = txn.QueryRow(`select id, confirmed from users where lower(email)=$1`, email).Scan(&id, &confirmed)
// 	if err == sql.ErrNoRows { // email doesn't exist
// 		ironman.Respond(404, w)
// 		return
// 	}

// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	if confirmed {
// 		ironman.Respond(320, w)
// 		return
// 	}

// 	s := ironman.RandString(20)
// 	now := time.Now()

// 	_, err = txn.Exec(`UPDATE users SET secret=$1, secret_sent_at=$2  WHERE id=$3 
//   			`, s, now, id)

// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	// TODO send email with secret and id
// 	// also encode the url

// 	ironman.Respond(200, w)
// 	return
// }


// func RenewPass(w http.ResponseWriter, r *http.Request) {
// 	params, ok := ironman.GetParams(w, r)
// 	if !ok {
// 		return
// 	}

// 	secret, ok := params["secret"].(string)
// 	if !ok {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	// this check is very important
// 	// because sometimes the secret_token in the database is empty
// 	if secret == "" {
// 		ironman.Respond(404, w)
// 		return
// 	}

// 	idstr, ok := params["id"].(string)
// 	if !ok {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	if idstr == "" {
// 		ironman.Respond(404, w)
// 		return
// 	}

// 	id, err := strconv.ParseInt(idstr, 10, 64)

// 	if params["password"] == nil {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	password, ok := params["password"].(string)
// 	if !ok {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	txn, err := ironman.Db.Begin()
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	defer txn.Commit()

// 	var xsecret string
// 	var secretSentAt time.Time
// 	err = txn.QueryRow(`select secret, secret_sent_at from users where id=$1`, id).Scan(&xsecret, &secretSentAt)
// 	if err == sql.ErrNoRows { // user doesn't exists
// 		ironman.Respond(404, w)
// 		return
// 	}

// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	if xsecret != secret {
// 		ironman.Respond(404, w)
// 		return
// 	}

// 	var age float64
// 	age = time.Since(secretSentAt).Minutes()

// 	if age > conf.ResetPassKeyLifeTime { // expired
// 		ironman.Respond(421, w)
// 		return
// 	}

// 	hasErrors := false
// 	type Errors struct {
// 		Password []string `json:"password"`
// 	}
// 	errors := Errors{}
// 	if password == "" {
// 		errors.Password = append(errors.Password, "Password can't be blank")
// 		hasErrors = true
// 	}

// 	if len(password) < 6 {
// 		errors.Password = append(errors.Password, "Password can't be less than 6 charachters")
// 		hasErrors = true
// 	}

// 	// if strings.Contains(password, " ") {
// 	// 	errors.Password = append(errors.Password, "Password can't contain spaces")
// 	// 	hasErrors = true
// 	// }

// 	if hasErrors {
// 		ironman.RespondWithJson(420, w, errors)
// 		return
// 	}

// 	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	_, err = txn.Exec(`UPDATE users SET hashed_password=$1,secret=''  WHERE id=$2`, hashedPassword, id)
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	redisconn := ironman.RedisPool.Get()
// 	defer redisconn.Close()
// 	// ignore errors here, because if error happend and the user got error message
// 	// he will thought that the password didn't change
// 	tokens, _ := redis.Strings(redisconn.Do("SMEMBERS", "p:"+idstr))

// 	redisconn.Send("MULTI")
// 	for _, v := range tokens {
// 		redisconn.Send("DEL", "p:"+idstr+":"+v)
// 	}
// 	redisconn.Send("DEL", "p:"+idstr)
// 	redisconn.Do("EXEC")

// 	ironman.Respond(200, w)
// 	return
// }


// func ResetPass(w http.ResponseWriter, r *http.Request) {
// 	params, ok := ironman.GetParams(w, r)
// 	if !ok {
// 		return
// 	}

// 	if params["email"] == nil || params["email"] == "" {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	email, ok := params["email"].(string)
// 	if !ok {
// 		ironman.Respond(400, w)
// 		return
// 	}
// 	email = strings.ToLower(email)

// 	txn, err := ironman.Db.Begin()
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	defer txn.Commit()

// 	var id int64
// 	var confirmed bool
// 	err = txn.QueryRow(`select id, confirmed from users where lower(email)=$1`, email).Scan(&id, &confirmed)
// 	if err == sql.ErrNoRows { // email doesn't exists
// 		ironman.Respond(404, w)
// 		return
// 	}

// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	if !confirmed {
// 		ironman.Respond(420, w)
// 		return
// 	}

// 	now := time.Now()
// 	s := ironman.RandString(20)
// 	_, err = txn.Exec(`UPDATE users SET secret_token=$1, secret_token_sent_at=$2  WHERE id=$3
// 	 			`, s, now, id)

// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	// TODO send email with secret token and id
// 	// also encode the url
// 	// /#/renew_pass?secret=987asdg9klwe982ks&&id=10000000739487

// 	ironman.Respond(200, w)
// 	return
// }


// // POST /users/confirm  -  Params: secret, Id
// func Confirm(w http.ResponseWriter, r *http.Request) {
// 	r.ParseForm()

// 	// this check is very important
// 	// because sometimes the secret in the database is empty
// 	if r.FormValue("secret") == "" || r.FormValue("id") == "" {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	secret := r.FormValue("secret")
// 	idstr := r.FormValue("id")

// 	id, err := strconv.ParseInt(idstr, 10, 64)
// 	if err != nil {
// 		ironman.Respond(404, w)
// 		return
// 	}

// 	txn, err := ironman.Db.Begin()
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	defer txn.Commit()

// 	user := models.User{}
// 	err = txn.QueryRow(`select name, username, img, secret, confirmed from users where id=$1`, id).Scan(&user.Name, &user.Username, &user.Img, &user.Secret, &user.Confirmed)
// 	if err == sql.ErrNoRows { // user doesn't exists
// 		ironman.Respond(404, w)
// 		return
// 	}

// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	if user.Confirmed {
// 		ironman.Respond(320, w)
// 		return
// 	}

// 	if user.Secret != secret {
// 		ironman.Respond(404, w)
// 		return
// 	}

// 	_, err = txn.Exec(`UPDATE users SET confirmed=true, secret='' WHERE id=$1`, id)
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	// TODO: add following userid,0

// 	redisconn := ironman.RedisPool.Get()
// 	defer redisconn.Close()

// 	redisconn.Send("MULTI")
// 	// add user hash
// 	// TODO: use one letter to fileds to save space  n, i , change that all over the api
// 	redisconn.Send("HMSET", "u:"+idstr, "name", user.Name, "username", user.Username)
// 	// add user tables key, add spacecafe table
// 	redisconn.Send("SADD", "u:"+idstr+":tables", "spacecafe")
// 	_, err = redisconn.Do("EXEC")
// 	if err != nil {
// 		txn.Rollback()
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	//TODO: add spacecafe table to membership table
// 	//TODO: add following to 0

// 	ironman.Respond(200, w)
// 	return
// }
