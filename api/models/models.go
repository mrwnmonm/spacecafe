package models

import "time"

type User struct {
	Id           int       `json:"id"`
	Name         string    `json:"name"`
	AccessToken  string    `json:"-"`
	AccessSecret string    `json:"-"`
	Created      time.Time `json:"-"`
}

type Table struct {
	Name         string    `json:"name"`
	Private      bool      `json:"-"`
	UserId       string    `json:"-"`
	MembersCount int       `json:"members_count"`
	Created      time.Time `json:"-"`
}

type Response struct {
	Code    int
	Error   error
	Message string
	Type    string
	Data    *interface{}
}

// type Comment struct {
// 	Id           int64     `json:"id"`
// 	PostId       int64     `json:"-"`
// 	Body         string    `json:"body"`
// 	UserId       int       `json:"user_id"`
// 	UserName     string    `json:"user_name"`
// 	ParentId     int64     `json:"parent_id"`
// 	Depth        int8      `json:"depth"`
// 	RepliesCount int       `json:"replies_count"`
// 	Visible      bool      `json:"visible"`
// 	Expanded     bool      `json:"expanded"`
// 	Created      time.Time `json:"created"`
// }

type Post struct {
	Id            int64     `json:"id"`
	Body          string    `json:"body"`
	Table         string    `json:"table"`
	UserId        int       `json:"user_id"`
	UserName      string    `json:"user_name"`
	CommentsCount int       `json:"comments_count"`
	LikesCount    int       `json:"likes_count"`
	Created       time.Time `json:"created"`
}

type Preview struct {
	Url         string   `json:"url"`
	Host        string   `json:"host"`
	Title       string   `json:"title"`
	Description string   `json:"description"`
	ImagesPaths []string `json:"images_paths"`
	PreviewType int8     `json:"preview_type"`
	Img         string   `json:"img"`
}

type SignupError struct {
	Name     string `json:"name"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}
