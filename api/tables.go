package api

import (
	"net/http"
	"regexp"
	"strconv"

	"github.com/microcosm-cc/bluemonday"
	"github.com/mrwnmonm/spacecafe/api/g"
	"github.com/russross/blackfriday"
	"github.com/garyburd/redigo/redis"
	// 	"github.com/mrwnmonm/spacecafe/api/ironman"
)

func NewTable(w http.ResponseWriter, r *http.Request) *Response {
	user, p, db, er := prepare(r, "name", "desc:max=140", "about:blank", "type:numeric")
	if er != nil {
		return er
	}

	// letters (lowercase only), numbers, underscore only, min 1 - max 25 , http://rubular.com/r/cYwC1AY7J9
	var reg = regexp.MustCompile(`^[a-z0-9_]{1,25}$`)
	if !reg.MatchString(p["name"]) {
		return Error(400, nil, "Only use small letters, numbers and '_'")
	}

	// TODO: add not allowed tables to configrations (friends, public, home)
	if p["name"] == "all" || p["name"] == "home" {
		return Error(400, nil, "Table name not allowed")
	}

	typ, _ := strconv.Atoi(p["type"])
	if typ != 0 && typ != 1 {
		return Error(400, nil)
	}

	p["desc"] = EliminateNewLines(p["desc"])

	unsafe := blackfriday.MarkdownBasic([]byte(p["about"]))
	about := string(bluemonday.UGCPolicy().SanitizeBytes(unsafe))

	// TODO: Markdown validation
	// TODO: add restricted tables logic
	// TODO: escape all data

	// check if table already exist
	exists := db.IsTableExist(p["name"])
	if exists {
		return Error(420, nil, "Table already exist")
	}

	// Create table
	err := db.CreateTable(&Table{Name: p["name"], UserId: user.Id, Type: typ, Desc: p["desc"], About: about})
	if err != nil {
		return Error(500, err, "Can't create table")
	}

	// Join Table
	err = db.JoinTable(user.Id, p["name"])
	if err != nil {
		db.Tx.Rollback()
		return Error(500, err, "Can't join table")
	}

	// TODO:? http://redis.io/commands/save
	return Success()
}

func GetTable(w http.ResponseWriter, r *http.Request) *Response {
	_, p, db, er := prepare(r, "name")
	if er != nil {
		return er
	}

	// get table info
	table := Table{}
	db.Tx.Query(`select name, about, members_count
    from tables where lower(name) = $1`, p["name"]).Rows(&table)

	g.TRACE.Println(table)

	if table.Name == "" { // table doesn't exist
		return Error(404, nil)
	}

	// check membership if authorized user
	isMember := true
	// if isUser {
	// 	isMember = ironman.CheckMembership(txn, userId, name)
	// }

	// TODO: check table privacy

	jsonData := struct {
		IsMember bool     `json:"is_member"`
		Table    *Table `json:"table"`
	}{
		IsMember: isMember,
		Table:    &table,
	}

	return Json(jsonData)
}

func GetUserTables(w http.ResponseWriter, r *http.Request) *Response {
	user, ok := Auth(r)
	if !ok {
		return Error(401, nil)
	}

	redisconn := g.RedisPool.Get()
	defer redisconn.Close()
	tables, err := redis.Strings(redisconn.Do("zrevrange", "t"+strconv.Itoa(user.Id), 0, -1))
	if err != nil {
		return Error(500, nil)
	}

	jsonData := struct {
		Tables []string `json:"tables"`
	}{
		Tables: tables,
	}

	return Json(jsonData)
}


// func Popular(w http.ResponseWriter, r *http.Request) {
// 	tables, err := ironman.GetTables("order by members DESC")
// 	if err != nil {
// 		ironman.RespondWithJson(500, w, struct{}{})
// 		return
// 	}

// 	ironman.RespondWithJson(200, w, tables)
// 	return
// }

// rank tables based on posts_count and comments_count is nonsense
// user must dicover the table himself
// we will use members_count only to rank tables
// 	// most active tables is determined by the amount of engagment (like or comment) for a post per second
// 	// table age must be considred in this calcuation to decrease the rate of the tables which was active and then became not active
// 	// age(created) returns the time interval from the created date until now
// 	// adding 1 day to prevent generating negative values which result in unexpected results
// 	// - like equal one engagment
// 	// - comment equals 4 engagments
// 	// - adding 0.1 to posts to prevent division by zero and to get float result
// 	// becuase postgresql get integer result from integer division which will be zero in alot of cases here
// 	// so what we acully need is the fraction so we add 0.1 to tell it, that this is float division

// 	// TODO: think about the way most active tables is calculated
// 	// you may take in considration the clicking posts
// func MostActive(w http.ResponseWriter, r *http.Request) {
// 	tables, err := ironman.GetTables(`order by
// 		(
// 			(
// 				(likes + (comments * 4))
// 				/
// 				extract(epoch from (age(created) + '1 day'))
// 			)
// 			/
// 			(posts + 0.1)
// 		)
// 		DESC
// 	`)
// 	if err != nil {
// 		ironman.RespondWithJson(500, w, struct{}{})
// 		return
// 	}

// 	ironman.RespondWithJson(200, w, tables)
// 	return
// }


// func Join(w http.ResponseWriter, r *http.Request) {
// 	id, s, ok := ironman.Authenticate(w, r)
// 	if !ok {
// 		ironman.Respond(s, w)
// 		return
// 	}

// 	// redisconn := ironman.RedisPool.Get()
// 	// defer redisconn.Close()
// 	// tables, err := redis.Strings(redisconn.Do("zadd", "t:"+strconv.Itoa(id), 0, "spacecafe"))
// 	// if err != nil {
// 	// 	ironman.Respond(500, w)
// 	// 	txn.Rollback()
// 	// 	ironman.ERROR.Println(err)
// 	// 	return
// 	// }

// 	// TODO: check if human here, (google capatcha)
// 	// then check number of posts per session
// 	// or make new members can't post, and wait for permission from owner

// 	params, ok := ironman.GetParams(w, r)
// 	if !ok {
// 		return
// 	}

// 	if params["name"] == nil || params["name"] == "" {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	name, ok := params["name"].(string)
// 	if !ok {
// 		ironman.Respond(400, w)
// 		return
// 	}
// 	name = strings.ToLower(name)

// 	// TODO:? check if table exists or leave that for the database to figure out from refrence realation
// 	// check out membership

// 	txn, err := ironman.Db.Begin()
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	defer txn.Commit()

// 	_, err = txn.Exec(`INSERT INTO membership(
//     user_id, table_name)
// 	VALUES ($1, $2)`, id, name)
// 	if err != nil { // TODO: handel the case if already a member
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	redisconn := ironman.RedisPool.Get()
// 	defer redisconn.Close()

// 	idstr := strconv.Itoa(id)

// 	_, err = redisconn.Do("SADD", "u:"+idstr+":tables", name)
// 	// TODO: save to insure that the user tables list will be accurate
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	ironman.Respond(200, w)
// 	return
// }


// func Leave(w http.ResponseWriter, r *http.Request) {
// 	id, s, ok := ironman.Authenticate(w, r)
// 	if !ok {
// 		ironman.Respond(s, w)
// 		return
// 	}

// 	params, ok := ironman.GetParams(w, r)
// 	if !ok {
// 		return
// 	}

// 	if params["name"] == nil || params["name"] == "" {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	name, ok := params["name"].(string)
// 	if !ok {
// 		ironman.Respond(400, w)
// 		return
// 	}

// 	txn, err := ironman.Db.Begin()
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}
// 	defer txn.Commit()

// 	redisconn := ironman.RedisPool.Get()
// 	defer redisconn.Close()

// 	// TODO: checkmembersihp first, or ignore error,
// 	//
// 	_, err = txn.Exec(`DELETE FROM membership
//  	WHERE user_id = $1 and table_name = $2`, id, name)

// 	idstr := strconv.Itoa(id)

// 	// handling the case if the removing from the main db successed but from redis didn't
// 	// the user will get 500 in the first time
// 	// in the second this will give an error because the raw is already deleted
// 	// so we just want to delete from redis
// 	if err == sql.ErrNoRows {
// 		_, _ = redisconn.Do("SREM", "u:"+idstr+":tables", name)
// 	}
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	_, err = redisconn.Do("SREM", "u:"+idstr+":tables", name)
// 	if err != nil {
// 		ironman.Respond(500, w)
// 		ironman.ERROR.Println(err)
// 		return
// 	}

// 	ironman.Respond(200, w)
// 	return
// }


// func Search(w http.ResponseWriter, r *http.Request) {
// 	// TODO: decide if you need to authenticate here
// 	// id, s, ok := ironman.Authenticate(w, r)
// 	// if !ok {
// 	// 	ironman.Respond(s, w)
// 	// 	return
// 	// }

// 	// r.ParseForm()

// 	// if r.FormValue("query") == "" {
// 	// 	ironman.Respond(400, w)
// 	// 	return
// 	// }

// 	// query := r.FormValue("query")

// 	// TODO: search redis or postgresql

// 	// ironman.TRACE.Println(query)

// 	type Option struct {
// 		Value string `json:"value"`
// 		Label string `json:"label"`
// 	}

// 	jsonData := struct {
// 		Tables []Option `json:"tables"`
// 	}{
// 		Tables: []Option{{Value: "react", Label: "react"}, {Value: "redis", Label: "redis"}},
// 	}

// 	ironman.RespondWithJson(200, w, jsonData)
// }

