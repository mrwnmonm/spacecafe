--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.4
-- Started on 2014-10-12 00:58:08 EET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 183 (class 3079 OID 11824)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2120 (class 0 OID 0)
-- Dependencies: 183
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 182 (class 1259 OID 41180)
-- Name: administration; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE administration (
    user_id bigint NOT NULL,
    stage_id bigint NOT NULL
);


ALTER TABLE public.administration OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 16385)
-- Name: comments; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comments (
    body text NOT NULL,
    post_id bigint NOT NULL,
    author_id bigint NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.comments OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 24783)
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comments_id_seq OWNER TO postgres;

--
-- TOC entry 2121 (class 0 OID 0)
-- Dependencies: 181
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE comments_id_seq OWNED BY comments.id;


--
-- TOC entry 172 (class 1259 OID 16408)
-- Name: following; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE following (
    user_id bigint NOT NULL,
    stage_id bigint NOT NULL
);


ALTER TABLE public.following OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 16411)
-- Name: membership; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE membership (
    user_id bigint NOT NULL,
    table_name text NOT NULL
);


ALTER TABLE public.membership OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 16392)
-- Name: posts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE posts (
    body text NOT NULL,
    stage_id bigint DEFAULT 0 NOT NULL,
    user_id bigint DEFAULT 0 NOT NULL,
    tables text[] NOT NULL,
    preview_type smallint DEFAULT 0 NOT NULL,
    title text DEFAULT ''::text NOT NULL,
    url text DEFAULT ''::text NOT NULL,
    host text DEFAULT ''::text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    img text DEFAULT ''::text NOT NULL,
    comments_count integer DEFAULT 0 NOT NULL,
    likes_count integer DEFAULT 0 NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.posts OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 24773)
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_id_seq OWNER TO postgres;

--
-- TOC entry 2122 (class 0 OID 0)
-- Dependencies: 180
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE posts_id_seq OWNED BY posts.id;


--
-- TOC entry 174 (class 1259 OID 16417)
-- Name: stages; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE stages (
    id bigint NOT NULL,
    name text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    creator_id bigint NOT NULL,
    created timestamp with time zone DEFAULT ('now'::text)::date NOT NULL,
    followers integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.stages OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 16424)
-- Name: stages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stages_id_seq OWNER TO postgres;

--
-- TOC entry 2123 (class 0 OID 0)
-- Dependencies: 175
-- Name: stages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stages_id_seq OWNED BY stages.id;


--
-- TOC entry 176 (class 1259 OID 16426)
-- Name: tables; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tables (
    name text NOT NULL,
    private boolean DEFAULT false NOT NULL,
    members_count integer DEFAULT 0 NOT NULL,
    posts_count integer DEFAULT 0 NOT NULL,
    likes_count integer DEFAULT 0 NOT NULL,
    comments_count integer DEFAULT 0 NOT NULL,
    active_rate numeric DEFAULT 0.0 NOT NULL,
    created timestamp with time zone DEFAULT ('now'::text)::date NOT NULL
);


ALTER TABLE public.tables OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 16439)
-- Name: tagging; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tagging (
    post_id bigint NOT NULL,
    table_name text NOT NULL
);


ALTER TABLE public.tagging OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 16445)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id bigint NOT NULL,
    name character varying(40),
    email character varying(100),
    hashed_password bytea,
    created timestamp with time zone DEFAULT now() NOT NULL,
    tables_count smallint DEFAULT 0 NOT NULL,
    confirmed boolean DEFAULT false NOT NULL,
    secret text NOT NULL,
    secret_sent_at timestamp with time zone DEFAULT now() NOT NULL,
    img text DEFAULT ''::text NOT NULL,
    stages_count smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 16457)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2124 (class 0 OID 0)
-- Dependencies: 179
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 1941 (class 2604 OID 24785)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comments ALTER COLUMN id SET DEFAULT nextval('comments_id_seq'::regclass);


--
-- TOC entry 1953 (class 2604 OID 24775)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY posts ALTER COLUMN id SET DEFAULT nextval('posts_id_seq'::regclass);


--
-- TOC entry 1955 (class 2604 OID 16460)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stages ALTER COLUMN id SET DEFAULT nextval('stages_id_seq'::regclass);


--
-- TOC entry 1971 (class 2604 OID 16461)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2001 (class 2606 OID 41184)
-- Name: administration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY administration
    ADD CONSTRAINT administration_pkey PRIMARY KEY (user_id, stage_id);


--
-- TOC entry 1973 (class 2606 OID 24795)
-- Name: comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- TOC entry 1979 (class 2606 OID 16465)
-- Name: following_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY following
    ADD CONSTRAINT following_pkey PRIMARY KEY (user_id, stage_id);


--
-- TOC entry 1984 (class 2606 OID 16467)
-- Name: membership_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY membership
    ADD CONSTRAINT membership_pkey PRIMARY KEY (user_id, table_name);


--
-- TOC entry 1975 (class 2606 OID 24793)
-- Name: posts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- TOC entry 1988 (class 2606 OID 16471)
-- Name: stages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY stages
    ADD CONSTRAINT stages_pkey PRIMARY KEY (id);


--
-- TOC entry 1990 (class 2606 OID 16473)
-- Name: tables_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tables
    ADD CONSTRAINT tables_pkey PRIMARY KEY (name);


--
-- TOC entry 1992 (class 2606 OID 16475)
-- Name: tagging_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tagging
    ADD CONSTRAINT tagging_pkey PRIMARY KEY (post_id, table_name);


--
-- TOC entry 1996 (class 2606 OID 16477)
-- Name: users_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- TOC entry 1999 (class 2606 OID 16479)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 1980 (class 1259 OID 16482)
-- Name: following_stage_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX following_stage_id_idx ON following USING btree (stage_id);


--
-- TOC entry 1981 (class 1259 OID 16483)
-- Name: following_user_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX following_user_id_idx ON following USING btree (user_id);


--
-- TOC entry 1982 (class 1259 OID 16484)
-- Name: following_user_id_stage_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX following_user_id_stage_id_idx ON following USING btree (user_id, stage_id);


--
-- TOC entry 1985 (class 1259 OID 24796)
-- Name: membership_user_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX membership_user_id_idx ON membership USING btree (user_id);


--
-- TOC entry 1986 (class 1259 OID 16485)
-- Name: membersihp_user_id_table_name_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX membersihp_user_id_table_name_idx ON membership USING btree (user_id, table_name);


--
-- TOC entry 1976 (class 1259 OID 16486)
-- Name: posts_stage_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX posts_stage_id_idx ON posts USING btree (stage_id);


--
-- TOC entry 1977 (class 1259 OID 33000)
-- Name: posts_tables_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX posts_tables_idx ON posts USING gin (tables);


--
-- TOC entry 1993 (class 1259 OID 16487)
-- Name: tagging_post_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX tagging_post_id_idx ON tagging USING btree (post_id);


--
-- TOC entry 1994 (class 1259 OID 16488)
-- Name: tagging_post_id_table_name_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX tagging_post_id_table_name_idx ON tagging USING btree (post_id, table_name);


--
-- TOC entry 1997 (class 1259 OID 16489)
-- Name: users_lower_email; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--
z