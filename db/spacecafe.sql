CREATE TABLE comments (
    id bigserial NOT NULL,
    body text NOT NULL,
    post_id bigint NOT NULL,
    author_id integer NOT NULL,
    parent_id bigint NOT NULL,
    replies_count integer DEFAULT 0 NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT comments_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE comments
  OWNER TO postgres;
-----------------------------------
CREATE TABLE membership (
    user_id integer NOT NULL,
    table_name text NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE membership
  OWNER TO postgres;
-----------------------------------
CREATE TABLE posts (
    id bigserial NOT NULL,
    body text NOT NULL,
    user_id integer NOT NULL,
    "table" text NOT NULL,
    comments_count integer DEFAULT 0 NOT NULL,
    likes_count integer DEFAULT 0 NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT posts_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE posts
  OWNER TO postgres;
----------------------------------
CREATE TABLE tables (
    name text NOT NULL,
    creator_id integer NOT NULL,
    private boolean DEFAULT false NOT NULL,
    members_count integer DEFAULT 0 NOT NULL,
    posts_count integer DEFAULT 0 NOT NULL,
    comments_count integer DEFAULT 0 NOT NULL,
    likes_count integer DEFAULT 0 NOT NULL,
    active_rate numeric DEFAULT 0.0 NOT NULL,
    created timestamp with time zone DEFAULT ('now'::text)::date NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tables
  OWNER TO postgres;
----------------------------------
CREATE TABLE users (
    id serial NOT NULL,
    name character varying(40),
    username character varying(40),
    email character varying(100),
    tables_count smallint DEFAULT 0 NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;
