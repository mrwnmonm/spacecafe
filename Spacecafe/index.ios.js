'use strict';

var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  TabBarIOS,
  Text,
  View,
} = React;

// Note: we currently require you to call the root module `App`,
// as it appears in this template.
var App = React.createClass({
  getInitialState: function() {
    return {
      selectedTab: 'Stream'
    };
  },
  render: function() {
    return (
      <TabBarIOS
      tintColor="#08c"
      barTintColor="white">
      <TabBarIOS.Item
      title="Stream"
      selected={this.state.selectedTab === 'Stream'}
      onPress={() => {
            this.setState({
              selectedTab: 'Stream',
            });
          }}>
         <View style={styles.container}>
            <Text style={styles.title}>Stream</Text>
          </View>
      </TabBarIOS.Item>

      <TabBarIOS.Item
          title="Explore"
          selected={this.state.selectedTab === 'Explore'}
          onPress={() => {
            this.setState({
              selectedTab: 'Explore'
            });
          }}>
          <View><Text>Explore</Text></View>
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  title:{
    fontSize: 19,
    fontWeight: 'bold'
  }
});

AppRegistry.registerComponent('App', () => App);
